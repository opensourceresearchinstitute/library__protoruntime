/*
 * Copyright 2010  OpenSourceResearchInstitute
 *
 * Licensed under BSD
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <inttypes.h>
#include <sys/time.h>

#include <PR__include/PR__PI.h>
#include <PR__include/PR__structs__common.h>
#include "PR__structs__int.h"
#include "Services_Offered_by_PR/Services_Language/PRServ_int.h"

#include <PR__include/PR__int.h>


/* MEANING OF   WL  PI  SS  int
 * These indicate which places the function is safe to use.  They stand for:
 * WL: Wrapper Library
 * PI: Plugin 
 * SS: Startup and Shutdown
 * int: internal to the PR implementation
 */


/*All langlets use this call to make their slaves ready..  this, in turn,
 * calls the make_ready that was registered by the langlet.
 */
void
PR_PI__make_slave_ready( SlaveVP *slave, void *_langEnv )
 { PRLangEnv *protoLangEnv;
 
   if( _PRTopEnv->overrideAssigner != NULL )
    {
      //put slave into override readyQ
      
      //update override hasWork flag ?
    }
   else
    { //call langlet's registered make ready
      protoLangEnv = PR_int__give_proto_lang_env( _langEnv );
      (*protoLangEnv->makeSlaveReadyFn)( slave, _langEnv );
      
      //incr amount of work ready, in langlet's environ
      protoLangEnv->numReadyWork += 1;
      if(protoLangEnv->numReadyWork == 1)
       { protoLangEnv->processEnvIsIn->numEnvsWithWork += 1;
       }
    }
 }
/*Make slave ready, without having to know the lang env.. just the magic num
 */
void
PR_PI__make_slave_ready_for_lang( SlaveVP *slave, int32 magicNum )
 { void *langEnv;
   langEnv = PR_PI__give_lang_env_for_slave( slave, magicNum );
   PR_PI__make_slave_ready( slave, langEnv );
 }

/*Any langlet can transfer slaves over to be resumed in PRServ.. the resume Fn
 * is registered in the PRServ lang env during process creation.
 */
void
PR_PI__resume_slave_in_PRServ( SlaveVP *slave )
 { void *langEnv;
   langEnv = PR_PI__give_lang_env_for_slave( slave, PRServ_MAGIC_NUMBER );
   PR_PI__make_slave_ready( slave, langEnv );
 }

void
PR_PI__make_task_ready( void *_task, void *_langEnv )
 { PRLangEnv *protoLangEnv;
 
   if( _PRTopEnv->overrideAssigner != NULL )
    {
      //put task into override readyQ
      
      //update numWorkReady
    }
   else
    { //call langlet's registered make ready
      protoLangEnv = PR_int__give_proto_lang_env( _langEnv );
      (*protoLangEnv->makeTaskReadyFn)( _task, _langEnv );
      
      //incr amount of work ready, in langlet's environ
      protoLangEnv->numReadyWork += 1;
      if(protoLangEnv->numReadyWork == 1)
       { protoLangEnv->processEnvIsIn->numEnvsWithWork += 1;
       }
    }
 }


/*This is used by langlets..  the intent is that they provide a wrapper
 * lib call for a "wait" command, and then call this PR service inside their
 * request handler.
 *Note: PRServ doesn't offer a "wait for work to end".. it also doesn't have any
 * create work calls..  and there is no way to end PRServ, except by ending the
 * process.  Therefore, this can safely use the PRServ env to
 * resume any waiting slaves (incl free task slaves, which are one-to-one
 * with a task).
 */
void
PR_PI__handle_wait_for_langlets_work_to_end( SlaveVP *slave, void *langEnv )
 { PRLangEnv *protoLangEnv;
 
   protoLangEnv = PR_int__give_proto_lang_env( langEnv );

   if( protoLangEnv->numLiveWork == 0 )
    {    //resume into different env than one with no work, as it may be shut down
      void *
      resumeEnv = PR_PI__give_lang_env_from_process( slave->processSlaveIsIn, PRServ_MAGIC_NUMBER ); 
      PR_PI__make_slave_ready( slave, resumeEnv );
      return;
    }
   else
    {
      writePrivQ( slave, protoLangEnv->waitingForWorkToEndQ );
    }
 }
   
SlaveVP *
PR_PI__give_slave_lang_meta_task_is_assigned_to( void *langMetaTask )
 { PRMetaTask *metaTask = PR_int__give_prolog_of_lang_meta_task( langMetaTask );
   return metaTask->slaveAssignedTo;
 }

void
PR_PI__set_no_del_flag_in_lang_meta_task( void *langMetaTask )
 { PRMetaTask *protoMetaTask = PR_int__give_prolog_of_lang_meta_task( langMetaTask );
   protoMetaTask->goAheadAndFree = FALSE;
 }

void
PR_PI__clear_no_del_flag_in_lang_meta_task( void *langMetaTask )
 { PRMetaTask *protoMetaTask = PR_int__give_prolog_of_lang_meta_task( langMetaTask );
   protoMetaTask->goAheadAndFree = TRUE;
 }

/*Two use-cases for freeing a meta-task..  one is the langlet has control
 * over the meta tasks's life-line it may differ from the task-work-unit's
 * lifeline..  the other is PR controls, for example when a langlet or 
 * process gets pre-maturely shutdown due to outside influences, or exception,
 * and so on..
 *In the first case, the langlet has two choices..  it can free all the langlet
 * malloc'd data owned by the meta-task, then as PR to free the remaining
 * proto meta-task..  or, it can just call PR's full-service "free meta-task"
 * which in turn calls the langlet's "free just langlet-malloc'd data" Fn, which
 * was given to the meta-task creator..
 * 
 *In the second case, PR calls the langlet's "free just langlet malloc'd data"
 * Fn, which was given to the meta-task creator.  The langlet has no other 
 * involvement.
 * 
 *This is used by the langlet when it separately frees its own
 * portion of the meta-task.
 * 
 *PR uses "PR_int__free_lang_meta_task", which calls the langlet's freer, which
 * was given to the meta task creator
 */
void
PR_PI__free_proto_meta_task_by_langlet( PRMetaTask *protoMetaTask )
 { 
   PR_int__remove_elem_from_collection( protoMetaTask->langMagicNumber,
                    (PRCollElem **) protoMetaTask->slaveAssignedTo->metaTasks );

   PR_int__free( protoMetaTask );       
 }


PRReqst *
PR_PI__take_next_request_out_of( SlaveVP *slaveWithReq )
 { PRReqst *req;

   req = slaveWithReq->request;
   if( req == NULL ) return NULL;

   slaveWithReq->request = slaveWithReq->request->nextReqst;
   return req;
 }

 

/*May 2012
 *CHANGED IMPL -- now a macro in header file
 *
 *Turn function into macro that just accesses the request field
 *
inline void *
PR_PI__take_lang_reqst_from( PRReqst *req )
 {
   return req->langReqData;
 }
*/




