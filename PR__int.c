/*
 * Copyright 2010  OpenSourceResearchInstitute
 *
 * Licensed under BSD
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <inttypes.h>
#include <sys/time.h>

#include <PR__include/PR__structs__common.h>
#include "PR__structs__int.h"
//#include "Services_Offered_by_PR/Services_Language/PRServ__wrapper_library.h"
//#include "Defines/PR_defs.h"
#include <PR__include/PR__int.h>


/* MEANING OF   WL  PI  SS  int
 * These indicate which places the function is safe to use.  They stand for:
 * WL: Wrapper Library
 * PI: Plugin 
 * SS: Startup and Shutdown
 * int: internal to the PR implementation
 */

//===========================================================================
//
//===========================================================================
/*This does the basic work of creating the SlaveVP structure.. but doesn't
 * know which process it goes in, nor what type of VP it is, etc..
 */
inline
SlaveVP *
PR_int__create_slaveVP_helper( BirthFnPtr fnPtr, void *dataParam )
 { SlaveVP *newSlv;
   void    *stackLocs;
   
   newSlv      = PR_int__malloc( sizeof(SlaveVP) );
   stackLocs   = PR_int__malloc( VIRT_PROCR_STACK_SIZE );
   if( stackLocs == 0 )
    { perror("PR_int__malloc stack"); exit(1); }

   newSlv->startOfStack = stackLocs;
   newSlv->slaveNum     = _PRTopEnv->numSlavesCreated++;
   newSlv->request      = NULL;
   newSlv->animSlotAssignedTo = NULL;
      
   newSlv->numTimesAssignedToASlot  = 0;  

      
   newSlv->langDatas = 
      (PRLangData **)PR_int__make_collection_of_size( NUM_IN_COLLECTION );
           
   newSlv->metaTasks = 
      (PRMetaTask **) PR_int__make_collection_of_size( NUM_IN_COLLECTION );

   PR_int__reset_slaveVP_to_BirthFn( newSlv, fnPtr, dataParam );
   
   //============================= MEASUREMENT STUFF ========================
   #ifdef PROBES__TURN_ON_STATS_PROBES
   //TODO: make this TSCHiLow or generic equivalent
   //struct timeval timeStamp;
   //gettimeofday( &(timeStamp), NULL);
   //newSlv->createPtInSecs = timeStamp.tv_sec +(timeStamp.tv_usec/1000000.0) -
   //                                           _PRTopEnv->createPtInSecs;
   #endif
   //========================================================================

   newSlv->typeOfVP            = GenericSlave; //may be changed later!
   
   return newSlv;
 }

inline
SlaveVP *
PR_int__create_slaveVP( BirthFnPtr fnPtr, void *dataParam, PRProcess *process )
 { SlaveVP *newSlv;
 
   newSlv = PR_int__create_slaveVP_helper( fnPtr, dataParam );
   
   process->numLiveGenericSlvs += 1;
   
   return newSlv;
 }


/*A slot slave doesn't use array for lang data nor meta tasks, but it's in
 * there anyway..  have to copy what create slave does, so can just change 
 * the type when a slot slave is converted..
 */
SlaveVP *
PR_int__create_slot_slave( )
 { SlaveVP *retSlv;
 
   retSlv = PR_int__create_slaveVP_helper( &idle_fn, NULL );
   retSlv->typeOfVP     = SlotTaskSlv; 
   
      
   retSlv->langDatas = 
      (PRLangData **) PR_int__make_collection_of_size( NUM_IN_COLLECTION );
           
   retSlv->metaTasks = 
      (PRMetaTask **) PR_int__make_collection_of_size( NUM_IN_COLLECTION );
     
   return retSlv;
 }



/*Called when a new slot slave is needed..  takes from recycle pool, and 
 * sets the slave up to be a slot slave -- no lang data array, no meta task 
 * array.  
 */
SlaveVP *
PR_int__get_recycled_slot_slave( )
 { SlaveVP *retSlv;
 
      //take slave from recycle Q
   retSlv = readPrivQ( _PRTopEnv->slaveRecycleQ );
 
   if( retSlv != NULL ) 
    {
      //set slave up with slot-slave's initial values.
      retSlv->typeOfVP     = SlotTaskSlv;
      retSlv->slaveNum     = _PRTopEnv->numSlavesCreated++;
      retSlv->numTimesAssignedToASlot  = 0;
      retSlv->request      = NULL;
    }
   else //if none to recycle, create a new one
    { retSlv = PR_int__create_slot_slave();
    }
   
   return retSlv;
 }

//==========================================================================
/*When a task in a slot slave suspends, the slot slave has to be changed to
 * a free task slave, then the slot slave replaced.  The replacement can be
 * either a recycled free task slave that finished it's task and has been
 * idle in the recycle queue, or else create a new slave to be the slot slave.
 *The master only calls this with a slot slave that needs to be replaced.
 * 
 *The master continues after this call, handing the converted Free task slave
 * to a request handler,  which places the converted slave into the langlet's
 * env, so it will eventually resume
 */
inline 
void
PR_int__replace_with_new_slot_slv( SlaveVP *oldSlotSlv )
 { SlaveVP *newSlotSlv;
   
      //get a new slave to be the slot slave
   newSlotSlv  = PR_int__get_recycled_slot_slave();
   
      //a slot slave is pinned to a particular slot on a particular core
      //Note, this happens before the request is seen by handler, so nothing
      // has had a chance to change the coreAnimatedBy or anything else..
   newSlotSlv->animSlotAssignedTo = oldSlotSlv->animSlotAssignedTo;
   newSlotSlv->coreAnimatedBy     = oldSlotSlv->coreAnimatedBy;
    
      //put it into the slot slave matrix
   int32 coreNum = oldSlotSlv->coreAnimatedBy;
   _PRTopEnv->slotTaskSlvs[coreNum] = newSlotSlv;

      //Fix up requester, to be an extra slave now (but not an ended one)
      // because it's active, doesn't go into freeTaskSlvRecycleQ
      //Note that process was set when task was assigned to the slot slave..
   oldSlotSlv->typeOfVP = FreeTaskSlv;   
 }

void idle_fn(void* data, SlaveVP *animatingSlv)
 {
   while(1)
    { PR_WL__suspend_slaveVP_and_send_req( animatingSlv );
    }
 }



//===========================================================================

/*In multi-lang mode, there are multiple langData in the slave..  
 * 
 *At some point want to recycle rather than free..
 * 
 *For now, iterate through langData, call registered free-er on each, then
 * free the basic slave
 * 
 *DOES NOT DECREMENT NUMBER OF LIVE SLAVES IN PROCESS
 */
void
PR_int__free_slaveVP( SlaveVP *slave )
 {       
   PR_int__apply_Fn_to_all_in_collection( &freeLangDataAsElem, 
                                           (PRCollElem**) slave->langDatas );
   PR_int__apply_Fn_to_all_in_collection( &freeMetaTaskAsElem, 
                                           (PRCollElem**) slave->metaTasks );
   
   PR_int__free( &(((int32*)(slave->langDatas))[-1]) );
   PR_int__free( &(((int32*)(slave->metaTasks))[-1]) );
   PR_int__free( slave->startOfStack );
   PR_int__free( slave );   
 }

/*This calls the freer registered for each langlet's lang data and meta
 * task.  Then it makes the collections empty.  It also recycles the slave 
 * struct.
 *Note: it does not clear "next in coll" pointers in the coll elements!
 * 
 *DOES NOT DECREMENT NUMBER OF LIVE SLAVES IN PROCESS
 *
 *Note, the fn applied to all elements of collections checks the "don't delete"
 * flag in the lang data and the meta task.. so, if an alternative way of 
 * freeing the slaveVP struct is created, it should handle the "don't delete"
 * flag. (although, an "abort" cleanup should ignore the don't delete flag)
 */
void
PR_int__recycle_slaveVP( SlaveVP *slave )
 { 
   PR_int__apply_Fn_to_all_in_collection( &freeLangDataAsElem, 
                                           (PRCollElem**) slave->langDatas );
   PR_int__apply_Fn_to_all_in_collection( &freeMetaTaskAsElem, 
                                           (PRCollElem**) slave->metaTasks );
   //now, set the collections to "empty"
   PR_int__set_collection_to_empty((PRCollElem**) slave->langDatas);
   PR_int__set_collection_to_empty((PRCollElem**) slave->metaTasks);

   writePrivQ( slave, _PRTopEnv->slaveRecycleQ );
 }

/*Receives a protoLangData, but in the form of a void, so have to cast
 *First checks the "no delete" flag..
 */
void
freeLangDataAsElem( void *elem )
 { PRLangData *protoLangData;
   
   protoLangData = (PRLangData *)elem; //recycler actually receives the prolog.
   if( protoLangData->goAheadAndFree == FALSE ) 
       return;
   
      //apply the free fn that's stored in the lang data prolog
      //Note: freeing whole slave, so no need to remove from collection
      //Note: langlet's freer only frees what langlet has malloc'd
   if(protoLangData->freer != NULL)
    { (*(protoLangData->freer))( PR_int__give_lang_data_of_prolog(protoLangData) );
    }
   PR_int__free( protoLangData );
 }

/*Receives a protoMetaTask, but in form of a void, so have to cast
 */
void
freeMetaTaskAsElem( void *elem )
 { PRMetaTask *protoMetaTask;
   
   protoMetaTask = (PRMetaTask *)elem;  //recycler receives the prolog, and must call
   //a PR Fn to convert prolog to lang-specific version.
   
   if( protoMetaTask->goAheadAndFree == FALSE ) 
       return;
   
      //apply the free fn that's stored in the meta task prolog
   (*(protoMetaTask->freer))( PR_int__give_lang_meta_task_of_prolog(protoMetaTask) );  
 }



//======================================
inline 
void *
PR_int__give_lang_env( PRLangEnv *protoLangEnv )
 {
    return (void *) &(protoLangEnv[1]);
 }
inline 
PRLangEnv *
PR_int__give_proto_lang_env( void *langEnv )
 {
   return (PRLangEnv *) &(((PRLangEnv *)langEnv)[-1]);
 }

//inline
void *
PR_int__create_lang_env_in_process( int32 numBytes, PRProcess *process, int32 magicNum )
 { PRLangEnv *retEnv;
   PRCollElem **langEnvs;
   
      //make the new lang env, with room for the prolog
   retEnv = PR_int__malloc( sizeof(PRLangEnv) + numBytes );
   retEnv->chainedLangEnv = NULL;      //used while inserting into langEnvs
   retEnv->langMagicNumber = magicNum; //used while inserting into langEnvs
   retEnv->processEnvIsIn = process;
   retEnv->numLiveWork = 0;
   retEnv->waitingForWorkToEndQ = makePrivQ();
   
      //process has a PR Collection of lang envs -- get it and insert into it
   langEnvs = (PRCollElem **)process->langEnvs;
   PR_int__insert_elem_into_collection( (PRCollElem *)retEnv, langEnvs, magicNum);
   int32 idxInProcess = process->numLangEnvs; //index starts at 0 for first lang
   process->protoLangEnvsList[idxInProcess] = retEnv;
   retEnv->idxInProcess = idxInProcess;
   process->numLangEnvs += 1;
   
   return (void *)&(retEnv[1]); //skip over prolog
 }

inline
void *
PR_int__remove_lang_env_from_process_and_free( void *langEnv )
 { PRLangEnv   *protoLangEnv = PR_int__give_proto_lang_env( langEnv );
   PRCollElem **langEnvs;
   PRLangEnv  **protoLangEnvsList;
   
      //process has a PR Collection of lang envs -- get it and remove env
   PRProcess *process = protoLangEnv->processEnvIsIn;
   langEnvs = (PRCollElem **)process->langEnvs;
   PR_int__remove_elem_from_collection( protoLangEnv->langMagicNumber, langEnvs );
   int32 count, idx;
   idx = protoLangEnv->idxInProcess;
      //move env pointers that are above the target, down by 1 position, overwriting
   count = (process->numLangEnvs - idx) * sizeof(PRLangEnv *);//idx starts at 0
   protoLangEnvsList = process->protoLangEnvsList;
   memmove( &(protoLangEnvsList[idx]), &(protoLangEnvsList[idx+1]), count);
   process->numLangEnvs -= 1; 
   
   PR_int__free( protoLangEnv );
 }

inline
void *
PR_int__give_lang_env_of_req( PRReqst *req, SlaveVP *requestingSlv )
 {     
   return PR_int__give_lang_env_from_process( requestingSlv->processSlaveIsIn, 
                                                         req->langMagicNumber );
 }

inline
void *
PR_int__give_lang_env_for_slave( SlaveVP *slave, int32 magicNum )
 {    
   return PR_int__give_lang_env_from_process( slave->processSlaveIsIn, magicNum );
 }
inline
PRLangEnv *
PR_int__give_proto_lang_env_for_slave( SlaveVP *slave, int32 magicNum )
 {    
   return PR_int__give_proto_lang_env_from_process( slave->processSlaveIsIn, magicNum );
 }

inline
void *
PR_int__give_lang_env_from_process( PRProcess *process, int32 magicNum )
 { PRLangEnv    *retLangEnv;
   PRCollElem **langEnvs;
   
   langEnvs = (PRCollElem **)process->langEnvs;
   retLangEnv = PR_int__lookup_elem_in_collection( magicNum, langEnvs );
   if( retLangEnv != NULL ) 
      return &(retLangEnv[1]); //skip over prolog
   else
      return NULL;
 }
inline
PRLangEnv *
PR_int__give_proto_lang_env_from_process( PRProcess *process, int32 magicNum )
 { PRLangEnv    *retLangEnv;
   PRCollElem **langEnvs;
   
   langEnvs = (PRCollElem **)process->langEnvs;
   retLangEnv = PR_int__lookup_elem_in_collection( magicNum, langEnvs );
   return retLangEnv; //return prolog
 }
  
/*This is to be called by langlet's assigner.
 */
inline
void
PR_int__put_slave_into_slot( SlaveVP *slave, AnimSlot *slot )
 {
   slave->coreAnimatedBy   = slot->coreSlotIsOn;
               //if work found, put into slot, and adjust flags and state
   slot->slaveAssignedToSlot = slave;
   slave->animSlotAssignedTo = slot;
   slot->needsWorkAssigned   = FALSE;

  #ifdef HOLISTIC__TURN_ON_OBSERVE_UCC
      //have a slave to be assigned to the slot
      //assignSlv->numTimesAssigned++;
         //get previous occupant of the slot
      PRProcess *process = slave->processSlaveIsIn;
      int32 coreNum = slot->coreSlotIsOn;
//      int32 slotNum = slot->slotIdx;  //only one slot per core now
      Unit prev_in_slot = 
         process->last_in_slot[coreNum];
      if(prev_in_slot.vp != 0) //if not first slave in slot, make dependency
       { Dependency newD;      // is a hardware dependency
         newD.from_vp = prev_in_slot.vp;
         newD.from_task = prev_in_slot.task;
         newD.to_vp = slave->slaveNum;
         newD.to_task = slave->numTimesAssignedToASlot;
         addToListOfArrays(Dependency,newD,process->hwArcs);   
       }
      prev_in_slot.vp = slave->slaveNum; //make new slave the new previous
      prev_in_slot.task = slave->numTimesAssignedToASlot;
      process->last_in_slot[coreNum] = prev_in_slot;        
    
  #endif
 }


/*This is to be called by langlet's assigner
 */
inline
void
PR_int__put_task_into_slot( void *langMetaTask, AnimSlot *slot )
 { int32 coreNum;
   SlaveVP *slotSlv;
   PRMetaTask *protoMetaTask;
 
   protoMetaTask = PR_int__give_prolog_of_lang_meta_task( langMetaTask );
   
      //get the slot slave to assign the task to..
   coreNum = slot->coreSlotIsOn;
   slotSlv = _PRTopEnv->slotTaskSlvs[coreNum];

     //point slave to task's function
   PR_int__reset_slaveVP_to_BirthFn( slotSlv, protoMetaTask->birthFn, protoMetaTask->initData );
   PR_int__insert_meta_task_into_slave( protoMetaTask, slotSlv );
   slotSlv->processSlaveIsIn = protoMetaTask->processTaskIsIn;
   PR_int__put_slave_into_slot( slotSlv, slot );
 }

//==========================
inline 
PRMetaTask *
PR_int__give_prolog_of_lang_meta_task( void *task )
 {
    return (PRMetaTask *) &(((PRMetaTask *)task)[-1]);
 }

inline
void *
PR_int__give_lang_meta_task_of_prolog( PRMetaTask *metaTask)
 {
   return (void *)&(metaTask[1]);
 }


inline
PRMetaTask *
PR_int__create_proto_meta_task( int32 size, LangMetaTaskFreer freer, int32 magicNum )
 { PRMetaTask *retMetaTask;

      //make the new meta task
   retMetaTask = PR_int__malloc( sizeof(PRMetaTask) + size );
   retMetaTask->chainedMetaTask = NULL;
   retMetaTask->langMagicNumber = magicNum;
   retMetaTask->slaveAssignedTo = NULL;
   retMetaTask->taskType        = SlotTask; //just common default
   retMetaTask->ID              = NULL;
   retMetaTask->freer           = freer;
   retMetaTask->goAheadAndFree  = TRUE;
     
   return retMetaTask; //skip over prolog
 }

/*Calls the lang's freer, which is pointed to, and also removes from collection
 */
void
PR_int__free_lang_meta_task_and_remove_from_coll( void *langMetaTask)
 { PRMetaTask *protoMetaTask;
   
   protoMetaTask = PR_int__give_prolog_of_lang_meta_task( langMetaTask );
   PR_int__remove_elem_from_collection( protoMetaTask->langMagicNumber,
                    (PRCollElem **) protoMetaTask->slaveAssignedTo->metaTasks );

   (*protoMetaTask->freer)(langMetaTask);
   PR_int__free( protoMetaTask );   
 }

/*Just calls the lang's freer, and frees the malloc'd meta-task chunk. Should
 * already have been removed from slave's collection and all queues holding it
 */
void
PR_int__free_lang_meta_task( void *langMetaTask)
 { PRMetaTask *protoMetaTask;
   
   protoMetaTask = PR_int__give_prolog_of_lang_meta_task( langMetaTask );

   (*protoMetaTask->freer)(langMetaTask);
   PR_int__free( protoMetaTask );   
 }

inline
void *
PR_int__create_lang_meta_task( int32 size, LangMetaTaskFreer freer, int32 magicNum )
 { PRMetaTask *retMetaTask;

      //make the new meta task

   retMetaTask = PR_int__create_proto_meta_task( size, freer, magicNum );
     
   return &(retMetaTask[1]); //skip over prolog
 }

inline
void
PR_int__insert_meta_task_into_slave( PRMetaTask *task, SlaveVP *slave )
 { 
   task->slaveAssignedTo = slave;
//at most one meta-task from a given language can be in slave,
   // so use a form that replaces old meta task if one already there
   PR_int__replace_or_insert_elem_into_collection( (PRCollElem *)task, 
                        (PRCollElem **)slave->metaTasks, task->langMagicNumber );
 }
inline
void
PR_int__insert_lang_meta_task_into_slave( void *langMetaTask, SlaveVP *slave )
 { PRMetaTask *metaTask = &(((PRMetaTask*)langMetaTask)[-1]);
   metaTask->slaveAssignedTo = slave;
   PR_int__replace_or_insert_elem_into_collection( (PRCollElem *)metaTask, 
                    (PRCollElem **)slave->metaTasks, metaTask->langMagicNumber );
 }

/*allocates space for a new lang-meta-task, and inserts it into the slave,
 * under the given magic number.
 */
//inline
void *
PR_int__create_lang_meta_task_in_slave( int32 size, LangMetaTaskFreer freer, SlaveVP *slave, int32 magicNum )
 { PRMetaTask *retMetaTask;
   PRCollElem **metaTasks;

      //make the new meta task
   retMetaTask = PR_int__create_proto_meta_task( size, freer, magicNum );
   retMetaTask->slaveAssignedTo = slave;
   retMetaTask->taskType        = GenericSlave;

      //have a "collection" of meta tasks inside the slave
   metaTasks = (PRCollElem **)slave->metaTasks; 
   PR_int__insert_elem_into_collection( (PRCollElem *)retMetaTask, 
                                                          metaTasks, magicNum );
   
   return &(retMetaTask[1]); //skip over prolog
 }



inline
void *
PR_int__give_lang_meta_task_from_slave( SlaveVP *slave, int32 magicNum )
 { PRMetaTask  *retMetaTask;
   PRCollElem **metaTasks;
   PRLangEnv   *protoLangEnv;
   
   metaTasks = (PRCollElem **)slave->metaTasks;
   retMetaTask = PR_int__lookup_elem_in_collection( magicNum, metaTasks );
   if( retMetaTask != NULL ) 
      return PR_int__give_lang_meta_task_of_prolog(retMetaTask); //skip over prolog
   else
    { protoLangEnv = PR_int__give_proto_lang_env_for_slave( slave, magicNum );
      if(protoLangEnv->langMetaTaskCreator == NULL) 
         PR_int__error("register a meta task creator");
         //This will call 
      return (*protoLangEnv->langMetaTaskCreator)( slave );
    }
 }


//================== langData =============================
inline 
PRLangData *
PR_int__give_prolog_of_lang_data( void *langData )
 {
    return (PRLangData *) &(((PRLangData *)langData)[-1]);
 }

inline
void *
PR_int__give_lang_data_of_prolog( PRLangData *langData)
 {
   return (void *)&(langData[1]);
 }

/*Allocates space for a new lang-data, and inserts it into the slave,
 * under the given magic number.  Also returns it.
 * 
 *Langlet is responsible for using this to create lang data inside its 
 * create-slave handler
 */
inline
void *
PR_PI__create_lang_data_in_slave( int32 size, LangDataFreer freer, 
                                  SlaveVP *slave, int32 magicNum )
 { PRLangData   *retLangData;
   PRCollElem **langDatas;

      //make the new lang Data
   retLangData = PR_int__malloc( sizeof(PRLangData) + size );
   retLangData->chainedLangData = NULL;
   retLangData->langMagicNumber = magicNum;
   retLangData->freer = freer;
   retLangData->goAheadAndFree = TRUE;
   retLangData->slaveAssignedTo = slave;
  
      //multilang has a "collection" of lang datas inside the slave
   langDatas = (PRCollElem **)slave->langDatas; 
   PR_int__insert_elem_into_collection( (PRCollElem *)retLangData, langDatas, magicNum );
   
   return &(retLangData[1]); //skip over prolog
 }
void
PR_int__free_lang_data( void *langData)
 { PRLangData *protoLangData;
   
   protoLangData = PR_int__give_prolog_of_lang_data( langData );
   PR_int__remove_elem_from_collection( protoLangData->langMagicNumber,
                    (PRCollElem **) protoLangData->slaveAssignedTo->langDatas );

   (*protoLangData->freer)(langData);
   PR_int__free( protoLangData );   
 }

inline
void *
PR_int__give_lang_data_from_slave( SlaveVP *slave, int32 magicNum )
 { PRLangData  *retLangData;
   PRCollElem **langDatas;
   PRLangEnv   *protoLangEnv;
   
   langDatas = (PRCollElem **)slave->langDatas;
   retLangData = PR_int__lookup_elem_in_collection( magicNum, langDatas );
   if( retLangData != NULL ) 
      return &( retLangData[1] ); //skip over prolog
   else
    { protoLangEnv = PR_int__give_proto_lang_env_for_slave( slave, magicNum );
      if(protoLangEnv->langDataCreator == NULL) 
         PR_int__error("register a lang data creator");
         //This will call PR_PI__create_lang_data_in_slave -- puts it into slave
      return (*protoLangEnv->langDataCreator)( slave );
    }
 }


//===============================================
PRCollElem ** //return an array of pointers
PR_int__make_collection_of_size( int32 numInColl )
 { int32 *prolog;
   PRCollElem **coll;
      //Note using size of a pointer to a coll element
   int32 numBytes = sizeof(int32) + numInColl * sizeof(PRCollElem *);
   prolog = PR_int__malloc( numBytes );

      //operation of collection relies upon initial entries being NULL
   memset(prolog, ZERO, numBytes>>2);  //BUG: if size not multiple of 4

   prolog[0] = numInColl;
   coll = (PRCollElem **)&(prolog[1]);
   return coll;
 }

inline
void
PR_int__insert_elem_into_collection( PRCollElem *elem, PRCollElem **coll, int32 hash )
 { int32 idx, numIdxsInColl;
   PRCollElem *test;

   numIdxsInColl = ((int32 *)coll)[-1]; //prolog holds number of idexes in array

      //figure out where to link in
   idx = hash & (numIdxsInColl - 1); //mask off, leaving lowest bits
   test = coll[idx];
   elem->chained = NULL; //elem goes at end of any chain, so just being sure here
   if( test == NULL ) //spot empty, so add there
    {    //add to the array
      coll[idx] = elem;
    }
   else //collision -- look for last in chain, then add there
    { while( test->chained != NULL ) 
       { test = test->chained;
       }
         //add new to the end of chain
      test->chained = elem;
    }
 }
 
inline
void
PR_int__replace_or_insert_elem_into_collection( PRCollElem *elem, 
                                                PRCollElem **coll, 
                                                int32 hash )
 { int32 idx, numIdxsInColl;
   PRCollElem *test, **prevPtrLoc;

   numIdxsInColl = ((int32 *)coll)[-1]; //prolog holds number of idexes in array

      //figure out where to link in
   idx = hash & (numIdxsInColl - 1); //mask off, leaving lowest bits
   test = coll[idx];
   if( test == NULL )
    { //insert
      coll[idx] = elem;
      return;
    }
   //see if any elements match
   prevPtrLoc = &(coll[idx]);
   while( test->hash != hash )
    { prevPtrLoc = &(test->chained);
      test = test->chained;
      if( test == NULL )
       { //got to end of chain, not there, so add elem to end
         *prevPtrLoc = elem;
         return;
       }
    }
   //found a match -- replace it
   elem->chained = test->chained;
   *prevPtrLoc = elem;
 }

inline
void *
PR_int__lookup_elem_in_collection( int32 hash, PRCollElem **coll )
 { int32 idx, numIdxsInColl;
   PRCollElem *test;

   numIdxsInColl = ((int32 *)coll)[-1]; //prolog holds number of indexes in array

   idx = hash & (numIdxsInColl - 1); //mask off, leaving lowest bits
   test = coll[idx];
   if( test == NULL ) goto NotFound;
   while( test->hash != hash )
    { test = test->chained;
      if( test == NULL ) goto NotFound;
    }
   return test;
   
 NotFound:
   return NULL;
 }

inline
void
PR_int__remove_elem_from_collection( int32 hash, PRCollElem **coll )
 { int32 idx, numIdxsInColl;
   PRCollElem *test, *prev;

   numIdxsInColl = ((int32 *)coll)[-1]; //num indexes in prolog -- power of 2

   idx = hash & (numIdxsInColl - 1); //mask off, leaving lowest bits (power of 2)
   test = coll[idx];
   if( test == NULL ) return; //not found, nothing to remove
   prev = NULL;
   while( test->hash != hash )
    { prev = test;
      test = test->chained;
      if( test == NULL ) return; //not found, nothing to remove
    }
   if( prev == NULL) //is first elem in chain, so modify ptr in array
    { coll[idx] = coll[idx]->chained;
      return;
    }
   else //remove link from chain
    { prev->chained = test->chained;
      return;
    }
 }


/*The function is allowed to modify its OWN chaining.. this takes the next
 * in chain before calling the function (useful when the function conditionally
 * frees)
 */
inline
void
PR_int__apply_Fn_to_all_in_collection( void (*Fn)(void *), PRCollElem **coll )
 { int32 idx, numIdxsInColl;
   PRCollElem *test, *chainedTest;

   numIdxsInColl = ((int32 *)coll)[-1]; //prolog is num idxs
      //march down array of elements, for each, march down linked chain of elems
   for( idx = 0; idx < numIdxsInColl; idx++ )
    { test = coll[idx];
      while( test != NULL )
       { chainedTest = test->chained; //Fn may destroy test or change test->chained
         (*Fn)((void *)test);
         test = chainedTest;
       }
    }
 }

/*Just sets all entries to NULL -- doesn't modify chaining of any elements
 * that may be left in the collection..
 */
inline
void
PR_int__set_collection_to_empty( PRCollElem **coll )
 { int32 numIdxsInColl = ((int32 *)coll)[-1]; //prolog is num idxs
   memset( coll, 0, numIdxsInColl * sizeof(PRCollElem *) );
 }
//==========================================


//==========================================

/*Later, improve this -- for now, just exits the application after printing
 * the error message.
 */
void
PR_int__throw_exception( char *msgStr, SlaveVP *reqstSlv, PRExcp *excpData )
 {
   printf("%s",msgStr);
   fflush(stdin);
   exit(1);
 }

void
PR_int__error( char *msgStr )
 {
   printf("%s",msgStr);
   fflush(stdin);
   exit(1);
 }


char *
PR_int__strDup( char *str )
 { char *retStr;

   if( str == NULL ) return NULL;
   retStr = (char *)PR_int__malloc( strlen(str) + 1 );
   strcpy( retStr, str );

   return (char *)retStr;
 }


inline void
PR_int__backoff_for_TooLongToGetLock( int32 numTriesToGetLock );

inline void
PR_int__get_wrapper_lock()
 { int32 *addrOfWrapperLock;
 
  #ifdef DEBUG__TURN_ON_SEQUENTIAL_MODE
            return;
  #endif

   addrOfWrapperLock = &(_PRTopEnv->wrapperLock);

   int numTriesToGetLock = 0;
   int gotLock = 0;
   
//            MEAS__Capture_Pre_Master_Lock_Point;
   while( !gotLock ) //keep going until get master lock
    { 
      numTriesToGetLock++;   //if too many, means too much contention
      if( numTriesToGetLock > NUM_TRIES_BEFORE_DO_BACKOFF )
       { PR_int__backoff_for_TooLongToGetLock( numTriesToGetLock );
       }
      if( numTriesToGetLock > MASTERLOCK_RETRIES_BEFORE_YIELD ) 
       { numTriesToGetLock = 0; 
         pthread_yield();
       }
   
         //try to get the lock
      gotLock = __sync_bool_compare_and_swap( addrOfWrapperLock,
                                                         UNLOCKED, LOCKED );
    }
//            MEAS__Capture_Post_Master_Lock_Point;
 }

inline void
PR_int__get_malloc_lock()
 { int32 *addrOfMallocLock;
 
  #ifdef DEBUG__TURN_ON_SEQUENTIAL_MODE
            return;
  #endif

   addrOfMallocLock = &(_PRTopEnv->mallocLock);

   int numTriesToGetLock = 0;
   int gotLock = 0;
   
            MEAS__Capture_Pre_Master_Lock_Point;
   while( !gotLock ) //keep going until get master lock
    { 
      numTriesToGetLock++;   //if too many, means too much contention
      if( numTriesToGetLock > NUM_TRIES_BEFORE_DO_BACKOFF )
       { PR_int__backoff_for_TooLongToGetLock( numTriesToGetLock );
       }
      if( numTriesToGetLock > MASTERLOCK_RETRIES_BEFORE_YIELD ) 
       { numTriesToGetLock = 0; 
         pthread_yield();
       }
   
         //try to get the lock
      gotLock = __sync_bool_compare_and_swap( addrOfMallocLock,
                                                         UNLOCKED, LOCKED );
    }
            MEAS__Capture_Post_Master_Lock_Point;
 }


inline void
PR_int__get_master_lock()
 { int32 *addrOfMasterLock;
 
  #ifdef DEBUG__TURN_ON_SEQUENTIAL_MODE
            return;
  #endif

   addrOfMasterLock = &(_PRTopEnv->masterLock);

   int numTriesToGetLock = 0;
   int gotLock = 0;
   
            MEAS__Capture_Pre_Master_Lock_Point;
   while( !gotLock ) //keep going until get master lock
    { 
      numTriesToGetLock++;   //if too many, means too much contention
      if( numTriesToGetLock > NUM_TRIES_BEFORE_DO_BACKOFF )
       { PR_int__backoff_for_TooLongToGetLock( numTriesToGetLock );
       }
      if( numTriesToGetLock > MASTERLOCK_RETRIES_BEFORE_YIELD ) 
       { numTriesToGetLock = 0; 
         pthread_yield();
       }
   
         //try to get the lock
      gotLock = __sync_bool_compare_and_swap( addrOfMasterLock,
                                                         UNLOCKED, LOCKED );
    }
            MEAS__Capture_Post_Master_Lock_Point;
 }

/*Used by the backoff to pick a random amount of busy-wait.  Can't use the
 * system rand because it takes much too long.
 *Note, are passing pointers to the seeds, which are then modified
 */
inline uint32_t
PR_int__randomNumber()
 {
	_PRTopEnv->seed1 = 36969 * (_PRTopEnv->seed1 & 65535) + 
                          (_PRTopEnv->seed1 >> 16);
	_PRTopEnv->seed2 = 18000 * (_PRTopEnv->seed2 & 65535) + 
                          (_PRTopEnv->seed2 >> 16);
	return (_PRTopEnv->seed1 << 16) + _PRTopEnv->seed2;
 }


/*Busy-waits for a random number of cycles -- chooses number of cycles 
 * differently than for the no-work backoff
 */
inline void
PR_int__backoff_for_TooLongToGetLock( int32 numTriesToGetLock )
 { int32 i, waitIterations;
   volatile double fakeWorkVar; //busy-wait fake work

   waitIterations = 
    PR_int__randomNumber()% (numTriesToGetLock * GET_LOCK_BACKOFF_WEIGHT);   
   //addToHist( wait_iterations, coreLoopThdParams->wait_iterations_hist );
   for( i = 0; i < waitIterations; i++ )
    { fakeWorkVar += (fakeWorkVar + 32.0) / 2.0; //busy-wait
    }
 }

