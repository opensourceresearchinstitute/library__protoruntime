## Variables for CPack Debian packaging

set(CPACK_PACKAGE_DESCRIPTION "Proto-Runtime library")
include(CPack)

cpack_add_component(lib_pr-dev
                     DISPLAY_NAME library__proto-runtime-dev
                     DESCRIPTION This is header files for development with core of a runtime environment that manages multiple threads (contexts) in user space
                     HIDDEN
                     DEPENDS lib_pr
                     DOWNLOADED)

cpack_add_component(lib_pr
                     DISPLAY_NAME library__proto-runtime
                     DESCRIPTION This is the core of a runtime environment that manages multiple threads (contexts) in user space
                     REQUIRED
                     DOWNLOADED)
