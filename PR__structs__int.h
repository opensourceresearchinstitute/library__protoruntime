/*
 *  Copyright 2009 OpenSourceResearchInstitute.org
 *  Licensed under GNU General Public License version 2
 *
 * Author: seanhalle@yahoo.com
 * 
 */

#ifndef _PR__structs__int_H
#define	_PR__structs__int_H
#define _GNU_SOURCE

#include <PR__include/PR__structs__common.h>

//============================ HW Dependent Fns ================================
#include "Defines/PR_defs__HW_constants.h"

#include "HW_Dependent_Primitives/PR__HW_measurement.h"
#include "HW_Dependent_Primitives/PR__primitives.h"


//====================  Core data structures  ===================
 
/* The one and only global variable, holds many odds and ends
 */
typedef struct
 {    //The offsets of these fields are hard-coded into assembly
   void            *coreCtlrReturnPt;    //offset to this field used in asm
   int8             falseSharePad1[256 - sizeof(void*)];
   int32            masterLock;          //offset to this field used in asm
   int8             falseSharePad2[256 - sizeof(int32)];
   int32            wrapperLock;         //offset to this field used in asm
   int8             falseSharePad3[256 - sizeof(int32)];
   int32            mallocLock;          //offset to this field used in asm
   int8             falseSharePad4[256 - sizeof(int32)];
      //============ below this, no fields are used in asm =============

      //Basic PR infrastructure
//   enum PRMode      mode;
   SlaveVP        **masterVPs;
   AnimSlot       **allAnimSlots;
   PrivQueueStruc  *slaveRecycleQ;
   SlaveVP        **slotTaskSlvs;
   SlaveVP        **idleSlv;
   
   
      //Random number seeds -- random nums used in various places  
   uint32_t         seed1;
   uint32_t         seed2;

   PRSysMetaInfo   *metaInfo; //info about this PR system -- vers, build, etc
   int32            num_cores;
   
   //============== This only used by multi-lang mode ============
   PRProcess      **processes;
   int32            numProcesses;
   int32            currProcessIdx;    //used to choose which process gets slot
   int32            firstProcessReady; //use while starting up coreCtlr
   
      //initialize flags for waiting for activity within PR to complete
   bool32           allActivityIsDone;
   pthread_mutex_t  activityDoneLock;
   pthread_cond_t   activityDoneCond;   

   SlaveAssigner    overrideAssigner;
   
   //============== Below this is only used by single-lang mode ==============
   void            *protoLangEnv;
      //Slave creation -- global count of slaves existing, across langs and processes
   int32            numSlavesCreated;  //used to give unique ID to processor
   int32            numTasksCreated;   //to give unique ID to a task
   int32            numSlavesAlive;

   bool32          *coreIsDone;
   int32            numCoresDone;
   int32            shutdownInitiated;
   
   
      //=========== MEASUREMENT STUFF =============
       IntervalProbe   **intervalProbes;
       PrivDynArrayInfo *dynIntervalProbesInfo;
       HashTable        *probeNameHashTbl;
       int32             masterCreateProbeID;
       float64           createPtInSecs; //real-clock time PR initialized
       Histogram       **measHists;
       PrivDynArrayInfo *measHistsInfo;
       MEAS__Insert_Susp_Meas_Fields_into_MasterEnv;
       MEAS__Insert_Master_Meas_Fields_into_MasterEnv;
       MEAS__Insert_Master_Lock_Meas_Fields_into_MasterEnv;
       MEAS__Insert_Malloc_Meas_Fields_into_MasterEnv;
       MEAS__Insert_Plugin_Meas_Fields_into_MasterEnv;
       MEAS__Insert_System_Meas_Fields_into_MasterEnv;
       MEAS__Insert_Counter_Meas_Fields_into_MasterEnv;
      //==========================================
 }
TopEnv;

//=======================  OS Thread related  ===============================

typedef struct
 {
   void           *endThdPt;
   unsigned int    coreNum;
 }
ThdParams;
//=============================  Global Vars ================================
extern volatile TopEnv *_PRTopEnv __align_to_cacheline__;

   //these are global, but only used for startup and shutdown
extern pthread_t       *coreCtlrThdHandles; //pthread's virt-procr state
extern ThdParams      **coreCtlrThdParams;

extern pthread_mutex_t suspendLock;
extern pthread_cond_t  suspendCond;

//=========================  Master Fns  ==================
void * coreController( void *paramsIn );  //standard PThreads fn prototype
void * coreCtlr_Seq( void *paramsIn );  //standard PThreads fn prototype
void animationMaster( void *initData, SlaveVP *masterVP );

inline bool32 masterFunction( AnimSlot  *slot );


#endif	/* _PR__structs_H */

