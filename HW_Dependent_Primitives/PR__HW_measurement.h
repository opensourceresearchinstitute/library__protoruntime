/*
 *  Copyright 2009 OpenSourceResearchInstitute.org
 *  Licensed under GNU General Public License version 2
 *
 * Author: seanhalle@yahoo.com
 * 
 */

#ifndef _PR__HW_MEASUREMENT_H
#define	_PR__HW_MEASUREMENT_H
#define _GNU_SOURCE


//===================  Macros to Capture Measurements  ======================

typedef union
 { uint32 lowHigh[2]; //lowHigh[0] is low, lowHigh[1] is high
   uint64 longVal;
 }
TSCountLowHigh;


//===================  Macros to Capture Measurements  ======================
//
//===== RDTSC wrapper =====  //Does work for x86_64 compile
//Also runs with x86_64 code
#define saveTSCLowHigh(lowHighIn) \
   asm volatile("RDTSC;                   \
                 movl %%eax, %0;          \
                 movl %%edx, %1;"         \
   /* outputs */ : "=m" (lowHighIn.lowHigh[0]), "=m" (lowHighIn.lowHigh[1])\
   /* inputs  */ :                        \
   /* clobber */ : "%eax", "%edx"         \
                );

#define saveTimeStampCountInto(low, high) \
   asm volatile("RDTSC;                   \
                 movl %%eax, %0;          \
                 movl %%edx, %1;"         \
   /* outputs */ : "=m" (low), "=m" (high)\
   /* inputs  */ :                        \
   /* clobber */ : "%eax", "%edx"         \
                );

#define saveLowTimeStampCountInto(low)    \
   asm volatile("RDTSC;                   \
                 movl %%eax, %0;"         \
   /* outputs */ : "=m" (low)             \
   /* inputs  */ :                        \
   /* clobber */ : "%eax", "%edx"         \
                );

//inline TSCount getTSCount();


   //For code that calculates normalization-offset between TSC counts of
   // different cores.
//#define NUM_TSC_ROUND_TRIPS 10

void 
setup_perf_counters();

uint64_t 
rdtsc(void);

void 
cpuID(unsigned i, unsigned regs[4]);

int32
giveNumLogicalCores();

#endif	/* */

