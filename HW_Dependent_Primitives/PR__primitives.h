/*
 *  Copyright 2009 OpenSourceResearchInstitute.org
 *  Licensed under GNU General Public License version 2
 *
 * Author: seanhalle@yahoo.com
 * 
 */

#ifndef  _PR__PRIMITIVES_H
#define	_PR__PRIMITIVES_H
#define _GNU_SOURCE

#include <PR__include/PR__structs__common.h>

void 
recordCoreCtlrReturnLabelAddr(void **returnAddress);

void 
switchToSlv(SlaveVP *nextSlave);

void 
switchToCoreCtlr(SlaveVP *nextSlave);

void 
masterSwitchToCoreCtlr(SlaveVP *nextSlave);

void 
startUpBirthFn();

void 
jmpToOneParamFn();

void 
jmpToTwoParamFn();

void
//asmTerminateCoreCtlr(SlaveVP *currSlv);
asmTerminateCoreCtlr();
void
asmTerminateCoreCtlrSeq(SlaveVP *animatingSlv);

#define flushRegisters() \
        asm volatile ("":::"%rbx", "%r12", "%r13","%r14","%r15")

void
PR_int__save_return_into_ptd_to_loc_then_do_ret(void *ptdToLoc);

void
PR_int__return_to_addr_in_ptd_to_loc(void *ptdToLoc);

inline void
PR_int__point_slaveVP_to_OneParamFn( SlaveVP *slaveVP, void *fnPtr,
                              void    *param);

inline void
PR_int__point_slaveVP_to_TwoParamFn( SlaveVP *slaveVP, void *fnPtr,
                              void    *param1, void *param2);

#endif	/* _PR__HW_DEPENDENT_H */

