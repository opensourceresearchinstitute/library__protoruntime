#include <unistd.h>
#include <fcntl.h>
#include <linux/types.h>
#include <linux/perf_event.h>
#include <errno.h>
#include <sys/syscall.h>
#include <linux/prctl.h>


#include <PR__include/PR__structs__common.h>
//#include "Services_Offered_by_PR/Services_Language/PRServ__wrapper_library.h"

#include "../PR__structs__int.h"
//#include "Defines/PR_defs.h"
//#include <PR__include/PR__int.h>


void 
cpuID(unsigned i, unsigned regs[4]) 
 {
   asm volatile
    ("cpuid" : "=a" (regs[0]), "=b" (regs[1]), "=c" (regs[2]), "=d" (regs[3])
     : "a" (i), "c" (0));
   // ECX is set to zero for CPUID function 4
 }

int32
giveNumLogicalCores()
 {
   //There are a number of ways to get the number of active CPU threads
   // available.  First version of code used CPUID -- but that doesn't
   // work on AMD machines.
   //As of June 2016, using Linux-specific sysconf(_SC_NPROCESSORS_ONLN)
   //See: http://stackoverflow.com/questions/150355/programmatically-find-the-number-of-cores-on-a-machine
   //and: http://man7.org/linux/man-pages/man3/sysconf.3.html

//old way, left here for future reference
//   unsigned regs[4];
     // Logical core count, for a chip (socket)
//   cpuID(1, regs); //see http://www.flounder.com/cpuid_explorer2.htm
//   unsigned logical = (regs[1] >> 16) & 0xff; // EBX[23:16]

   int logical = sysconf(_SC_NPROCESSORS_ONLN);
   if(logical <= 0) logical = 1;  //in case of error, set num cores to 1
   DEBUG__printf(TRUE, " logical cpus: %d\n", logical);
   return logical;
 }

void setup_perf_counters(){
#ifdef HOLISTIC__TURN_ON_PERF_COUNTERS
    struct perf_event_attr hw_event;
   memset(&hw_event,0,sizeof(hw_event));
	hw_event.size = sizeof(struct perf_event_attr);
	hw_event.disabled = 1;
	hw_event.inherit = 1; /* children inherit it   */
	hw_event.pinned = 1; /* must always be on PMU */
	hw_event.exclusive = 0; /* only group on PMU     */
	hw_event.exclude_user = 0; /* don't count user      */
	hw_event.exclude_kernel = 0; /* ditto kernel          */
	hw_event.exclude_hv = 0; /* ditto hypervisor      */
	hw_event.exclude_idle = 0; /* don't count when idle */

        int coreIdx;
   for( coreIdx = 0; coreIdx < _PRTopEnv->num_cores; coreIdx++ )
    {
       hw_event.type = PERF_TYPE_HARDWARE;	
       hw_event.config = PERF_COUNT_HW_CPU_CYCLES; //cycles
        _PRTopEnv->cycles_counter_fd[coreIdx] = syscall(__NR_perf_event_open, &hw_event,
 		0,//pid_t pid, 
		coreIdx,//int cpu, 
		-1,//int group_fd,
		0//unsigned long flags
	);
        if (_PRTopEnv->cycles_counter_fd[coreIdx]<0){
            fprintf(stderr,"On core %d: ",coreIdx);
            perror("Failed to open cycles counter");
        }
        hw_event.type = PERF_TYPE_HARDWARE;
        hw_event.config = PERF_COUNT_HW_INSTRUCTIONS; //instrs
        _PRTopEnv->instrs_counter_fd[coreIdx] = syscall(__NR_perf_event_open, &hw_event,
 		0,//pid_t pid, 
		coreIdx,//int cpu, 
		-1,//int group_fd,
		0//unsigned long flags
	);
        if (_PRTopEnv->instrs_counter_fd[coreIdx]<0){
            fprintf(stderr,"On core %d: ",coreIdx);
            perror("Failed to open instrs counter");
        }
        hw_event.type = PERF_TYPE_HW_CACHE;
        hw_event.config = PERF_COUNT_HW_CACHE_L1D <<  0  |
	(PERF_COUNT_HW_CACHE_OP_READ		<<  8) |
	(PERF_COUNT_HW_CACHE_RESULT_MISS	<< 16); //cache misses
        _PRTopEnv->cachem_counter_fd[coreIdx] = syscall(__NR_perf_event_open, &hw_event,
 		0,//pid_t pid, 
		coreIdx,//int cpu, 
		-1,//int group_fd,
		0//unsigned long flags
	);
        if (_PRTopEnv->cachem_counter_fd[coreIdx]<0){
            fprintf(stderr,"On core %d: ",coreIdx);
            perror("Failed to open cache miss counter");
            exit(1);
        }
   }
        
   prctl(PR_TASK_PERF_EVENTS_ENABLE);
#endif
}
