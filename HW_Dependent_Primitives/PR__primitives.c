/*
 * This File contains all hardware dependent C code.
 */

#include "PR__primitives.h"

#include "../Defines/PR_defs__HW_constants.h"

//#include "PR__structs__int.h"
//#include "Services_Offered_by_PR/Services_Language/PRServ__wrapper_library.h"
//#include "Defines/PR_defs.h"
//#include <PR__include/PR__int.h>


/*Reset the stack then set it up with __cdecl structure on it
 * Except doing a trick for 64 bits, where point slave to helper assembly
 * that copies the function pointer off stack and into a reg, then
 * jumps to it.  So, set the resumeInstrPtr to the helper-assembly.
 *This is for first-time startup of slave.. it trashes the stack.
 *No registers saved into old stack frame, and no animator state to
 * return to
 * 
 *This was factored into separate function because it's used stand-alone in
 * some wrapper-libraries (but only "int" version, to warn users to check
 * carefully that it's safe)
 */
inline void
PR_int__reset_slaveVP_to_BirthFn( SlaveVP *slaveVP, BirthFnPtr fnPtr,
                              void    *dataParam)
 { void  *stackPtr;

// Start of Hardware dependent part           
   
    //Set slave's instr pointer to a helper Fn that copies params from stack
   slaveVP->resumeInstrPtr  = (BirthFnPtr)&startUpBirthFn;
   
    //fnPtr takes two params -- void *dataParam & void *animSlv
    // Stack grows *down*, so start it at highest stack addr, minus room
    // for 2 params + return addr. Do ptr arith in terms of bytes..
   stackPtr = 
     (uint8 *)slaveVP->startOfStack + VIRT_PROCR_STACK_SIZE - 4*sizeof(void*);
  
    //setup __cdecl on stack
    //Normally, return Addr is in loc pointed to by stackPtr, but doing a
    // trick for 64 bit arch, where put ptr to top-level fn there instead,
    // and set resumeInstrPtr to a helper-fn that copies the top-level
    // fn ptr and params into registers.
    //Then, dataParam is at stackPtr + 8 bytes, & animating SlaveVP above
    //Do ptr arith in terms of pointers
   *((SlaveVP**)stackPtr + 2 ) = slaveVP; //rightmost param
   *((void**)stackPtr + 1 ) = dataParam;  //next  param to left
   *((void**)stackPtr) = (void*)fnPtr;    //copied to reg by helper Fn
   
  
// end of Hardware dependent part           
   
      //core controller will switch to stack & frame pointers stored in slave,
      // can't use this fn if have state on stack that needs preserving.
   slaveVP->stackPtr = stackPtr; 
   slaveVP->framePtr = stackPtr; 
 }


/*Preserve the stack, pushing the __cdecl structure onto it
 * For 64 bits, params passed in regs, so point slave to helper assembly
 * that copies the arguments off stack and into regs, then
 * jumps to Fn.  So, set the resumeInstrPtr to the helper-assembly.
 * 
 *This preserves the stack state existed at time slave was suspended.
 */
inline void
PR_int__point_slaveVP_to_OneParamFn( SlaveVP *slaveVP, void *fnPtr,
                              void    *param)
 { void  *stackPtr;

// Start of Hardware dependent part           
   
    // Get the slave's current stack ptr, and make room for param + ret addr
   stackPtr = ((void **)slaveVP->stackPtr - 2);
  
    //save slave's current instr ptr as the return addr, so stack looks
    // just like it does after a call instr.
    //Put argument plus fn addr onto stack -- helper will copy into regs
    // then jump to the fn
    //fnPtr is just below top of stack, param is above at stackPtr + 8 bytes
   *((void**)stackPtr + 1 ) = param;
   *((void**)stackPtr) = slaveVP->resumeInstrPtr; //acts as return addr
   *((void**)stackPtr - 1) = (void*)fnPtr;        //what helper jmps to
   
    //Set slave's instr pointer to a helper Fn that copies params from stack
   slaveVP->resumeInstrPtr  = (BirthFnPtr)&jmpToOneParamFn;
   
// end of Hardware dependent part           
   
      //core controller will switch to stack & frame pointers stored in slave,
      // then jmp to helper Fn, which will then move param to register used
      // to pass argument and jmp to fnPtr saved on stack.
      //That fn should save the framePtr on stack and make room
      // for its own frame, as normal.  So don't modify framePtr, only stack
   slaveVP->stackPtr = stackPtr;
 }


/*Same as for one-parameter function, but puts two arguments on stack
 *Preserve the stack, pushing the __cdecl structure onto it
 * For 64 bits, params passed in regs, so point slave to helper assembly
 * that copies the arguments off stack and into regs, then
 * jumps to Fn.  So, set the resumeInstrPtr to the helper-assembly.
 * 
 *This preserves the stack state existed at time slave was suspended.
 */
inline void
PR_int__point_slaveVP_to_TwoParamFn( SlaveVP *slaveVP, void *fnPtr,
                              void    *param1, void *param2)
 { void  *stackPtr;

// Start of Hardware dependent part           
   
    // Get the slave's current stack ptr, and make room for param + ret addr
   stackPtr = slaveVP->stackPtr - 3;
  
    //save slave's current instr ptr as the return addr, so stack looks
    // just like it does after a call instr.
    //Put argument plus fn addr onto stack -- helper will copy into regs
    // then jump to the fn
    //fnPtr is just below top of stack, param1 is above at stackPtr + 8 bytes
   *((void**)stackPtr + 2 ) = param2;
   *((void**)stackPtr + 1 ) = param1;
   *((void**)stackPtr) = slaveVP->resumeInstrPtr; //acts as return addr
   *((void**)stackPtr - 1) = (void*)fnPtr;        //what helper jmps to
   
    //Set slave's instr pointer to a helper Fn that copies params from stack
   slaveVP->resumeInstrPtr  = (BirthFnPtr)&jmpToTwoParamFn;
   
// end of Hardware dependent part           
   
      //core controller will switch to stack & frame pointers stored in slave,
      // then jmp to helper Fn, which will then move param to register used
      // to pass argument and jmp to fnPtr saved on stack.
      //That fn should save the framePtr on stack and make room
      // for its own frame, as normal.  So don't modify framePtr, only stack
   slaveVP->stackPtr = stackPtr;
 }

