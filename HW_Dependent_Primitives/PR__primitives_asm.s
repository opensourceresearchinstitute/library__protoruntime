
//This is x64 assembly, re-written to be in PIC style..
//There are artifacts from pre-PIC version
//And was written to be as easy to get working as  possible
// so wasteful choices were made for sake of simple code..
//Could use some optimization.. for example, birth Fn addr is trampoline addr
// calculated from the .got.plt, which is passed in to the assembly call
// by placing it on the stack, then in the assembly copying off the stack
// into a reg and jumping to it..  can probably streamline that whole thing

.data
//all vars are defined in C code

.text

//No longer use the result stored by this -- changed when did PIC.. but
// still call this because don't want to mess with the code.. clean up later
//Save return label address for the coreCtlr to pointer
//Arguments: Pointer to variable holding address
.globl recordCoreCtlrReturnLabelAddr
recordCoreCtlrReturnLabelAddr:
    leaq    coreCtlrReturn(%rip), %rcx  #load addr of label 
    movq    %rcx, (%rdi)           #save address to pointer (rdi is passed in)
    ret

#junk from when trying to puzzle out how to use the GOT (no docs..)
#    movq    $coreCtlrReturn@GOTPCREL(%rip), %rcx  #load addr of label address
#    movq    (%rcx), %rcx  #load label address


#    movq    $coreCtlrReturn, %rcx  #load label address
#    movq    %rcx, (%rdi)           #save address to pointer (rdi is passed in)
#    ret
 

//Trick for 64 bit arch -- copies args from stack into regs, then does jmp to
// the top-level function, which was pointed to by the stack-ptr
.globl startUpBirthFn
startUpBirthFn:
    movq    %rdi      , %rsi #get second argument from first argument of switchSlv
    movq    0x08(%rsp), %rdi #get first argument from stack
    movq    (%rsp)    , %rax #get top-level function's addr from stack
    jmp     *%rax            #jump to the top-level function


//Args passed in regs in 64 bit arch. This copies args from stack into regs,
// then does jmp to the function, whose addr is on stack.
//For 64bit, %rdi is first arg, %rsi is second arg to function
//The top of stack is a valid return addr (old value of slaveVP's instrPtr),
// and the fnPtr is just below the top of stack (will be overwritten when
// fn saves the frame ptr)
.globl jmpToOneParamFn
jmpToOneParamFn:
    movq    0x08(%rsp), %rdi #get the argument from stack
    movq   -0x08(%rsp), %rax #get function's addr from stack
    jmp     *%rax            #jump to the function

.globl jmpToTwoParamFn
jmpToTwoParamFn:
    movq    0x10(%rsp), %rsi #get the second argument from stack
    movq    0x08(%rsp), %rdi #get the first argument from stack
    movq   -0x08(%rsp), %rax #get function's addr from stack
    jmp     *%rax            #jump to the function


//Switches from CoreCtlr to a Slv VP's app code.
//switch to VP's stack and frame ptr then jump to VP's next-instr-ptr
/* SlaveVP  offsets:
 * 0x00  stackPtr
 * 0x08 framePtr
 * 0x10 resumeInstrPtr
 * 0x18 coreCtlrFramePtr
 * 0x20 coreCtlrStackPtr
 *
 * _PRTopEnv  offsets:
 * 0x00 coreCtlrReturnPt
 * 0x100 masterLock
 */
.globl switchToSlv
switchToSlv:
    #SlaveVP in %rdi
    movq    %rsp      , 0x20(%rdi)   #save core ctlr stack pointer 
    movq    %rbp      , 0x18(%rdi)   #save core ctlr frame pointer
    movq    0x00(%rdi), %rsp         #restore stack pointer
    movq    0x08(%rdi), %rbp         #restore frame pointer
    movq    0x10(%rdi), %rax         #get the saved 'pointer-into-app-code'
    jmp     *%rax                    #jmp into Slv's app-code  
coreCtlrReturn:
    ret   #jmp here to return back to the core Ctlr, which orig called this Fn

    
//switches to core controller. saves return address
/* SlaveVP  offsets:
 * 0x00 app code stackPtr
 * 0x08 app code framePtr
 * 0x10 addr of instr to jmp-to in order to resume app code
 * 0x18 coreCtlr FramePtr
 * 0x20 coreCtlr StackPtr
 *
 * _PRTopEnv  offsets:
 * 0x00 coreCtlrReturnPt
 * 0x100 masterLock
 */
.globl switchToCoreCtlr
switchToCoreCtlr:
    #SlaveVP in %rdi
    leaq    SlvReturn(%rip), %rcx    #resume by jmp to "ret" at end of this Fn
    movq    %rcx      , 0x10(%rdi)   #store "ret" instr's addr into slave
    movq    %rsp      , 0x00(%rdi)   #save app stack pointer into slave
    movq    %rbp      , 0x08(%rdi)   #save app frame pointer into slave
    movq    0x20(%rdi), %rsp         #restore coreCtlr stack pointer from slave
    movq    0x18(%rdi), %rbp         #restore coreCtlr frame pointer from slave
    leaq    coreCtlrReturn(%rip), %rax #address (PIC way) of end of sw-to-slave 
#Also works if load jmp addr from the field of _PRTopEnv, but more instrs:
#    leaq    _PRTopEnv@GOTPCREL(%rip), %rcx    #calc addr of start of _PRTopEnv var
#    movq    (%rcx), %rcx             #content of _PRTopEnv == ptr to struct
#    movq    (%rcx), %rcx             #get addr to start of struct
#    movq    0x00(%rcx), %rax         #load contents of field of struct
    jmp     *%rax                      #jmp to ret instr to reenter CoreCtlr
SlvReturn:
    ret     #jmp here, to return to app code that called this Fn

//junk accumulated while figuring out PIC version that works.. 
#    movq    $SlvReturn, 0x10(%rdi)   #store return address

#    movq    $_PRTopEnv, %rcx
#    leaq    _PRTopEnv@GOTPCREL(%rip), %rcx    #calc addr of start of _PRTopEnv var

#    movq        (%rcx), %rcx         #get what's at var addr (= addr of struct)
#    movq    0x00(%rcx), %rax         #get contents of CoreCtlrStartPt field


//switches to core controller from master. saves return address
//Releases masterLock so the next AnimationMaster can be executed
/* SlaveVP  offsets:
 * 0x00  stackPtr
 * 0x08 framePtr
 * 0x10 resumeInstrPtr
 * 0x18 coreCtlrFramePtr
 * 0x20 coreCtlrStackPtr
 *
 * _PRTopEnv  offsets:
 * 0x00 coreCtlrReturnPt
 * 0x100 masterLock
 */
.globl masterSwitchToCoreCtlr
masterSwitchToCoreCtlr:
    #SlaveVP in %rdi
#    movq    $MasterReturn, 0x10(%rdi)   #store return address
    leaq    MasterReturn(%rip), %rcx #resume master by jmp to the "ret" at end
    movq    %rcx      , 0x10(%rdi)   #store addr of ret at end into slave struct
    movq    %rsp      , 0x00(%rdi)   #save stack pointer 
    movq    %rbp      , 0x08(%rdi)   #save frame pointer
    movq    0x20(%rdi), %rsp         #restore stack pointer
    movq    0x18(%rdi), %rbp         #restore frame pointer
#    movq    $_PRTopEnv, %rcx
#    leaq    _PRTopEnv@GOTPCREL(%rip), %rcx #get addr of _PRTopEnv
#    movq        (%rcx), %rcx         #get pointer to struct out of _PRTopEnv
#    movq    0x00(%rcx), %rax         #get content of CoreCtlrReturnAddr field
    leaq    coreCtlrReturn(%rip), %rax #addr of "ret" instr at end of switch-to-slave(master)
    movl    $0x0      , 0x100(%rcx)  #release lock
    jmp     *%rax                    #jmp to "ret" that resumes CoreCtlr
MasterReturn:
    ret     #core ctlr jmps to here to ret into master (addr saved into slave)


//As of ML and Univ versions, this isn't used in PR code.. MasterFn calls
// terminateCoreCtlr directly.. never executes "end slave"s birthFn
//Univ vers will eventually eliminate core controller, and change shut-down
// sequence, removing this..
/*Switch to terminateCoreCtlr
 *This is called by endOSThreadFn, which is the top-level function given
 * to a shutdown slave.  When such a slave gets switched to, by the core
 * controller, it runs the top-level function, which calls this, which
 * then calls terminateCoreCtlr, which ends the pthread.  Note, when get
 * here, stack is already set up for switchSlv and Slv ptr is in %rdi.
 *Do not save registers of Slv because this function will never return
 *
 * SlaveVP  offsets:
 * 0x00  stackPtr
 * 0x08 framePtr
 * 0x10 resumeInstrPtr
 * 0x18 coreCtlrFramePtr
 * 0x20 coreCtlrStackPtr
 *
 * _PRTopEnv  offsets:
 * 0x00 coreCtlrReturnPt
 * 0x100 masterLock
 */
.globl asmTerminateCoreCtlr
asmTerminateCoreCtlr:                #SlaveVP ptr is in %rdi
    movq    0x20(%rdi), %rsp         #restore core ctlr stack pointer
    movq    0x18(%rdi), %rbp         #restore core ctlr frame pointer
    #note: terminateCoreCtlr takes a pointer to slave as param.. ignoring that..
#    movq    $terminateCoreCtlr, %rax
#    leaq    terminateCoreCtlr@GOTPCREL(%rip), %rax #addr of Fn that ends pthread
    leaq    terminateCoreCtlr@PLT(%rip), %rax #addr of Fn that ends pthread
    jmp     *%rax                    #jmp to fn that ends the pthread


/*
 * This one for the sequential version is special. It restores core ctlr stack
 *  then returns from the call to core ctlr Fn
 */
.globl asmTerminateCoreCtlrSeq
asmTerminateCoreCtlrSeq:
    #special "end slave" is in %rdi
    movq    0x20(%rdi), %rsp         #restore core ctlr stack pointer
    movq    0x18(%rdi), %rbp         #restore core ctlr frame pointer
    call    *PR_int__free_slaveVP@PLT(%rip) #"*" indicates indirect call
    movq    %rbp      , %rsp        #remove core ctlr's frame
    pop     %rbp        #restore the old framepointer
    ret                 #return ends the seq call to coreCtlr == ends proto-runtime
    
//As of ML and Univ versions, this isn't used in PR code.. 
//Takes the return addr off the stack and saves into the loc pointed to by
// by the parameter passed in via rdi.  Return addr is at 0x8(%rbp) for 64bit
.globl PR_int__save_return_into_ptd_to_loc_then_do_ret
PR_int__save_return_into_ptd_to_loc_then_do_ret:
    movq 0x08(%rbp),   %rax  #get ret address, rbp is the same as in the calling function
    movq      %rax,   (%rdi) #write ret addr into addr passed as param field
    ret


//As of ML and Univ versions, this isn't used in PR code.. 
//Assembly code changes the return addr on the stack to the one
// pointed to by the parameter, then returns. Stack's return addr is at 0x8(%rbp)
.globl PR_int__return_to_addr_in_ptd_to_loc
PR_int__return_to_addr_in_ptd_to_loc:
    movq   (%rdi),     %rax  #get return addr from addr passed as param
    movq    %rax, 0x08(%rbp) #write return addr to the stack of the caller
    ret                      #ret causes jump to that just-saved addr

