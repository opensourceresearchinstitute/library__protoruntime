/*
 *  Copyright 2009 OpenSourceResearchInstitute.org
 *  Licensed under GNU General Public License version 2
 *
 * Author: seanhalle@yahoo.com
 * 
 */

#ifndef  _PR_LANG_SPEC_DEFS_H
#define	_PR_LANG_SPEC_DEFS_H



//===================  Language-specific Measurement Stuff ===================
//
//TODO:  move these into the language implementation directories
//


//===========================================================================
//VCilk

#ifdef VCILK

/*These defines are used in the macros below*/
#define spawnHistIdx      1 //note: starts at 1
#define syncHistIdx       2

#define MEAS__Make_Meas_Hists_for_VCilk( slave, magicNum ) \
 do \
 { VCilkLangEnv * \
   langEnv = PR_PI__get_lang_env_from_slave( slave, magicNum ); \ 
   langEnv->measHistsInfo = \
          makePrivDynArrayOfSize( (void***)&(_PRTopEnv->measHists), 200); \
   histInfo = langEnv->measHistsInfo;
    makeAMeasHist( histInfo, spawnHistIdx,      "Spawn",        50, 0, 200 ) \
    makeAMeasHist( histInfo, syncHistIdx,       "Sync",         50, 0, 200 ) \
 }while(FALSE); /* macro magic to protect local vars from name collision */

#define Meas_startSpawn fixme; /* changed names -- added __Cilk to end*/
    
#define Meas_startSpawn__Cilk \
    int32 startStamp, endStamp; \
    saveLowTimeStampCountInto( startStamp ); \

#define Meas_endSpawn__Cilk \
    saveLowTimeStampCountInto( endStamp ); \
    addIntervalToHist( startStamp, endStamp, \
                             _PRTopEnv->measHists[ spawnHistIdx ] );

#define Meas_startSync__Cilk \
    int32 startStamp, endStamp; \
    saveLowTimeStampCountInto( startStamp ); \

#define Meas_endSync__Cilk \
    saveLowTimeStampCountInto( endStamp ); \
    addIntervalToHist( startStamp, endStamp, \
                             _PRTopEnv->measHists[ syncHistIdx ] );
#endif

//===========================================================================

#endif	/* _PR_DEFS_H */

