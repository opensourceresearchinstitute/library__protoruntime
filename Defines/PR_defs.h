/*
 *  Copyright 2009 OpenSourceResearchInstitute.org
 *  Licensed under GNU General Public License version 2
 *
 * Author: seanhalle@yahoo.com
 * 
 */

#ifndef  _PR_DEFS_MAIN_H
#define	_PR_DEFS_MAIN_H
#define _GNU_SOURCE

//===========================  PR-wide defs  ===============================

#define SUCCESS 0

   //only after macro-expansion are the defs of writePrivQ, aso looked up
   // so these defs can be at the top, and writePrivQ defined later on..
#define writePRQ     writePrivQ
#define readPRQ      readPrivQ
#define makePRQ      makePrivQ
#define numInPRQ     numInPrivQ
#define PRQueueStruc PrivQueueStruc


/*The language should re-define this, but need a default in case it doesn't*/
#ifndef _LANG_NAME_
#define _LANG_NAME_ ""
#endif

//======================  Hardware Constants ============================
#include "PR_defs__HW_constants.h"

//======================  Macros  ======================
   //for turning macros and other PR features on and off
#include "../PR_defs__turn_on_and_off.h"

//#include "../Services_Offered_by_PR/Debugging/DEBUG__macros.h"
//#include "../Services_Offered_by_PR/Measurement_and_Stats/MEAS__macros.h"

//===========================================================================
#endif	/*  */

