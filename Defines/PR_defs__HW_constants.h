/*
 *  Copyright 2012 OpenSourceResearchInstitute
 *  Licensed under BSD
 *
 * Author: seanhalle@yahoo.com
 * 
 */

#ifndef _PR_HW_SPEC_DEFS_H
#define	_PR_HW_SPEC_DEFS_H
#define _GNU_SOURCE


//=========================  Hardware related Constants =====================

   //number of PRLangEnv structs created inside a process -- can't start more
   // than this many langlets inside a single process
#define NUM_IN_COLLECTION 64

   //These are for backoff inside core-loop, which reduces lock contention
#define NUM_REPS_W_NO_WORK_BEFORE_YIELD      10
#define NUM_REPS_W_NO_WORK_BEFORE_BACKOFF    2
#define MASTERLOCK_RETRIES_BEFORE_YIELD      100
#define NUM_TRIES_BEFORE_DO_BACKOFF          10
#define GET_LOCK_BACKOFF_WEIGHT 100
   
   // stack size in virtual processors created
#define VIRT_PROCR_STACK_SIZE 0x8000 /* 32K */

   // memory for PR_int__malloc
#define MALLOC_ADDITIONAL_MEM_FROM_OS_SIZE 0x8000000 /* 128M */

   //Frequency of TS counts -- have to do tests to verify
   //NOTE: turn off (in BIOS)  TURBO-BOOST and SPEED-STEP else won't be const
#define TSCOUNT_FREQ 3180000000
#define TSC_LOW_CYCLES 27
#define TSC_LOWHI_CYCLES 45

#define CACHE_LINE_SZ  256
#define PAGE_SIZE     4096

//To prevent false-sharing, aligns a variable to a cache-line boundary.
//No need to use for local vars because those are never shared between cores
#define __align_to_cacheline__ __attribute__ ((aligned(CACHE_LINE_SZ)))

//aligns a pointer to cacheline. The memory area has to contain at least
//CACHE_LINE_SZ bytes more then needed
#define __align_address(ptr) ((void*)(((uintptr_t)(ptr))&((uintptr_t)(~0x0FF))))

//===========================================================================

#endif	/* _PR_DEFS_H */

