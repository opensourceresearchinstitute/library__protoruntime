/*
 *  Copyright 2009 OpenSourceStewardshipFoundation.org
 *  Licensed under GNU General Public License version 2
 *
 * Author: seanhalle@yahoo.com
 * 
 */


/*NOTE: this file should only be included AFTER some other 
 * file has defined which kind of probe to use, and whether
 * probes are turned on or off..
 *
 *In other words, this is a static library that uses macros,
 * and there are multiple versions of the same macro.. which
 * version gets used depends on #define statements in a
 * DIFFERENT file..  (traditionally PR_defs__turn_on_and_off.h)
 *
 *So, this file relies on #defines that appear in other files,
 * which must come first in the sequence of #includes that 
 * include this one..
 */

#ifndef _PROBES_INT_H
#define	_PROBES_INT_H
#define _GNU_SOURCE

#include <PR__include/PR__structs__common.h>

#include <PR__include/Services_offered_by_PR/probes__wrapper_library.h>

#include <sys/time.h>
#include <PR__include/prhistogram.h>

//==========================
#ifdef PROBES__USE_TSC_PROBES
   #define PROBES__Insert_timestamps_and_intervals_into_probe_struct \
   TSCount    startStamp; \
   TSCount    endStamp; \
   TSCount    interval; \
   Histogram *hist; /*if left NULL, then is single interval probe*/
#endif
#ifdef PROBES__USE_TIME_OF_DAY_PROBES
   #define PROBES__Insert_timestamps_and_intervals_into_probe_struct \
   struct timeval  startStamp; \
   struct timeval  endStamp; \
   float64         startSecs; \
   float64         endSecs; \
   float64         interval; \
   DblHist        *hist; /*if NULL, then is single interval probe*/
#endif
#ifdef PROBES__USE_PERF_CTR_PROBES
   #define PROBES__Insert_timestamps_and_intervals_into_probe_struct \
   int64  startStamp; \
   int64  endStamp; \
   int64  interval; \
   Histogram *hist; /*if left NULL, then is single interval probe*/
#endif

//typedef struct _IntervalProbe IntervalProbe; -- is in PR.h
struct _IntervalProbe
 {
   char           *nameStr;
   int32           probeID;

   int32           schedChoiceWasRecorded;
   int32           coreNum;
   int32           slaveNum;
   float64         slaveCreateSecs;
   PROBES__Insert_timestamps_and_intervals_into_probe_struct;
 };

//=========================== NEVER USE THESE ==========================
/*NEVER use these in any code!!  These are here only for use in the macros
 * defined in this file!!
 */
int32
PR_impl__create_single_interval_probe( char *nameStr, SlaveVP *animSlv );

int32
PR_impl__create_histogram_probe( int32   numBins, float64    startValue,
               float64 binWidth, char    *nameStr, SlaveVP *animSlv );

int32
PR_impl__record_time_point_into_new_probe( char *nameStr, SlaveVP *animSlv);

int32
PR_ext_impl__record_time_point_into_new_probe( char *nameStr );

void
PR_impl__free_probe( IntervalProbe *probe );

void
PR_impl__index_probe_by_its_name( int32 probeID, SlaveVP *animSlv );

IntervalProbe *
PR_impl__get_probe_by_name( char *probeName, SlaveVP *animSlv );

void
PR_impl__record_sched_choice_into_probe( int32 probeID, SlaveVP *animSlv );

void
PR_impl__record_interval_start_in_probe( int32 probeID );

void
PR_impl__record_interval_end_in_probe( int32 probeID );

void
PR_impl__print_stats_of_probe( IntervalProbe *probe );

void
PR_impl__print_stats_of_all_probes();


//======================== Probes =============================
//
// Use macros to allow turning probes off with a #define switch
// This means probes have zero impact on performance when off
//=============================================================

#ifdef PROBES__TURN_ON_STATS_PROBES

   #define PROBES__Create_Probe_Bookkeeping_Vars \
      _PRTopEnv->dynIntervalProbesInfo = \
       makePrivDynArrayOfSize( (void***)&(_PRTopEnv->intervalProbes), 200); \
      \
      _PRTopEnv->probeNameHashTbl = makeHashTable( 1000, &PR_int__free ); \
      \
      /*put creation time directly into master env, for fast retrieval*/ \
   struct timeval timeStamp; \
   gettimeofday( &(timeStamp), NULL); \
   _PRTopEnv->createPtInSecs = \
                           timeStamp.tv_sec +(timeStamp.tv_usec/1000000.0);

   #define PR_WL__record_time_point_into_new_probe( nameStr, animSlv ) \
           PR_impl__record_time_point_in_new_probe( nameStr, animSlv )

   #define PR_ext__record_time_point_into_new_probe( nameStr ) \
           PR_ext_impl__record_time_point_into_new_probe( nameStr )

   #define PR_WL__create_single_interval_probe( nameStr, animSlv ) \
           PR_impl__create_single_interval_probe( nameStr, animSlv )

   #define PR_WL__create_histogram_probe(      numBins, startValue,              \
                                             binWidth, nameStr, animSlv )       \
           PR_impl__create_histogram_probe( numBins, startValue,              \
                                             binWidth, nameStr, animSlv )
   #define PR_int__free_probe( probe ) \
           PR_impl__free_probe( probe )

   #define PR_WL__index_probe_by_its_name( probeID, animSlv ) \
           PR_impl__index_probe_by_its_name( probeID, animSlv )

   #define PR_WL__get_probe_by_name( probeID, animSlv ) \
           PR_impl__get_probe_by_name( probeName, animSlv )

   #define PR_WL__record_sched_choice_into_probe( probeID, animSlv ) \
           PR_impl__record_sched_choice_into_probe( probeID, animSlv )

   #define PR_WL__record_interval_start_in_probe( probeID ) \
           PR_impl__record_interval_start_in_probe( probeID )

   #define PR_WL__record_interval_end_in_probe( probeID ) \
           PR_impl__record_interval_end_in_probe( probeID )

   #define PR_WL__print_stats_of_probe( probeID ) \
           PR_impl__print_stats_of_probe( probeID )

   #define PR_WL__print_stats_of_all_probes() \
           PR_impl__print_stats_of_all_probes()


#else
   #define PROBES__Create_Probe_Bookkeeping_Vars
   #define PR_WL__record_time_point_into_new_probe( nameStr, animSlv ) 0 /* do nothing */
   #define PR_ext__record_time_point_into_new_probe( nameStr )  0 /* do nothing */
   #define PR_WL__create_single_interval_probe( nameStr, animSlv ) 0 /* do nothing */
   #define PR_WL__create_histogram_probe( numBins, startValue,              \
                                             binWidth, nameStr, animSlv )       \
          0 /* do nothing */
   #define PR_WL__index_probe_by_its_name( probeID, animSlv ) /* do nothing */
   #define PR_WL__get_probe_by_name( probeID, animSlv ) NULL /* do nothing */
   #define PR_WL__record_sched_choice_into_probe( probeID, animSlv ) /* do nothing */
   #define PR_WL__record_interval_start_in_probe( probeID )  /* do nothing */
   #define PR_WL__record_interval_end_in_probe( probeID )  /* do nothing */
   #define PR_WL__print_stats_of_probe( probeID ) ; /* do nothing */
   #define PR_WL__print_stats_of_all_probes() ;/* do nothing */

#endif   /* defined PROBES__TURN_ON_STATS_PROBES */

#endif	/* _PROBES_H */

