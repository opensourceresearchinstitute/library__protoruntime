/*
 * Copyright 2010  OpenSourceStewardshipFoundation
 *
 * Licensed under BSD
 */

#include <stdio.h>
#include <malloc.h>
#include <sys/time.h>

#include <PR__include/PR__structs__common.h>
#include <PR__include/PR__int.h>
#include <PR__include/Services_offered_by_PR/probes__wrapper_library.h>

#include "../../PR__structs__int.h"
#include "../../PR_defs__turn_on_and_off.h" //NOTE: defines syms used in probes__int.h
#include "probes__int.h"


//====================  Probes =================
/*
 * In practice, probe operations are called from the app, from inside slaves
 *  -- so have to be sure each probe is single-Slv owned, and be sure that
 *  any place common structures are modified it's done inside the master.
 * So -- the only place common structures are modified is during creation.
 *  after that, all mods are to individual instances.
 *
 * Thniking perhaps should change the semantics to be that probes are
 *  attached to the virtual processor -- and then everything is guaranteed
 *  to be isolated -- except then can't take any intervals that span Slvs,
 *  and would have to transfer the probes to Master env when Slv dissipates..
 *  gets messy..
 *
 * For now, just making so that probe creation causes a suspend, so that
 *  the dynamic array in the master env is only modified from the master
 * 
 */



/*Only call from inside master or main startup/shutdown thread
 */
void
PR_impl__free_probe( IntervalProbe *probe )
 { if( probe->hist != NULL )   freeDblHist( probe->hist );
   if( probe->nameStr != NULL) PR_int__free( probe->nameStr );
   PR_int__free( probe );
 }


void
PR_impl__index_probe_by_its_name( int32 probeID, SlaveVP *animSlv )
 { IntervalProbe *probe;

   PR_int__get_wrapper_lock();
   probe = _PRTopEnv->intervalProbes[ probeID ];

   addValueIntoTable(probe->nameStr, probe, _PRTopEnv->probeNameHashTbl);
   PR_int__release_wrapper_lock();
 }


IntervalProbe *
PR_impl__get_probe_by_name( char *probeName, SlaveVP *animSlv )
 {
   //TODO: fix this To be in Master -- race condition
   return getValueFromTable( probeName, _PRTopEnv->probeNameHashTbl );
 }


/*Everything is local to the animating slaveVP, so no need for request, do
 * work locally, in the anim Slv
 */
void
PR_impl__record_sched_choice_into_probe( int32 probeID, SlaveVP *animatingSlv )
 { IntervalProbe *probe;
 
   probe = _PRTopEnv->intervalProbes[ probeID ];
   probe->schedChoiceWasRecorded = TRUE;
   probe->coreNum = animatingSlv->coreAnimatedBy;
   probe->slaveNum = animatingSlv->slaveNum;
   probe->slaveCreateSecs = animatingSlv->createPtInSecs;
 }

/*Everything is local to the animating slaveVP, so no need for request, do
 * work locally, in the anim Slv
 */
void
PR_impl__record_interval_start_in_probe( int32 probeID )
 { IntervalProbe *probe;

         DEBUG__printf( dbgProbes, "record start of interval" )
   probe = _PRTopEnv->intervalProbes[ probeID ];

      //record *start* point as last thing, after lookup
#ifdef PROBES__USE_TIME_OF_DAY_PROBES
   gettimeofday( &(probe->startStamp), NULL);
#endif
#ifdef PROBES__USE_TSC_PROBES
   probe->startStamp = getTSCount();
#endif
 }


/*Everything is local to the animating slaveVP, except the histogram, so do
 * work locally, in the anim Slv -- may lose a few histogram counts
 * 
 *This should be safe to run inside SlaveVP
 */
void
PR_impl__record_interval_end_in_probe( int32 probeID )
 { IntervalProbe *probe;

   //Record first thing -- before looking up the probe to store it into
#ifdef PROBES__USE_TIME_OF_DAY_PROBES
   struct timeval  endStamp;
   gettimeofday( &(endStamp), NULL);
#endif
#ifdef PROBES__USE_TSC_PROBES
   TSCount endStamp, interval;
   endStamp = getTSCount();
#endif
#ifdef PROBES__USE_PERF_CTR_PROBES

#endif
   
   probe = _PRTopEnv->intervalProbes[ probeID ];

#ifdef PROBES__USE_TIME_OF_DAY_PROBES
   if( probe->hist != NULL )
    { addToDblHist( giveInterval( probe->startStamp, endStamp), probe->hist );
    }
#endif
#ifdef PROBES__USE_TSC_PROBES
   if( probe->hist != NULL )
    { interval = probe->endStamp - probe->startStamp;
         //Sanity check for TSC counter overflow: if sane, add to histogram
      if( interval < probe->hist->endOfRange * 10 )
         addToHist( interval, probe->hist );
    }
#endif
#ifdef PROBES__USE_PERF_CTR_PROBES

#endif
   
         DEBUG__printf( dbgProbes, "record end of interval" )
 }


void
print_probe_helper( IntervalProbe *probe )
 {
   printf( "\nprobe: %s, ",  probe->nameStr );
   
   
   if( probe->schedChoiceWasRecorded )
    { printf( "coreNum: %d, slaveNum: %d, slaveVPCreated: %0.6f | ",
              probe->coreNum, probe->slaveNum, probe->slaveCreateSecs );
    }

   if( probe->endSecs == 0 ) //just a single point in time
    {
      printf( " time point: %.6f\n",
              probe->startSecs - _PRTopEnv->createPtInSecs );
    }
   else if( probe->hist == NULL ) //just an interval
    {
      printf( " startSecs: %.6f interval: %.6f\n", 
         (probe->startSecs - _PRTopEnv->createPtInSecs), probe->interval);
    }
   else  //a full histogram of intervals
    {
      printDblHist( probe->hist );
    }
 }

void
PR_impl__print_stats_of_probe( IntervalProbe *probe )
 { 

//   probe = _PRTopEnv->intervalProbes[ probeID ];

   print_probe_helper( probe );
 }


void
PR_impl__print_stats_of_all_probes()
 {
   forAllInDynArrayDo( _PRTopEnv->dynIntervalProbesInfo,
                          (DynArrayFnPtr) &PR_impl__print_stats_of_probe );
   fflush( stdout );
 }
