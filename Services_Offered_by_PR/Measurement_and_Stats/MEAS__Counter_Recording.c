/*
 * 
 * author: Nina Engelhardt
 */

#include <PR__include/Services_offered_by_PR/MEAS__Counter_Recording.h>

//#include "PR__common_includes/PR__common_structs.h"
//#include "PRServ__wrapper_library/PRServ__wrapper_library.h"

#ifdef HOLISTIC__TURN_ON_PERF_COUNTERS
int32 counter_lock; //use standard PR method to acquire this lock

void 
MEAS__init_counter_data_structs_for_Lang( SlaveVP *slave, int32 magicNum )
 {
   PRServLangEnv *langEnv = 
                (PRServLangEnv *)PR_SS__give_lang_env_for_slave( slave, magicNum );
   int i;
   for(i=0;i<_PRTopEnv->num_cores;i++)
    { langEnv->counterList[i] = makeListOfArrays(sizeof(CounterEvent), 128);
    }
 }

/*Pass file by side effect..
 *The reason is that using doAllInListOfArrays, to which one passes the pointer
 * to a function, which is then applied to all the elements..  but, that fn
 * can only take one input -- the element from the list of arrays..  so, it
 * can't also be passed the file..  hence pass the file by side effect
 * 
 *However, multiple cores could be trying to do this..  so, create a separate
 * lock just for counters, and acquire that when set the file, then release
 * once all the printing is done.
 */
void 
MEAS__set_counter_file(FILE* f)
 {
   fixme; //impl generic "acquire lock" and pass it the counter-lock addr.. 
   acquire counter lock
   counterfile = f;
 }

MEAS__release_counter_file()
 {
   fixme; //impl generic "release lock" and pass it the counter-lock addr
   release counter lock
 }

void 
MEAS__addToListOfArraysCounterEvent(CounterEvent value, ListOfArrays* list)
 {
   fixme; //figure out if this needs to be protected by a lock, along with rest of this file
   int offset_in_fragment = list->next_free_index % list->num_entries_per_fragment; 
   if(offset_in_fragment == 0)
    { void* newBlock = malloc(list->entry_size * list->num_entries_per_fragment); 
     addToDynArray(newBlock,list->dim1info); 
    } 
   CounterEvent* typedFragment = (CounterEvent*) ((list->dim1)[list->dim1info->numInArray -1]); 
   typedFragment[offset_in_fragment] = value; 
   list->next_free_index++; 
 }

void 
MEAS__counter_handler(int evt_type, int vpid, int task, SlaveVP* pr, uint64 cycles, uint64 instrs)
 {
   fixme; //figure out if this needs to be protected by a lock, along with rest of this file
   if (pr->typeOfVP == Master_VP || pr->typeOfVP == ShutdownVP)
    { //Only save values for application work, done in a SlaveVP
      return;
    }

   //Note: have made many changes without compiler flag turned on, nor test..
   PRLangEnv *langEnv = 
      PR_int__give_proto_lang_env_from_slave( pr, magicNum);

   CounterEvent e;
   e.event_type = evt_type;
   e.vp = vpid;
   e.task = task;

   e.cycles = cycles;
   e.instrs = instrs;

   if(pr)
    { e.coreID = pr->coreAnimatedBy;
      e.slot = pr->animSlotAssignedTo;
    } 
   else 
    { e.coreID = -1;
      e.slot = NULL;
    }

   int corenum;

   if(pr) 
      corenum = pr->coreAnimatedBy; 
   else 
      return; 

   if(evt_type==Work_start || evt_type==Work_end || evt_type==AppResponderInvocation_start)
    { addToListOfArrays_ext(CounterEvent,e,langEnv->counterList[corenum]);
    } 
   else 
    { MEAS__addToListOfArraysCounterEvent(e,langEnv->counterList[corenum]);
    }
 }



void 
MEAS__print_counter_event_to_file( void* _e )
 {
   CounterEvent* e = (CounterEvent*) _e;
   fprintf(counterfile, "event, ");
   switch(e->event_type)
    {
      case AppResponderInvocation_start:
          fprintf(counterfile, "AppResponderInvocation_start");
          break;
      case AppResponder_start:
          fprintf(counterfile, "AppResponder_start");
          break;
      case AppResponder_end:
          fprintf(counterfile, "AppResponder_end");
          break;
      case AssignerInvocation_start:
          fprintf(counterfile, "AssignerInvocation_start");
          break;
      case NextAssigner_start:
          fprintf(counterfile, "NextAssigner_start");
          break;
      case Assigner_start:
          fprintf(counterfile, "Assigner_start");
          break;
      case Assigner_end:
          fprintf(counterfile, "Assigner_end");
          break;
      case Work_end:
          fprintf(counterfile, "Work_end");
          break;
      case Work_start:
          fprintf(counterfile, "Work_start");
          break;
      case HwResponderInvocation_start:
          fprintf(counterfile, "HwResponderInvocation_start");
          break;
      case Timestamp_start:
          fprintf(counterfile, "Timestamp_start");
          break;
      case Timestamp_end:
          fprintf(counterfile, "Timestamp_end");
          break;
      default:
          fprintf(counterfile, "unknown event");
    }
   fprintf(counterfile,", %d, %d, %llu, %llu",e->vp,e->task,e->cycles,e->instrs);
   if(e->coreID >=0)
   fprintf(counterfile,", %d",e->coreID);
   fprintf(counterfile,"\n");
   fflush(counterfile);
 }
#endif
