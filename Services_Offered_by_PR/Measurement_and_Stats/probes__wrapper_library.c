/*
 * Copyright 2010  OpenSourceStewardshipFoundation
 *
 * Licensed under BSD
 */

#include <stdio.h>
#include <malloc.h>
#include <sys/time.h>

#include <PR__include/Services_offered_by_PR/probes__wrapper_library.h>

//NOTE: this defines symbols used inside probes__int.h
#include "../../PR_defs__turn_on_and_off.h"

#include "probes__int.h"

//====================  Probes =================
/*
 * In practice, probe operations are called from the app, from inside slaves
 *  -- so have to be sure each probe is single-Slv owned, and be sure that
 *  any place common structures are modified it's done inside the master.
 * So -- the only place common structures are modified is during creation.
 *  after that, all mods are to individual instances.
 *
 * Thniking perhaps should change the semantics to be that probes are
 *  attached to the virtual processor -- and then everything is guaranteed
 *  to be isolated -- except then can't take any intervals that span Slvs,
 *  and would have to transfer the probes to Master env when Slv dissipates..
 *  gets messy..
 *
 * For now, just making so that probe creation causes a suspend, so that
 *  the dynamic array in the master env is only modified from the master
 * 
 */

//============================  Helpers ===========================
inline void 
doNothing()
 {
 }

float64 inline
giveInterval( struct timeval _start, struct timeval _end )
 { float64 start, end;
   start = _start.tv_sec + _start.tv_usec / 1000000.0;
   end   = _end.tv_sec   + _end.tv_usec   / 1000000.0;
   return end - start;
 }
          
//=================================================================
IntervalProbe *
create_generic_probe( char *nameStr, SlaveVP *animSlv )
 {
   PRServiceReq reqData;

   reqData.reqType  = make_probe;
   reqData.nameStr  = nameStr;

   PR_WL__send_service_request( &reqData, animSlv );

   return animSlv->dataRetFromReq;
 }

/*Use this version from outside PR -- it uses external malloc, and modifies
 * dynamic array, so can't be animated in a slave Slv
 */
/* Can't access _PRTopEnv in a wrapper library
IntervalProbe *
ext__create_generic_probe( char *nameStr )
 { IntervalProbe *newProbe;
   int32          nameLen;

   newProbe          = malloc( sizeof(IntervalProbe) );
   nameLen = strlen( nameStr );
   newProbe->nameStr = malloc( nameLen );
   memcpy( newProbe->nameStr, nameStr, nameLen );
   newProbe->hist    = NULL;
   newProbe->schedChoiceWasRecorded = FALSE;
   newProbe->probeID =
             addToDynArray( newProbe, _PRTopEnv->dynIntervalProbesInfo );

   return newProbe;
 }
*/
//============================ Fns def in header =======================

int32
PR_impl__create_single_interval_probe( char *nameStr, SlaveVP *animSlv )
 { IntervalProbe *newProbe;

   newProbe = create_generic_probe( nameStr, animSlv );
   
   return newProbe->probeID;
 }

int32
PR_impl__create_histogram_probe( int32   numBins, float64    startValue,
               float64 binWidth, char   *nameStr, SlaveVP *animSlv )
 { IntervalProbe *newProbe;

   newProbe = create_generic_probe( nameStr, animSlv );
   
#ifdef PROBES__USE_TIME_OF_DAY_PROBES
   DblHist *hist;
   hist =  makeDblHistogram( numBins, startValue, binWidth );
#else
   Histogram *hist;
   hist =  makeHistogram( numBins, startValue, binWidth );
#endif
   newProbe->hist = hist;
   return newProbe->probeID;
 }


int32
PR_impl__record_time_point_into_new_probe( char *nameStr, SlaveVP *animSlv)
 { IntervalProbe *newProbe;
   struct timeval *startStamp;
   float64 startSecs;

   newProbe           = create_generic_probe( nameStr, animSlv );
   newProbe->endSecs  = 0;

   
   gettimeofday( &(newProbe->startStamp), NULL);

      //turn into a double
   startStamp = &(newProbe->startStamp);
   startSecs = startStamp->tv_sec + ( startStamp->tv_usec / 1000000.0 );
   newProbe->startSecs = startSecs;

   return newProbe->probeID;
 }

/*
int32
PR_ext_impl__record_time_point_into_new_probe( char *nameStr )
 { IntervalProbe *newProbe;
   struct timeval *startStamp;
   float64 startSecs;

   newProbe           = ext__create_generic_probe( nameStr );
   newProbe->endSecs  = 0;

   gettimeofday( &(newProbe->startStamp), NULL);

      //turn into a double
   startStamp = &(newProbe->startStamp);
   startSecs = startStamp->tv_sec + ( startStamp->tv_usec / 1000000.0 );
   newProbe->startSecs = startSecs;

   return newProbe->probeID;
 }
*/
