/*
 *  Copyright 2009 OpenSourceResearchInstitute.org
 *  Licensed under GNU General Public License version 2
 *
 * Author: seanhalle@yahoo.com
 *
 */

#ifndef _PRServ_int_H
#define	_PRServ_int_H

#include <PR__include/prqueue.h>
#include <PR__include/prhash.h>

#include <PR__include/PR__structs__common.h>
#include <PR__include/PR__WL.h>
#include <PR__include/PR__int.h>

#include "../../Defines/PR_defs__HW_constants.h"
#include "../../PR__structs__int.h" //has structs not in pr__common_structs.h

#include <PR__include/langlets/PRServ__wrapper_library.h>

//===========================================================================

//uniquely identifies PRServ -- should be a jenkins char-hash of "PRServ" to int32
#define PRServ_MAGIC_NUMBER 0000000001

#define NUM_STRUCS_IN_LANG_ENV 1000

   //This is hardware dependent -- it's the number of cycles of scheduling
   // overhead -- if a work unit is fewer than this, it is better being
   // combined sequentially with other work
   //This value depends on both PR overhead and PRServ's plugin.  At some point
   // it will be derived by perf-counter measurements during init of PRServ
#define MIN_WORK_UNIT_CYCLES 20000

//===========================================================================
/*This header defines everything specific to the PRServ semantic plug-in
 */
typedef struct _PRServLangReq    PRServLangReq;
//===========================================================================

/*Semantic layer-specific data sent inside a request from lib called in app
 * to request handler called in AnimationMaster
 */

typedef struct
 {
   SlaveVP      *VPCurrentlyExecuting;
   PrivQueueStruc *waitingVPQ;
 }
PRServTrans;


struct _PRServLangReq
 { 
   SlaveVP           *callingSlv;

   int32              singletonID;
   PRServSingleton     **singletonPtrAddr;

   PtrToAtomicFn      fnToExecInMaster;
   void              *dataForFn;

   int32              transID;
   
   DKUPiece          *dkuPiece;
   DKUInstance       *dkuInstance;
 }
/* PRServLangReq */;


typedef struct
 { 
   PrivQueueStruc  *slaveReadyQ; //Shared (slaves not pinned)
   PrivQueueStruc  *taskReadyQ;           //will never be used in seed langlet
   
   int32            primitiveStartTime;

                       //fix limit on num with dynArray
   PRServSingleton     fnSingletons[NUM_STRUCS_IN_LANG_ENV];
   PRServTrans         transactionStrucs[NUM_STRUCS_IN_LANG_ENV];
   
   #ifdef HOLISTIC__TURN_ON_OBSERVE_UCC
   ListOfArrays* unitList;
   ListOfArrays* ctlDependenciesList;
   ListOfArrays* commDependenciesList;
   NtoN** ntonGroups;
   PrivDynArrayInfo* ntonGroupsInfo;
   ListOfArrays* dynDependenciesList;
   Unit *last_in_slot = 
      PR__malloc( _PRTopEnv->num_cores * NUM_ANIM_SLOTS *sizeof(Unit));
   ListOfArrays* hwArcs;
   #endif

   #ifdef HOLISTIC__TURN_ON_PERF_COUNTERS
   ListOfArrays* *counterList = PR__malloc(_PRTopEnv->num_cores * sizeof(ListOfArrays *));
   #endif
 }
PRServLangEnv;


typedef struct _PRServTransListElem PRServTransListElem;
struct _PRServTransListElem
 {
   int32          transID;
   PRServTransListElem *nextTrans;
 };
//TransListElem
 
typedef struct
 {
   int32            highestTransEntered;
   PRServTransListElem   *lastTransEntered;
   int32            primitiveStartTime;
 }
PRServLangData;

typedef struct
 {
   DKUPiece *dkuPiece; //the dku piece operated upon by a (serial) kernel task
 }
PRServMetaTask;
//===========================================================================

//=========================  Internal use only  =============================

PRServLangEnv *
PRServ__start( SlaveVP *seedSlv );

void
PRServ__end();

bool32
PRServ__assign_work_to_slot( void *_langEnv, AnimSlot *slot );
   
//=====================    =====================

#include "PRServ_Request_Handlers.h"

//=====================  Measurement of Lang Overheads  =====================
#include "Measurement/PRServ_Measurement.h"

//===========================================================================
#endif	/* _PRServ_H */

