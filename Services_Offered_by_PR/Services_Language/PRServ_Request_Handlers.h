/*
 *  Copyright 2009 OpenSourceResearchInstitute.org
 *  Licensed under GNU General Public License version 2
 *
 * Author: seanhalle@yahoo.com
 *
 */

#ifndef _PRServ_REQ_H
#define	_PRServ_REQ_H

#include "PRServ_int.h"

/*This header defines everything specific to the PRServ semantic plug-in
 */
#define NO_ID  NULL

//============================================
void
PRServ__handleStartFnSingleton( PRServLangReq *langReq, SlaveVP *reqstingSlv,
                      PRServLangEnv *langEnv );
void
PRServ__handleEndFnSingleton( PRServLangReq *langReq, SlaveVP *requestingSlv,
                    PRServLangEnv *langEnv );
inline void
PRServ__handleStartDataSingleton( PRServLangReq *langReq, SlaveVP *reqstingSlv,
                      PRServLangEnv *langEnv );
inline void
PRServ__handleEndDataSingleton( PRServLangReq *langReq, SlaveVP *requestingSlv,
                    PRServLangEnv *langEnv );
void
PRServ__handleTransStart( PRServLangReq *langReq, SlaveVP *requestingSlv,
                  PRServLangEnv *langEnv );
void
PRServ__handleTransEnd(PRServLangReq *langReq, SlaveVP *requestingSlv, PRServLangEnv*langEnv);

void
PRServ__handleAtomic( PRServLangReq *langReq, SlaveVP *requestingSlv, PRServLangEnv *langEnv);

//==================================
void
PRServ__make_task_ready( void *taskStub, void *_langEnv );
void
PRServ__resume_slave( SlaveVP *slave, void *langEnv );

void
PRServ__handleDissipateSeed(void *langReq, SlaveVP *seedSlv, PRServLangEnv *langEnv );

void
PRServ__handle_end_process_from_inside( void *langReq, SlaveVP *reqSlave, PRServLangEnv *langEnv );

void
PRServ__handle_shutdown( void *_langEnv );

void * 
PRServ__create_lang_data_in_slave( SlaveVP *slave );

void
PRServ__free_lang_data( void *_langData );

void
PRServ__lang_meta_task_freer( void *_langMetaTask );

//==========================

void
PRServ__handleDKUPerformWork( PRServLangReq *langReq, SlaveVP *requestingSlv, PRServLangEnv *langEnv );

void
PRServ__handleDKUWaitTillDone( PRServLangReq *langReq, SlaveVP *requestingSlv, PRServLangEnv *langEnv );

void
PRServ__handleDKUEndKernelTask( PRServLangReq *langReq, SlaveVP *requestingSlv, PRServLangEnv *langEnv );

void
PRServ__handleDKUEndSerialKernelTask( PRServLangReq *langReq, SlaveVP *requestingSlv, PRServLangEnv *langEnv );

#endif	/* _PRServ_REQ_H */

