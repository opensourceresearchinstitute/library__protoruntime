/*
 * Copyright 2010  OpenSourceCodeStewardshipFoundation
 *
 * Licensed under BSD
 */

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>

#include <PR__include/prqueue.h>
#include <PR__include/prhash.h>

#include "PRServ_int.h"
#include <PR__include/PR__SS.h>  //has the PR_SS fn declarations
//==========================================================================



//===========================================================================

/*
 */
PRServLangEnv *
PRServ__start( SlaveVP *seedSlv )
 { PRServLangEnv       *langEnv;
   int32                i;
   
   langEnv = 
         PR_SS__create_lang_env( sizeof(PRServLangEnv), seedSlv, PRServ_MAGIC_NUMBER);
   
 
      //create the ready queues
   langEnv->slaveReadyQ = makePrivQ();
   langEnv->taskReadyQ           = makePrivQ();
   
      //register the langlet's handlers with PR
   PR_SS__register_assigner(                &PRServ__assign_work_to_slot, seedSlv, PRServ_MAGIC_NUMBER );
   PR_SS__register_lang_shutdown_handler(        &PRServ__handle_shutdown, seedSlv, PRServ_MAGIC_NUMBER );
   PR_SS__register_lang_data_creator(       &PRServ__create_lang_data_in_slave, seedSlv, PRServ_MAGIC_NUMBER );
//   PR_SS__register_lang_meta_task_creator(  &PRServ__create_empty_lang_meta_task_in_slave, seedSlv, PRServ_MAGIC_NUMBER );
   PR_SS__register_make_slave_ready_fn(     &PRServ__resume_slave, seedSlv, PRServ_MAGIC_NUMBER );
   PR_SS__register_make_task_ready_fn(      &PRServ__make_task_ready, seedSlv, PRServ_MAGIC_NUMBER );
   
  #ifdef HOLISTIC__TURN_ON_PERF_COUNTERS
   _PRTopEnv->counterHandler = &PR_MEAS__counter_handler;
   PR_MEAS__init_counter_data_structs_for_lang( seedSlv, PRServ_MAGIC_NUMBER );
  #endif

   
   //TODO: bug -- turn these arrays into dyn arrays to eliminate limit
   //langEnv->singletonHasBeenExecutedFlags = makeDynArrayInfo( );
   //langEnv->transactionStrucs = makeDynArrayInfo( );
   for( i = 0; i < NUM_STRUCS_IN_LANG_ENV; i++ )
    {
      langEnv->fnSingletons[i].endInstrAddr      = NULL;
      langEnv->fnSingletons[i].hasBeenStarted    = FALSE;
      langEnv->fnSingletons[i].hasFinished       = FALSE;
      langEnv->fnSingletons[i].waitQ             = makePrivQ();
      langEnv->transactionStrucs[i].waitingVPQ   = makePrivQ();
    }


   #ifdef HOLISTIC__TURN_ON_OBSERVE_UCC
   langEnv->unitList = makeListOfArrays(sizeof(Unit),128);
   langEnv->ctlDependenciesList = makeListOfArrays(sizeof(Dependency),128);
   langEnv->commDependenciesList = makeListOfArrays(sizeof(Dependency),128);
   langEnv->dynDependenciesList = makeListOfArrays(sizeof(Dependency),128);
   langEnv->ntonGroupsInfo = makePrivDynArrayOfSize((void***)&(langEnv->ntonGroups),8);
   
   langEnv->hwArcs = makeListOfArrays(sizeof(Dependency),128);
   memset(langEnv->last_in_slot,0,sizeof(_PRTopEnv->num_cores * sizeof(Unit)));
   #endif

   return langEnv;
 }


/*This shuts down the langlet
 * Frees any memory allocated by PRServ__init() and deletes any slaves or tasks
 * still in ready Qs.
 */
void
PRServ__handle_shutdown( void *_langEnv )
 { PRServLangEnv *langEnv = (PRServLangEnv *)_langEnv;
   
   #ifdef HOLISTIC__TURN_ON_OBSERVE_UCC
   //UCC
   FILE* output;
   int n;
   char filename[255];    
    for(n=0;n<255;n++)
    {
        sprintf(filename, "./counters/UCC.%d",n);
        output = fopen(filename,"r");
        if(output)
        {
            fclose(output);
        }else{
            break;
        }
    }
   if(n<255){
    printf("Saving UCC to File: %s ...\n", filename);
    output = fopen(filename,"w+");
    if(output!=NULL){
        set_dependency_file(output);
        //fprintf(output,"digraph Dependencies {\n");
        //set_dot_file(output);
        //FIXME:  first line still depends on counters being enabled, replace w/ unit struct!
        //forAllInDynArrayDo(_PRTopEnv->counter_history_array_info, &print_dot_node_info );
        forAllInListOfArraysDo(langEnv->unitList, &print_unit_to_file);
        forAllInListOfArraysDo( langEnv->commDependenciesList, &print_comm_dependency_to_file );
        forAllInListOfArraysDo( langEnv->ctlDependenciesList, &print_ctl_dependency_to_file );
        forAllInDynArrayDo(langEnv->ntonGroupsInfo,&print_nton_to_file);
        //fprintf(output,"}\n");
        fflush(output);

    } else
        printf("Opening UCC file failed. Please check that folder \"counters\" exists in run directory and has write permission.\n");
   } else {
       printf("Could not open UCC file, please clean \"counters\" folder. (Must contain less than 255 files.)\n");
   }
   //Loop Graph
   for(n=0;n<255;n++)
    {
        sprintf(filename, "./counters/LoopGraph.%d",n);
        output = fopen(filename,"r");
        if(output)
        {
            fclose(output);
        }else{
            break;
        }
    }
   if(n<255){
    printf("Saving LoopGraph to File: %s ...\n", filename);
    output = fopen(filename,"w+");
    if(output!=NULL){
        set_dependency_file(output);
        //fprintf(output,"digraph Dependencies {\n");
        //set_dot_file(output);
        //FIXME:  first line still depends on counters being enabled, replace w/ unit struct!
        //forAllInDynArrayDo(_PRTopEnv->counter_history_array_info, &print_dot_node_info );
        forAllInListOfArraysDo( langEnv->unitList, &print_unit_to_file );
        forAllInListOfArraysDo( langEnv->commDependenciesList, &print_comm_dependency_to_file );
        forAllInListOfArraysDo( langEnv->ctlDependenciesList, &print_ctl_dependency_to_file );
        forAllInListOfArraysDo( langEnv->dynDependenciesList, &print_dyn_dependency_to_file );
        forAllInListOfArraysDo( langEnv->hwArcs, &print_hw_dependency_to_file );
        //fprintf(output,"}\n");
        fflush(output);

    } else
        printf("Opening LoopGraph file failed. Please check that folder \"counters\" exists in run directory and has write permission.\n");
   } else {
       printf("Could not open LoopGraph file, please clean \"counters\" folder. (Must contain less than 255 files.)\n");
   }
   
   
   freeListOfArrays(langEnv->unitList);
   freeListOfArrays(langEnv->commDependenciesList);
   freeListOfArrays(langEnv->ctlDependenciesList);
   freeListOfArrays(langEnv->dynDependenciesList);
   
   #endif
#ifdef HOLISTIC__TURN_ON_PERF_COUNTERS    
    for(n=0;n<255;n++)
    {
        sprintf(filename, "./counters/Counters.%d.csv",n);
        output = fopen(filename,"r");
        if(output)
        {
            fclose(output);
        }else{
            break;
        }
    }
    if(n<255){
    printf("Saving Counter measurements to File: %s ...\n", filename);
    output = fopen(filename,"w+");
    if(output!=NULL){
        int i;
        set_counter_file(output);
        for(i=0;i<_PRTopEnv->num_cores;i++)
         {
           forAllInListOfArraysDo( langEnv->counterList[i], &PR_MEAS__print_counter_event_to_file );
           fflush(output);
         }

    } else
        printf("Opening UCC file failed. Please check that folder \"counters\" exists in run directory and has write permission.\n");
   } else {
       printf("Could not open UCC file, please clean \"counters\" folder. (Must contain less than 255 files.)\n");
   }
    
#endif
   
   //Things to free:
   // tasks and slaves in ready Qs
   // ready Qs
   // tasks and slaves in the comm hash table
   // comm hash table
   // tasks in the argPtrHashTbl
   // the argPtrHashTbl
   PrivQueueStruc *queue;
   SlaveVP *slave;
   
   //dissipate any slaves still in the readyQ, because this is last lang
   queue = langEnv->slaveReadyQ;
   slave = readPrivQ( queue );
   while( slave != NULL )
    { 
      PR_int__recycle_slaveVP( slave ); //recycler is for all of PR
      slave = readPrivQ( queue );
    }
   freePrivQ( queue );
   
   //no any tasks in the readyQ
   queue = langEnv->taskReadyQ;
   freePrivQ( queue );
      
   int32 i;
   for( i = 0; i < NUM_STRUCS_IN_LANG_ENV; i++ )
    {
      freePrivQ( langEnv->fnSingletons[i].waitQ );
      freePrivQ( langEnv->transactionStrucs[i].waitingVPQ );
    }
   //note, PR handles removing the langEnv from the process and freeing
   // the langEnv chunk of memory
 }


