
//Assembly code takes the return addr off the stack and saves
// into the singleton.  The first field in the singleton is the
// "endInstrAddr" field, and the return addr is at 0x4(%ebp)
.globl asm_save_ret_to_singleton
asm_save_ret_to_singleton:
    movq 0x8(%rbp),     %rax   #get ret address, ebp is the same as in the calling function
    movq     %rax,     (%rdi) #write ret addr to endInstrAddr field
    ret


//Assembly code changes the return addr on the stack to the one
// saved into the singleton by the end-singleton-fn
//The stack's return addr is at 0x4(%%ebp)
.globl asm_write_ret_from_singleton
asm_write_ret_from_singleton:
    movq    (%rdi),    %rax  #get endInstrAddr field
    movq      %rax,    0x8(%rbp) #write return addr to the stack of the caller
    ret


