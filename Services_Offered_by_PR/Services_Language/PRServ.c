/*
 * Copyright 2010-2013  OpenSourceResearchInstitute.org
 *
 * Licensed under BSD
 */

//===========================================================================
//
//===========================================================================
/*These are the library functions *called in the application*
 * 
 *There's a pattern for the outside sequential code to interact with the
 * PR_HW code.
 *The PR_HW system is inside a boundary..  every PRServ system is in its
 * own directory that contains the functions for each of the processor types.
 * One of the processor types is the "seed" processor that starts the
 * cascade of creating all the processors that do the work.
 */


//===========================================================================
//
//===========================================================================
/*This file is for the built-in "services" or "utility" langlet, which starts
 * automatically when a new process is created.  The seed VP initially runs
 * within this langlet's environment.
 */

//===========================================================================

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>

#include <PR__include/prqueue.h>
#include <PR__include/prhash.h>

#include "PRServ_int.h"

//#include "Measurement/PRServ_Counter_Recording.h"

//==========================================================================
//=== prototypes for local helper functions
//=
void
PRServ__init_Helper();

//==========================================================================



int32
PRServ__giveMinWorkUnitCycles( float32 percentOverhead )
 {
   return MIN_WORK_UNIT_CYCLES;
 }

int32
PRServ__giveIdealNumWorkUnits()
 {
   return 3 * _PRTopEnv->num_cores;
 }

int32
PRServ__give_number_of_cores_to_schedule_onto()
 {
   return _PRTopEnv->num_cores;
 }

/*For now, use TSC -- later, make these two macros with assembly that first
 * saves jump point, and second jumps back several times to get reliable time
 */
void
PRServ__begin_primitive( SlaveVP *animSlv )
 { PRServLangData *langData;
   
   langData = (PRServLangData *)PR_WL__give_lang_data( animSlv, PRServ_MAGIC_NUMBER);

   saveLowTimeStampCountInto( langData->primitiveStartTime );
 }

/*Just quick and dirty for now -- make reliable later
 * will want this to jump back several times -- to be sure cache is warm
 * because don't want comm time included in calc-time measurement -- and
 * also to throw out any "weird" values due to OS interrupt or TSC rollover
 */
int32
PRServ__end_primitive_and_give_cycles( SlaveVP *animSlv )
 { int32 endTime, startTime;
   PRServLangData *langData;
   
   //TODO: fix by repeating time-measurement
   saveLowTimeStampCountInto( endTime );
   langData = (PRServLangData *)PR_WL__give_lang_data( animSlv, PRServ_MAGIC_NUMBER);
   startTime = langData->primitiveStartTime;
   return (endTime - startTime);
 }

inline 
int32 *
PRServ__give_self_taskID( SlaveVP *animSlv )
 { void *metaTask;
   metaTask = PR_WL__give_lang_meta_task_from_slave( animSlv, PRServ_MAGIC_NUMBER );
   return PR__give_ID_from_lang_meta_task( metaTask );
 }

inline 
int32 *
PRServ__give_slave_ID( SlaveVP *animSlv )
 {
   return PR__give_ID_from_slave( animSlv, PRServ_MAGIC_NUMBER );
 }

/*
 *This function returns information about the version of PR, the language
 * the program is being run in, its version, and information on the 
 * hardware.
 */
char *
PRServ___give_environment_string()
 { char *buffer = PR_WL__malloc(10000);
   int32 j;
   
   j = sizeof(int32);  //put total num chars here when done
   //--------------------------
   j += sprintf(buffer+j, "#\n# >> Build information <<\n");
   j += sprintf(buffer+j, "# GCC VERSION: %d.%d.%d\n",__GNUC__,__GNUC_MINOR__,__GNUC_PATCHLEVEL__);
   j += sprintf(buffer+j, "# Build Date: %s %s\n", __DATE__, __TIME__);
    
   j += sprintf(buffer+j, "#\n# >> Hardware information <<\n");
   j += sprintf(buffer+j, "# Hardware Architecture: ");
   #ifdef __x86_64
   j += sprintf(buffer+j, "x86_64");
   #endif //__x86_64
   #ifdef __i386
   j += sprintf(buffer+j, "x86");
   #endif //__i386
   j += sprintf(buffer+j, "\n");
   j += sprintf(buffer+j, "# Number of Cores: %d\n", _PRTopEnv->num_cores);
   //--------------------------
    
   //PR Plugins
   j += sprintf(buffer+j, "#\n# >> PR Plugins <<\n");
   j += sprintf(buffer+j, "# Language : ");
//   j += sprintf(buffer+j, _LANG_NAME_);
   j += sprintf(buffer+j, "\n");
       //Meta info gets set by calls from the language during its init,
       // and info registered by calls from inside the application
   j += sprintf(buffer+j, "# Assigner: %s\n", _PRTopEnv->metaInfo->assignerInfo);

   //--------------------------
   //Application
   j += sprintf(buffer+j, "#\n# >> Application <<\n");
   j += sprintf(buffer+j, "# Name: %s\n", _PRTopEnv->metaInfo->appInfo);
   j += sprintf(buffer+j, "# Data Set:\n%s\n",_PRTopEnv->metaInfo->inputInfo);
    
   ((int32 *)buffer)[0] = j - sizeof(int32); //insert the number of chars
   //--------------------------
   return (char *) &(((int32 *)buffer)[1]); //return pointer to first char
 }


//==========================================================================
//
/*A function singleton is a function whose body executes exactly once, on a
 * single core, no matter how many times the fuction is called and no
 * matter how many cores or the timing of cores calling it.
 *
 *A data singleton is a ticket attached to data.  That ticket can be used
 * to get the data through the function exactly once, no matter how many
 * times the data is given to the function, and no matter the timing of
 * trying to get the data through from different cores.
 */

/*asm function declarations*/
void asm_save_ret_to_singleton(PRServSingleton *singletonPtrAddr);
void asm_write_ret_from_singleton(PRServSingleton *singletonPtrAddr);

/*Fn singleton uses ID as index into array of singleton structs held in the
 * language environment.
 */
void
PRServ__start_fn_singleton( int32 singletonID,   SlaveVP *animSlv )
 {
   PRServLangReq  reqData;

      //
   reqData.singletonID = singletonID;

   PR_WL__send_lang_request( &reqData, (RequestHandler)&PRServ__handleStartFnSingleton,
                             animSlv, PRServ_MAGIC_NUMBER );
   if( animSlv->dataRetFromReq ) //will be 0 or addr of label in end singleton
    {
      PRServLangEnv *langEnv =
              PR_int__give_lang_env_for_slave( animSlv, PRServ_MAGIC_NUMBER );
      asm_write_ret_from_singleton(&(langEnv->fnSingletons[ singletonID]));
    }
 }

/*Data singleton hands addr of loc holding a pointer to a singleton struct.
 * The start_data_singleton makes the structure and puts its addr into the
 * location.
 */
void
PRServ__start_data_singleton( PRServSingleton **singletonAddr,  SlaveVP *animSlv )
 {
   PRServLangReq  reqData;

   if( *singletonAddr && (*singletonAddr)->hasFinished )
       goto JmpToEndSingleton;
   
   reqData.singletonPtrAddr = singletonAddr;

   PR_WL__send_lang_request( &reqData, (RequestHandler)&PRServ__handleStartDataSingleton,
                             animSlv, PRServ_MAGIC_NUMBER );
   if( animSlv->dataRetFromReq ) //either 0 or end singleton's return addr
    {    //Assembly code changes the return addr on the stack to the one
         // saved into the singleton by the end-singleton-fn
         //The return addr is at 0x4(%%ebp)
        JmpToEndSingleton:
          asm_write_ret_from_singleton(*singletonAddr);
    }
   //now, simply return
   //will exit either from the start singleton call or the end-singleton call
 }

/*Uses ID as index into array of flags.  If flag already set, resumes from
 * end-label.  Else, sets flag and resumes normally.
 *
 *Note, this call cannot be inlined because the instr addr at the label
 * inside is shared by all invocations of a given singleton ID.
 */
void
PRServ__end_fn_singleton( int32 singletonID, SlaveVP *animSlv )
 {
   PRServLangReq  reqData;

      //don't need this addr until after at least one singleton has reached
      // this function
   PRServLangEnv *
   langEnv = PR_int__give_lang_env_for_slave( animSlv, PRServ_MAGIC_NUMBER );
   
   asm_write_ret_from_singleton( &(langEnv->fnSingletons[ singletonID]) );

   reqData.singletonID = singletonID;

   PR_WL__send_lang_request( &reqData, (RequestHandler)&PRServ__handleEndFnSingleton, 
                             animSlv, PRServ_MAGIC_NUMBER );

EndSingletonInstrAddr:
   return;
 }

void
PRServ__end_data_singleton(  PRServSingleton **singletonPtrAddr, SlaveVP *animSlv )
 {
   PRServLangReq  reqData;

      //don't need this addr until after singleton struct has reached
      // this function for first time
      //do assembly that saves the return addr of this fn call into the
      // data singleton -- that data-singleton can only be given to exactly
      // one instance in the code of this function.  However, can use this
      // function in different places for different data-singletons.
   asm_save_ret_to_singleton(*singletonPtrAddr);

   reqData.singletonPtrAddr = singletonPtrAddr;

   PR_WL__send_lang_request( &reqData, (RequestHandler)&PRServ__handleEndDataSingleton,
                             animSlv, PRServ_MAGIC_NUMBER );
 }

/*This executes the function in the masterVP, so it executes in isolation
 * from any other copies -- only one copy of the function can ever execute
 * at a time.
 *
 *It suspends to the master, and the request handler takes the function
 * pointer out of the request and calls it, then resumes the VP.
 *Only very short functions should be called this way -- for longer-running
 * isolation, use transaction-start and transaction-end, which run the code
 * between as work-code.
 */
void
PRServ__animate_short_fn_in_isolation( PtrToAtomicFn ptrToFnToExecInMaster,
                                    void *data, SlaveVP *animSlv )
 {
   PRServLangReq  reqData;

   
   reqData.fnToExecInMaster = ptrToFnToExecInMaster;
   reqData.dataForFn        = data;

   PR_WL__send_lang_request( &reqData, (RequestHandler)&PRServ__handleAtomic, 
                             animSlv, PRServ_MAGIC_NUMBER );
 }


/*This suspends to the master.
 *First, it looks at the VP's data, to see the highest transactionID that VP
 * already has entered.  If the current ID is not larger, it throws an
 * exception stating a bug in the code.  Otherwise it puts the current ID
 * there, and adds the ID to a linked list of IDs entered -- the list is
 * used to check that exits are properly ordered.
 *Next it is uses transactionID as index into an array of transaction
 * structures.
 *If the "VP_currently_executing" field is non-null, then put requesting VP
 * into queue in the struct.  (At some point a holder will request
 * end-transaction, which will take this VP from the queue and resume it.)
 *If NULL, then write requesting into the field and resume.
 */
void
PRServ__start_transaction( int32 transactionID, SlaveVP *animSlv )
 {
   PRServLangReq  reqData;

      //
   reqData.callingSlv  = animSlv;
   reqData.transID     = transactionID;

   PR_WL__send_lang_request( &reqData, (RequestHandler)&PRServ__handleTransStart,
                             animSlv, PRServ_MAGIC_NUMBER );
 }

/*This suspends to the master, then uses transactionID as index into an
 * array of transaction structures.
 *It looks at VP_currently_executing to be sure it's same as requesting VP.
 * If different, throws an exception, stating there's a bug in the code.
 *Next it looks at the queue in the structure.
 *If it's empty, it sets VP_currently_executing field to NULL and resumes.
 *If something in, gets it, sets VP_currently_executing to that VP, then
 * resumes both.
 */
void
PRServ__end_transaction( int32 transactionID, SlaveVP *animSlv )
 {
   PRServLangReq  reqData;

      //
   reqData.callingSlv      = animSlv;
   reqData.transID     = transactionID;

   PR_WL__send_lang_request( &reqData, (RequestHandler)&PRServ__handleTransEnd,
                             animSlv, PRServ_MAGIC_NUMBER );
 }


//============================ DKU ==============================

DKUInstance *
PRServ__DKU_make_empty_DKU_instance( SlaveVP *animSlv )
 {
   return PR__malloc( sizeof(DKUInstance) );
 }

DKUPiece *
PRServ__DKU_make_empty_DKU_piece()
 {
   DKUPiece *retPiece = PR__malloc( sizeof(DKUPiece) );
   retPiece->parent = NULL;
   retPiece->wasRecursivelyDivided = FALSE;
   retPiece->waitingVP = NULL;
   return retPiece;
 }

DKUPiece *
PRServ__DKU_make_child_piece_from( DKUPiece *parent )
 {
   DKUPiece *retPiece = PR__malloc( sizeof(DKUPiece) );
   retPiece->dkuInstance = parent->dkuInstance;
   retPiece->parent = parent;
   retPiece->wasRecursivelyDivided = FALSE;
   retPiece->waitingVP = NULL;
   return retPiece;
 }

void
PRServ__DKU_set_root_piece_maker( DKUInstance *dkuInstance, 
                                  DKURootPieceMaker rootPieceMakerFn, 
                                  SlaveVP *animSlv )
 {
   dkuInstance->rootPieceMaker = rootPieceMakerFn;
 }

void
PRServ__DKU_set_kernel( DKUInstance *dkuInstance, 
                        DKUKernel kernelFn, 
                        SlaveVP *animSlv )
 {
   dkuInstance->kernel = kernelFn;
 }

void
PRServ__DKU_set_divider( DKUInstance *dkuInstance, 
                         DKUDivider dividerFn, 
                         SlaveVP *animSlv )
 {
   dkuInstance->divider = dividerFn;
 }

void
PRServ__DKU_set_undivider( DKUInstance *dkuInstance, 
                           DKUUndivider undividerFn, 
                           SlaveVP *animSlv )
 {
   dkuInstance->undivider = undividerFn;
 }

void
PRServ__DKU_set_serial_kernel( DKUInstance *dkuInstance, 
                               DKUSerialKernel serialKernelFn, 
                               SlaveVP *animSlv )
 {
   dkuInstance->serialKernel = serialKernelFn;
 }

DKUPiece *
PRServ__DKU_make_root_piece( DKUInstance *dkuInstance, 
                             void *data, 
                             SlaveVP *animSlv )
 { DKUPiece *
     retPiece = (*(dkuInstance->rootPieceMaker))(data, dkuInstance);
   return retPiece;
 }

void
PRServ__DKU_perform_work_on( DKUPiece *rootPiece, 
                             SlaveVP *animSlv )
 { PRServLangReq  reqData;

   reqData.dkuPiece    = rootPiece;

   PR_WL__send_lang_request( &reqData, (RequestHandler)&PRServ__handleDKUPerformWork,
                             animSlv, PRServ_MAGIC_NUMBER );
 }

void
PRServ__DKU_wait_for_result_to_be_complete( DKUPiece *dkuPiece, 
                                            SlaveVP  *animSlv )
 { PRServLangReq  reqData;

   reqData.dkuPiece = dkuPiece;

   PR_WL__send_lang_request( &reqData, (RequestHandler)&PRServ__handleDKUWaitTillDone,
                             animSlv, PRServ_MAGIC_NUMBER );
 }

void
PRServ__DKU_end_kernel( DKUPiece *piece, SlaveVP *animVP )
 { PRServLangReq  reqData;

   reqData.dkuPiece = piece;

   PR_WL__send_lang_request( &reqData, (RequestHandler)&PRServ__handleDKUEndKernelTask,
                             animVP, PRServ_MAGIC_NUMBER );
 }

void
PRServ__DKU_end_serial_kernel( SlaveVP *animVP )
 { PRServLangReq  reqData;

   PR_WL__send_lang_request( &reqData, (RequestHandler)&PRServ__handleDKUEndSerialKernelTask,
                             animVP, PRServ_MAGIC_NUMBER );
 }

//==========================================================

