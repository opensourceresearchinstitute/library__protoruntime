/*
 * Copyright 2010  OpenSourceCodeStewardshipFoundation
 *
 * Licensed under BSD
 */

#include <stdio.h>
#include <stdlib.h>


#include <PR__include/prqueue.h>
#include <PR__include/prhash.h>
#include <PR__include/PR__PI.h>
#include <PR__include/langlets/PRServ__wrapper_library.h>

#include "PRServ_int.h"
#include "PRServ_Request_Handlers.h"



//=========================== Local Fn Prototypes ===========================


//===========================================================================
//
/*Uses ID as index into array of flags.  If flag already set, resumes from
 * end-label.  Else, sets flag and resumes normally.
 */
void 
//inline
PRServ__handleStartSingleton_helper( PRServSingleton *singleton, SlaveVP *reqstingSlv,
                             PRServLangEnv    *langEnv )
 {
   if( singleton->hasFinished )
    {    //the code that sets the flag to true first sets the end instr addr
      reqstingSlv->dataRetFromReq = singleton->endInstrAddr;
      PR_PI__make_slave_ready( reqstingSlv, langEnv );
      return;
    }
   else if( singleton->hasBeenStarted )
    {    //singleton is in-progress in a diff slave, so wait for it to finish
      writePrivQ(reqstingSlv, singleton->waitQ );
      return;
    }
   else
    {    //hasn't been started, so this is the first attempt at the singleton
      singleton->hasBeenStarted = TRUE;
      reqstingSlv->dataRetFromReq = 0x0;
      PR_PI__make_slave_ready( reqstingSlv, langEnv );
      return;
    }
 }
void 
//inline
PRServ__handleStartFnSingleton( PRServLangReq *langReq, SlaveVP *requestingSlv,
                      PRServLangEnv *langEnv )
 { PRServSingleton *singleton;
         DEBUG__printf1(dbgRqstHdlr,"StartFnSingleton request from processor %d",requestingSlv->slaveNum)

   singleton = &(langEnv->fnSingletons[ langReq->singletonID ]);
   PRServ__handleStartSingleton_helper( singleton, requestingSlv, langEnv );
 }
void 
//inline
PRServ__handleStartDataSingleton( PRServLangReq *langReq, SlaveVP *requestingSlv,
                      PRServLangEnv *langEnv )
 { PRServSingleton *singleton;

         DEBUG__printf1(dbgRqstHdlr,"StartDataSingleton request from processor %d",requestingSlv->slaveNum)
   if( *(langReq->singletonPtrAddr) == NULL )
    { singleton                 = PR_PI__malloc( sizeof(PRServSingleton) );
      singleton->waitQ          = makePrivQ();
      singleton->endInstrAddr   = 0x0;
      singleton->hasBeenStarted = FALSE;
      singleton->hasFinished    = FALSE;
      *(langReq->singletonPtrAddr)  = singleton;
    }
   else
      singleton = *(langReq->singletonPtrAddr);
   PRServ__handleStartSingleton_helper( singleton, requestingSlv, langEnv );
 }


void 
//inline
PRServ__handleEndSingleton_helper( PRServSingleton *singleton, SlaveVP *requestingSlv,
                           PRServLangEnv    *langEnv )
 { PrivQueueStruc *waitQ;
   int32           numWaiting, i;
   SlaveVP      *resumingSlv;

   if( singleton->hasFinished )
    { //by definition, only one slave should ever be able to run end singleton
      // so if this is true, is an error
      ERROR1( "singleton code ran twice", requestingSlv );
    }

   singleton->hasFinished = TRUE;
   waitQ = singleton->waitQ;
   numWaiting = numInPrivQ( waitQ );
   for( i = 0; i < numWaiting; i++ )
    {    //they will resume inside start singleton, then jmp to end singleton
      resumingSlv = readPrivQ( waitQ );
      resumingSlv->dataRetFromReq = singleton->endInstrAddr;
      PR_PI__make_slave_ready( resumingSlv, langEnv );
    }

   PR_PI__make_slave_ready( requestingSlv, langEnv );

}
void
PRServ__handleEndFnSingleton( PRServLangReq *langReq, SlaveVP *requestingSlv,
                        PRServLangEnv *langEnv )
 {
   PRServSingleton   *singleton;

         DEBUG__printf1(dbgRqstHdlr,"EndFnSingleton request from processor %d",requestingSlv->slaveNum)
   
   singleton = &(langEnv->fnSingletons[ langReq->singletonID ]);
   PRServ__handleEndSingleton_helper( singleton, requestingSlv, langEnv );
  }
void
PRServ__handleEndDataSingleton( PRServLangReq *langReq, SlaveVP *requestingSlv,
                        PRServLangEnv *langEnv )
 {
   PRServSingleton   *singleton;

         DEBUG__printf1(dbgRqstHdlr,"EndDataSingleton request from processor %d",requestingSlv->slaveNum)
   
   singleton = *(langReq->singletonPtrAddr);
   PRServ__handleEndSingleton_helper( singleton, requestingSlv, langEnv );
  }


/*This executes the function in the masterVP, take the function
 * pointer out of the request and call it, then resume the VP.
 */
void
PRServ__handleAtomic( PRServLangReq *langReq, SlaveVP *requestingSlv, PRServLangEnv *langEnv )
 {
         DEBUG__printf1(dbgRqstHdlr,"Atomic request from processor %d",requestingSlv->slaveNum)
   langReq->fnToExecInMaster( langReq->dataForFn );
   PR_PI__make_slave_ready( requestingSlv, langEnv );
 }

/*First, it looks at the VP's semantic data, to see the highest transactionID
 * that VP
 * already has entered.  If the current ID is not larger, it throws an
 * exception stating a bug in the code.
 *Otherwise it puts the current ID
 * there, and adds the ID to a linked list of IDs entered -- the list is
 * used to check that exits are properly ordered.
 *Next it is uses transactionID as index into an array of transaction
 * structures.
 *If the "VP_currently_executing" field is non-null, then put requesting VP
 * into queue in the struct.  (At some point a holder will request
 * end-transaction, which will take this VP from the queue and resume it.)
 *If NULL, then write requesting into the field and resume.
 */
void
PRServ__handleTransStart( PRServLangReq *langReq, SlaveVP *requestingSlv,
                  PRServLangEnv *langEnv )
 { PRServLangData *langData;
   PRServTransListElem *nextTransElem;

         DEBUG__printf1(dbgRqstHdlr,"TransStart request from processor %d",requestingSlv->slaveNum)
   
      //check ordering of entering transactions is correct
   langData = PR_PI__give_lang_data_from_slave(requestingSlv, PRServ_MAGIC_NUMBER);
   if( langData->highestTransEntered > langReq->transID )
    {    //throw PR exception, which shuts down PR.
      PR_PI__throw_exception( "transID smaller than prev", requestingSlv, NULL);
    }
      //add this trans ID to the list of transactions entered -- check when
      // end a transaction
   langData->highestTransEntered = langReq->transID;
   nextTransElem = PR_PI__malloc( sizeof(PRServTransListElem) );
   nextTransElem->transID = langReq->transID;
   nextTransElem->nextTrans = langData->lastTransEntered;
   langData->lastTransEntered = nextTransElem;

      //get the structure for this transaction ID
   PRServTrans *
   transStruc = &(langEnv->transactionStrucs[ langReq->transID ]);

   if( transStruc->VPCurrentlyExecuting == NULL )
    {
      transStruc->VPCurrentlyExecuting = requestingSlv;
      PR_PI__make_slave_ready( requestingSlv, langEnv );
    }
   else
    {    //note, might make future things cleaner if save request with VP and
         // add this trans ID to the linked list when gets out of queue.
         // but don't need for now, and lazy..
      writePrivQ( requestingSlv, transStruc->waitingVPQ );
    }
 }


/*Use the trans ID to get the transaction structure from the array.
 *Look at VP_currently_executing to be sure it's same as requesting VP.
 * If different, throw an exception, stating there's a bug in the code.
 *Next, take the first element off the list of entered transactions.
 * Check to be sure the ending transaction is the same ID as the next on
 * the list.  If not, incorrectly nested so throw an exception.
 *
 *Next, get from the queue in the structure.
 *If it's empty, set VP_currently_executing field to NULL and resume
 * requesting VP.
 *If get somethine, set VP_currently_executing to the VP from the queue, then
 * resume both.
 */
void
PRServ__handleTransEnd(PRServLangReq *langReq, SlaveVP *requestingSlv, PRServLangEnv *langEnv)
 { PRServLangData    *langData;
   SlaveVP     *waitingSlv;
   PRServTrans      *transStruc;
   PRServTransListElem *lastTrans;
   
         DEBUG__printf1(dbgRqstHdlr,"TransEnd request from processor %d",requestingSlv->slaveNum)
   
   transStruc = &(langEnv->transactionStrucs[ langReq->transID ]);

      //make sure transaction ended in same VP as started it.
   if( transStruc->VPCurrentlyExecuting != requestingSlv )
    {
      PR_PI__throw_exception( "trans ended in diff VP", requestingSlv, NULL );
    }

      //make sure nesting is correct -- last ID entered should == this ID
   langData = PR_PI__give_lang_data_from_slave(requestingSlv, PRServ_MAGIC_NUMBER);
   lastTrans = langData->lastTransEntered;
   if( lastTrans->transID != langReq->transID )
    {
      PR_PI__throw_exception( "trans incorrectly nested", requestingSlv, NULL );
    }

   langData->lastTransEntered = langData->lastTransEntered->nextTrans;


   waitingSlv = readPrivQ( transStruc->waitingVPQ );
   transStruc->VPCurrentlyExecuting = waitingSlv;

   if( waitingSlv != NULL )
      PR_PI__make_slave_ready( waitingSlv, langEnv );

   PR_PI__make_slave_ready( requestingSlv, langEnv );
 }


//==========================================================================
//                           Helpers
//


/*Proto Runtime has a special call-chain for resuming a slaveVP.  The langlet
 * supplies a function to perform the resume, but then registers it with
 * PR, and instead calls PR's version..  PR then decides whether to handle
 * the resume itself, or pass resume along to this registered handler. 
 */
void
PRServ__resume_slave( SlaveVP *slave, void *_langEnv )
 { PRServLangEnv *langEnv = (PRServLangEnv *)_langEnv;
 
      //both suspended tasks and suspended explicit slaves resumed with this
   writePrivQ( slave, langEnv->slaveReadyQ );
//PR handles this..   PR_int__set_work_in_lang_env(langEnv);
   
   #ifdef HOLISTIC__TURN_ON_PERF_COUNTERS
/*
   int lastRecordIdx = slave->counter_history_array_info->numInArray -1;
   CounterRecord* lastRecord = slave->counter_history[lastRecordIdx];
   saveLowTimeStampCountInto(lastRecord->unblocked_timestamp);
*/
   #endif
   #ifdef HOLISTIC__TURN_ON_OBSERVE_UCC
   slave->numTimesAssignedToASlot++; //Somewhere here!
   Unit newU;
   newU.vp = slave->slaveNum;
   newU.task = slave->numTimesAssignedToASlot;
   addToListOfArrays(Unit,newU,langEnv->unitList);
   
   if (slave->numTimesAssignedToASlot > 1)
    { Dependency newD;
      newD.from_vp = slave->slaveNum;
      newD.from_task = slave->numTimesAssignedToASlot - 1;
      newD.to_vp = slave->slaveNum;
      newD.to_task = slave->numTimesAssignedToASlot;
      addToListOfArrays(Dependency, newD ,langEnv->ctlDependenciesList);  
    }
   #endif
 }

void
PRServ__handleDissipateSeed(void *langReq, SlaveVP *seedSlv, PRServLangEnv *langEnv )
 { 
 
         DEBUG__printf1(dbgRqstHdlr,"Dissipate in PRServ %d",
                                                     seedSlv->slaveNum);
   //This should only ever be called for the seed slave -- for which there's
   // nothing to do..
   if( seedSlv->typeOfVP != SeedSlv )
    {
      PR_int__throw_exception( "PR__end_seed called on non-seed slave\n", NULL, NULL);
    }
 }

void
PRServ__handle_end_process_from_inside( void *langReq, SlaveVP *reqSlave, PRServLangEnv *langEnv )
 { PR_SS__end_process_normally( reqSlave->processSlaveIsIn );
 }


/*Initialize semantic data struct..  this initializer doesn't need any input,
 * but some languages may need something from inside the request that was sent
 * to create a slave..  in that case, just make initializer do the malloc then
 * use the PR_PI__give_lang_data  inside the create handler, and fill in the
 * langData values there.
 */
void * 
PRServ__create_lang_data_in_slave( SlaveVP *slave )
 { PRServLangData *langData;

   langData = PR_PI__create_lang_data_in_slave( sizeof(PRServLangData), 
                                 &PRServ__free_lang_data, slave, PRServ_MAGIC_NUMBER );
   
   langData->highestTransEntered = -1;
   langData->lastTransEntered    = NULL;
   return langData;
 }


/*Register this by passing a pointer to this to PR's lang data creator Fn
 * 
 *It deletes all the transactions that the slave had entered..
 *Probably a bug -- need to do something more to free blocked slaves waiting. 
 *
 *At some point, may change PR so that it recycles langData, in which case this
 * only gets called when a process shuts down..  at that point, PR will call
 * dissipate on all the slaves it has in the recycle Q.
 */
void
PRServ__free_lang_data( void *_langData )
 { PRServLangData *langData;
   PRServTransListElem *transElem, *nextTransElem;

   langData = (PRServLangData *)_langData;
   transElem = langData->lastTransEntered;
   
   //Each trans the slave has entered was saved in a linked list elem inside
   // lang data -- delete those linked list elements
   //Bug: if this is not part of shutdown, then update state of transactions
   // to free blocked VPs
   while( transElem != NULL)
    { nextTransElem = transElem->nextTrans;
      PR_int__free( transElem );
      transElem = nextTransElem;
    }
   PR_PI__free( _langData );
 }

/*Will be used by a langData recycler, when (and if) such a thing added
 * to PR
 */
void 
resetPRServLangData( void *_langData ) 
 { PRServLangData *langData = (PRServLangData *)_langData;
   
   langData->highestTransEntered = -1;
   langData->lastTransEntered    = NULL;
 }


//===========================  Task Stuff  ==========================
void
PRServ__make_task_ready( void *taskStub, void *_langEnv )
 {
   writePrivQ( taskStub, ((PRServLangEnv*)_langEnv)->taskReadyQ );
 }

void
PRServ__lang_meta_task_freer( void *_langMetaTask )
 { //no malloc'd structs inside task stub, so nothing to do
 }

//===============================  DKU  =============================

void
PRServ__handleDKUPerformWork( PRServLangReq *langReq, SlaveVP *requestingSlv, PRServLangEnv *langEnv )
 { DKUPiece *dkuPiece, *childPiece, **childArray;
   int32 i;
   PRServMetaTask    *metaTask;

   dkuPiece = langReq->dkuPiece;
   
   //Place num children desired into DKUPiece to be divided
   // then call divider that's inside DKU instance that's in the piece
   // then iterate down the children
   //  create a task for each, and make that task ready
   dkuPiece->numChildren = _PRTopEnv->num_cores * 3;
  
      //do division outside of master lock -- normally takes significant time,
      // doesn't need to be protected, so don't make other cores wait to
      // get access to shared lang env and process structs..
   PR_int__release_master_lock();
   (*(dkuPiece->dkuInstance->divider))(dkuPiece); //makes array of children inside, sets numChildren
   PR_int__get_master_lock();
   
      //Before creating any tasks, set the count of how many finished
   dkuPiece->numUnfinishedChildren = dkuPiece->numChildren;

   if( dkuPiece->numChildren == 0 ) //divider wants serial kernel
    { metaTask = (PRServMetaTask *)PR_int__create_lang_meta_task( 
                sizeof(PRServMetaTask), &PRServ__lang_meta_task_freer, 
                                                PRServ_MAGIC_NUMBER );
      metaTask->dkuPiece = dkuPiece; //comm to end_serial_kernel
      
      //take params out of DKUPiece for the serial kernel
      PR_PI__set_task_properties( NO_ID, dkuPiece->dkuInstance->serialKernel,
                                  dkuPiece->payload, metaTask, requestingSlv,
                                  PRServ_MAGIC_NUMBER );
     
      PR_PI__make_task_ready( metaTask, langEnv );
    }
   else //make a task for each child piece that was created
    {
      childArray = dkuPiece->children;
      for( i = 0; i < dkuPiece->numChildren; i++ )
       {
         childPiece = childArray[i];

         //There's nothing in the lang part of the meta task -- all info
         // is in the DKUPiece, which is given as birth data
         metaTask = (PRServMetaTask *)PR_int__create_lang_meta_task( 
                   sizeof(PRServMetaTask), &PRServ__lang_meta_task_freer, 
                                                   PRServ_MAGIC_NUMBER );

         //give the DKUPiece to the kernel as the birth data, so it has
         // pointer to the undivider, data to comm to undiv, aso
         PR_PI__set_task_properties( NO_ID, dkuPiece->dkuInstance->kernel,
                                     childPiece, metaTask, requestingSlv,
                                     PRServ_MAGIC_NUMBER );

         PR_PI__make_task_ready( metaTask, langEnv );
       }
    }
   
      //resume the invoking VP
   PR_PI__make_slave_ready( requestingSlv, langEnv );
 }

/* Waits for all children of a parent to complete and have 
 * undivision completed on them.
 */
void
PRServ__handleDKUWaitTillDone( PRServLangReq *langReq, SlaveVP *requestingSlv, PRServLangEnv *langEnv )
 { DKUPiece *dkuPiece;

   dkuPiece = langReq->dkuPiece;

   if( dkuPiece->numUnfinishedChildren == 0 ) //all children done so resume
      PR_PI__make_slave_ready( requestingSlv, langEnv );
   else
      dkuPiece->waitingVP = requestingSlv;//end of last child resumes it
 }


/*End of kernel:
 * Recursively do:
 * invoke the undivider (if non-null)
 * Decrement parent's count of unfinished children
 * Check whether this is the last of the children
 *   if so, and there's a waiter on parent, resume it.
 *   also, recursively call processing of parent, so it updates its
 *   own parent's count of outstanding children..
 * Note: it's not possible for this to get invoked if the serial
 *  kernel was run instead of the divider.
 */
void
PRServ__handleDKUCompletedChildPiece( DKUPiece *childPiece, PRServLangEnv *langEnv )
 { DKUPiece *parentPiece;
 
   //Call undivider with this child piece -- it will get the parent
   if( childPiece->dkuInstance->undivider != NULL )
      (*childPiece->dkuInstance->undivider)( childPiece );
   
   parentPiece = childPiece->parent;
   parentPiece->numUnfinishedChildren -= 1;
   if( parentPiece->numUnfinishedChildren == 0 ) //all children done
    { if( parentPiece->waitingVP != NULL ) //resume waiter on parent
         PR_PI__make_slave_ready( parentPiece->waitingVP, langEnv ); 
      //Check whether PRServ made a hierarchy of DKU pieces
      if( parentPiece->parent != NULL && parentPiece->parent->wasRecursivelyDivided )
         PRServ__handleDKUCompletedChildPiece( parentPiece, langEnv );
    }   
 }
void
PRServ__handleDKUEndKernelTask( PRServLangReq *langReq, SlaveVP *requestingSlv, PRServLangEnv *langEnv )
 { 
   PRServ__handleDKUCompletedChildPiece( langReq->dkuPiece, langEnv );
 }


/*The app doesn't know whether the runtime chose regular kernel or
 * serial kernel, so it still executes the same wait for completion
 * command.  So, there may be a VP waiting for this serial kernel to
 * end.  To find it, the do-work handler, when it chose to run the
 * serial kernel, put the DKUPiece into the lang meta-task whose
 * birth Fn is the serial kernel.  Get meta-task from slave, and
 * DKUPiece from that.
 */
void
PRServ__handleDKUEndSerialKernelTask( PRServLangReq *langReq, 
                                      SlaveVP *requestingSlv, 
                                      PRServLangEnv *langEnv )
 { PRServMetaTask *langMetaTask;
   DKUPiece *dkuPiece;

   langMetaTask = (PRServMetaTask *) 
       PR_int__give_lang_meta_task_from_slave( requestingSlv, PRServ_MAGIC_NUMBER );

   dkuPiece = langMetaTask->dkuPiece;
   
   //The work is guaranteed done at this point -- that is the work 
   // represented by the parameters held inside the dku piece.  No
   // undivider to invoke.  The dku piece is not the child, but rather
   // what would have been the parent, if the work had been divided.
   if( dkuPiece->waitingVP != NULL )
      PR_PI__make_slave_ready( dkuPiece->waitingVP, langEnv ); 

   //Check whether PRServ made a hierarchy of DKU pieces
   if( dkuPiece->parent != NULL && dkuPiece->parent->wasRecursivelyDivided )
      PRServ__handleDKUCompletedChildPiece( dkuPiece, langEnv );
 }

//====================================================================


