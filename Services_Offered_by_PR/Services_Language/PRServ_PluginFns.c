/*
 * Copyright 2010  OpenSourceCodeStewardshipFoundation
 *
 * Licensed under BSD
 */

#include <stdio.h>
#include <stdlib.h>

#include <PR__include/prqueue.h>
#include "PRServ_int.h"
#include "PRServ_Request_Handlers.h"

//=========================== Local Fn Prototypes ===========================

//============================== Assigner ==================================
//
/*The assigner is complicated by having both tasks and explicitly created
 * VPs, and by tasks being able to suspend.
 *It can't use an explicit slave to animate a task because of stack
 * pollution. So, it has to keep the two kinds separate.
 * 
 *Q: one assigner for both tasks and slaves, or separate?
 * 
 *Simplest way for the assigner logic is with a Q for extra empty task
 * slaves, and another Q for slaves of both types that are ready to resume.
 *
 *Keep a current task slave for each anim slot. The request handler manages
 * it by pulling from the extraTaskSlvQ when a task suspends, or else
 * creating a new task slave if taskSlvQ empty. 
 *Assigner only assigns a task to the current task slave for the slot.
 *If no more tasks, then takes a ready to resume slave, if also none of them
 * then dissipates extra task slaves (one per invocation).
 *Shutdown condition is: must have no suspended tasks, and no suspended
 * explicit slaves and no more tasks in taskQ.  Will only have the masters
 * plus a current task slave for each slot.. detects this condition. 
 * 
 *Having the two types of slave is part of having communications directly
 * between tasks, and tasks to explicit slaves, which requires the ability
 * to suspend both kinds, but also to keep explicit slave stacks clean from
 * the junk tasks are allowed to leave behind.
 */
bool32
PRServ__assign_work_to_slot( void *_langEnv, AnimSlot *slot )
 { SlaveVP     *returnSlv;
   PRServMetaTask *task;
   PRServLangEnv  *langEnv;
   int32        coreNum;
  
   coreNum = slot->coreSlotIsOn;
   
   langEnv = (PRServLangEnv *)_langEnv;
   
      //Check for suspended slaves that are ready to resume
   returnSlv = readPrivQ( langEnv->slaveReadyQ );
   if( returnSlv != NULL )  //Yes, have a slave, so return it.
    { returnSlv->coreAnimatedBy   = coreNum;
      
      //PR calls this Fn, which in turn uses PR's primitive to assign
      // a slave to a slot.
      //Note: this represents a security risk, if langlet has a bug and gives
      // the wrong slot.. assuming will have a tool in future that analyzes
      // plugin code before certifying it..
      PR_int__put_slave_into_slot( returnSlv, slot );
      goto Success;
    }
   //No slave, see if any tasks (DKU tasks)
   task = readPrivQ( langEnv->taskReadyQ );
   if( task != NULL )  //Yes, have a slave, so return it.
    {       
      //call PR's primitive to assign task to the slot.
      //Note: this represents a security risk, if langlet has a bug and gives
      // the wrong slot.. assuming will have a tool in future that analyzes
      // plugin code before certifying it..
      PR_int__put_task_into_slot( task, slot );
      goto Success;
    }
   
   
   //If here, didn't assign any work
 Fail:
   return FALSE;
   //fixme; //figure out what to do -- go through holistic code still?

 Success:  //doing gotos to here should help with holistic..

   #ifdef HOLISTIC__TURN_ON_OBSERVE_UCC
   //This no longer works -- should be moved into PR in master
   //This assumes the task has already been assigned to a slave, which happens
   // inside Master..
   if( returnMetaTask == NULL )
    { returnSlv = langEnv->process->idleSlv[coreNum]; 
    
         //things that would normally happen in resume(), but these VPs
         // never go there
      returnSlv->numTimesAssignedToASlot++;
      Unit newU;
      newU.vp = returnSlv->slaveNum;
      newU.task = returnSlv->numTimesAssignedToASlot;
      addToListOfArrays(Unit,newU,langEnv->unitList);

      if (returnSlv->numTimesAssignedToASlot > 1)
       { Dependency newD;
         newD.from_vp = returnSlv->slaveNum;
         newD.from_task = returnSlv->numTimesAssignedToASlot - 1;
         newD.to_vp = returnSlv->slaveNum;
         newD.to_task = returnSlv->numTimesAssignedToASlot;
         addToListOfArrays(Dependency, newD, langEnv->ctlDependenciesList);  
       }
      returnMetaTask = returnSlv->metaTasks;
    }
   else //returnSlv != NULL
    { //assignSlv->numTimesAssigned++;
      Unit prev_in_slot = 
         langEnv->last_in_slot[coreNum];
      if(prev_in_slot.vp != 0)
       { Dependency newD;
         newD.from_vp = prev_in_slot.vp;
         newD.from_task = prev_in_slot.task;
         newD.to_vp = returnSlv->slaveNum;
         newD.to_task = returnSlv->numTimesAssignedToASlot;
         addToListOfArrays(Dependency,newD,langEnv->hwArcs);   
       }
      prev_in_slot.vp = returnSlv->slaveNum;
      prev_in_slot.task = returnSlv->numTimesAssignedToASlot;
      langEnv->last_in_slot[coreNum] =  prev_in_slot;
    }
   #endif
/* PR handles work available in lang env and in process..
   if( isEmptyPrivQ(langEnv->slavesReadyToResumeQ) &&
       isEmptyPrivQ(langEnv->taskReadyQ) ) 
      PR_int__clear_work_in_lang_env(langEnv);
 */
 
   return TRUE;
 }



//=========================== Helper ==============================
