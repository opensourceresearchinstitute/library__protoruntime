OpenSourceResearchInstitute.org   Proto-Runtime library

This is the core of a runtime environment that manages multiple threads (contexts) in user space.  It makes full use of all the cores on a multi-core machine, with ultra-low overhead, beating native Linux threads by 5x to 1000x, depending upon the size of the machine.  It manages millions of threads without slowdown (with performance measured to better than Erlang). 

It is intended to be used as a skeleton with which to construct your own runtime system for your own programming model or language.

A number of programming models are available at opensourceresearchinstitute.org and bitbucket.  These include:
* vthread -- an implementation of pthreads that is faster than native pthreads by 5x to 1000x
* SSR -- a new simple alternative to pthreads that has equivalent flexibility and breadth of scope, but is simpler to understand and easier to reason about 
* StarSS -- a faster implementation of the StarSS (OMPSS) runtime system
* HWSim -- a productivity enhancer for writing simulations that involve time in the simulated domain, such as a simulation of objects that interact.  HWSim handles all aspects of simulated time and ensures that computations take place in the host machine in a manner that is consistent with the simulated order.
* PRDSL -- a programming model for quickly and easily creating your own domain specific language

The library is dynamic, and has a number of versions, each of which is tuned for high performance on a particular type of machine.  A given version is built by switching to that version's branch (this is only enabled in the mercurial repository, coming soon to the git repo).

The library has a cmake build script as well as a netbeans project directory

To compile, you can just cd into the root directory and type:
cmake .
make

This will do an in-source compile.  (Although, the cmake is designed to move the library to a location that is specific to the proto-runtime development environment.  You will likely want to change the cmake so that it moves the built .so file to a location of your choice)

The cmake is designed to be included in a larger project.  In that case, there will be a higher level cmake file in a directory above this one, just add a line to that to include this sub-directory.  Then modify the cmake files in here so that they move the final .so file to a location of your choice.

This repository relies on have the OSRI C utility libraries at a known location, which is encoded in the cmake files.  These libraries can be found at opensourceresearchinstitute.org or on bitbucket under the opensourceresearchinstitute.org team's repos.

running tests requires also having a plugin (AKA programming model) repository, an application repository, and an umbrella that builds them all.  These can all be found in the same two places opensourceresearchinstitute.org or on bitbucket under the opensourceresearchinstitute.org team's repos.

For questions, email seanhalle@opensourceresearchinstitute.org