/*
 * Copyright 2010  OpenSourceResearchInstitute
 *
 * Licensed under BSD
 */



#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#define  __USE_GNU  //needed bby pthread.h
#include <pthread.h>
#include <sched.h>

#include <PR__include/PR__structs__common.h>
#include <PR__include/PR__int.h>

#include "PR__structs__int.h"
#include <PR__include/langlets/PRServ__wrapper_library.h>


//=====================  Functions local to this file =======================
void *terminateCoreController(SlaveVP *currSlv);

inline void
doBackoff_for_TooLongToGetLock( int32  numTriesToGetLock, uint32 *seed1, 
                                uint32 *seed2 );
inline void
doBackoff_for_TooLongWithNoWork( int32   numRepsWithNoWork, uint32 *seed1, 
                                 uint32 *seed2 );

//===========================================================================


/*The Core Controller is left over from proto-runtime Ver 1.0.. it will be
 * removed at some point, as a performance improvement..
 */
void *
coreController( void *paramsIn )
 { 
   int32           thisCoresIdx;
   int32           numRepetitionsWithNoWork;
   bool32          foundWork;
   AnimSlot       *animSlot;
   volatile int32 *addrOfMasterLock; //thing pointed to is volatile, not ptr
//   SlaveVP        *thisCoresMasterVP;
      //Variables used for pthread related things
   ThdParams      *thisCoresThdParams;
   cpu_set_t       coreMask;  //used during pinning pthread to CPU core
   int32           errorCode;
      //Variables used during measurements (inside macro!)
   TSCountLowHigh  endSusp;
      //Variables used in random-backoff, for master-lock and waiting for work
   uint32_t seed1 = rand()%1000; // init random number generator for backoffs
   uint32_t seed2 = rand()%1000;

   
   //===============  Initializations ===================
   thisCoresThdParams = (ThdParams *)paramsIn;
   thisCoresIdx = thisCoresThdParams->coreNum;

      //Assembly that saves addr of label of return instr -- addr used in assmbly
   recordCoreCtlrReturnLabelAddr((void**)&(_PRTopEnv->coreCtlrReturnPt));

   //TODO: DEBUG: check get correct pointer here
   animSlot = _PRTopEnv->allAnimSlots[ thisCoresIdx ];
   animSlot->slaveAssignedToSlot =  (_PRTopEnv->idleSlv)[thisCoresIdx];

   numRepetitionsWithNoWork = 0;
   addrOfMasterLock = &(_PRTopEnv->masterLock);
//   thisCoresMasterVP = _PRTopEnv->masterVPs[ thisCoresIdx ];
   
   //==================== pthread related stuff ======================
      //pin the pthread to the core -- takes away Linux control
      //Linux requires pinning to be done inside the thread-function
      //Designate a core by a 1 in bit-position corresponding to the core
   CPU_ZERO(&coreMask); //initialize mask bits to zero
   CPU_SET(thisCoresThdParams->coreNum,&coreMask); //set bit repr the coreNum
   pthread_t selfThd = pthread_self();
   errorCode =
   pthread_setaffinity_np( selfThd, sizeof(coreMask), &coreMask);
   if(errorCode){ printf("\n pinning thd to core failed \n"); exit(0); }

      //make sure the controllers all start at same time, by making them wait
   pthread_mutex_lock(  &suspendLock );
   while( !(_PRTopEnv->firstProcessReady) )
    { pthread_cond_wait( &suspendCond, &suspendLock );
    }
   pthread_mutex_unlock( &suspendLock );
   
         HOLISTIC__CoreCtrl_Setup;
   
         DEBUG__printf1(TRUE, "started coreCtrlr %d", thisCoresIdx );
         
   //====================== The Core Controller ======================
   while(1)  
    {        //Assembly code switches the core between animating a VP and
             // animating this core controller.  The switch is done by
             // changing the stack-pointer and frame-pointer and then doing
             // an assembly jmp.  When reading this code, the effect is 
             // that the "switchToSlv()" at the end of the loop is sort of a
             // "warp in time" -- the core disappears inside this, jmps to
             // animating a VP, and when that VP suspends, the suspend
             // jmps back. This has the effect of "returning" from the
             // switchToSlv() call. Then control loops back to here.
             //Alternatively, the VP suspend primitive could just not bother
             // returning from switchToSlv, and instead jmp directly to here.
            
      if(animSlot->slaveAssignedToSlot->typeOfVP == IdleVP)
       { //The Holistic stuff turns on idle slaves..  but can also be in mode
         // where have no idle slaves..  so, this IF statement can only be true
         // executed when HOLISTIC is turned on..
         numRepetitionsWithNoWork ++;
               HOLISTIC__Record_last_work;
       } 
      


            HOLISTIC__Record_AppResponderInvocation_start;
            MEAS__Capture_Pre_Master_Lock_Point;

      int numTriesToGetLock = 0; int gotLock = 0;
      while( gotLock == FALSE ) //keep going until get master lock
       { 
            //want to
            // reduce lock contention from cores with no work, so first
            // check if this is a core with no work, and busy wait if so.
            //Then, if it's been way too long without work, yield pthread
         if( numRepetitionsWithNoWork > NUM_REPS_W_NO_WORK_BEFORE_BACKOFF)
            doBackoff_for_TooLongWithNoWork( numRepetitionsWithNoWork, &seed1, &seed2 );
         if( numRepetitionsWithNoWork > NUM_REPS_W_NO_WORK_BEFORE_YIELD )
          { numRepetitionsWithNoWork = 0; pthread_yield(); }


            //Try to get the lock
         gotLock = __sync_bool_compare_and_swap( addrOfMasterLock,
                                                 UNLOCKED, LOCKED );
         if( gotLock )
          {    //At this point, have successfully gotten master lock.
               //So, break out of get-lock loop.
            break;
          }
            //Get here only when failed to get lock -- check in should do backoff

         numTriesToGetLock++;   //if too many, means too much contention
         if( numTriesToGetLock > NUM_TRIES_BEFORE_DO_BACKOFF ) 
            doBackoff_for_TooLongToGetLock( numTriesToGetLock, &seed1, &seed2 );
         if( numTriesToGetLock > MASTERLOCK_RETRIES_BEFORE_YIELD ) 
          { numTriesToGetLock = 0; pthread_yield(); }
       } //while( currVP == NULL )
            MEAS__Capture_Post_Master_Lock_Point;
         //have master lock, perform master function, which manages request
         // handling and assigning work to this core's slot
      foundWork =
         
         masterFunction( animSlot );
      
      PR_int__release_master_lock();
      
      if( foundWork )
         numRepetitionsWithNoWork = 0;
      else
         numRepetitionsWithNoWork += 1;

      //now that master is done, have work in the slot, so switch to it
            HOLISTIC__Record_Work_start;

      switchToSlv(animSlot->slaveAssignedToSlot); //The future Slave suspend will 
                                             //jmp to "ret" at end of this call
      flushRegisters();    //prevent GCC optimization from doing bad things 

             MEAS__Capture_End_Susp_in_CoreCtlr_ForSys;
             HOLISTIC__Record_Work_end;
    }//while(1)
 }

/*Shutdown of PR involves several steps, of which this is the last.  This
 * function is jumped to from the asmTerminateCoreCtrl, which is in turn
 * called from endOSThreadFn, which is the birth Fn of the shutdown
 * slaves.
 */
void *
terminateCoreCtlr(SlaveVP *currSlv)
 {
   //first, free shutdown Slv that jumped here, then end the pthread
//   PR_int__free_slaveVP( currSlv );
   pthread_exit( NULL );
 }

inline uint32_t
randomNumber()
 {
	_PRTopEnv->seed1 = (uint32)(36969 * (_PRTopEnv->seed1 & 65535) + 
                                   (_PRTopEnv->seed1 >> 16) );
	_PRTopEnv->seed2 = (uint32)(18000 * (_PRTopEnv->seed2 & 65535) + 
                                   (_PRTopEnv->seed2 >> 16) );
	return (_PRTopEnv->seed1 << 16) + _PRTopEnv->seed2;
 }



/*Busy-wait for a random number of cycles -- chooses number of cycles 
 * differently than for the too-many-tries-to-get-lock backoff
 */
inline void
doBackoff_for_TooLongWithNoWork( int32   numRepsWithNoWork, uint32 *seed1, 
                                 uint32 *seed2 )
 { int32 i, waitIterations;
   volatile double fakeWorkVar; //busy-wait fake work
 
     //Get a random number of iterations to busy-wait.  The % is a simple
     // way to set the maximum value that can be generated.
   waitIterations = 
    randomNumber(seed1, seed2) % 
    (numRepsWithNoWork * numRepsWithNoWork * _PRTopEnv->num_cores);
   for( i = 0; i < waitIterations; i++ )
    { fakeWorkVar += (fakeWorkVar + 32.0) / 2.0; //busy-wait
    }
 }

/*Busy-waits for a random number of cycles -- chooses number of cycles 
 * differently than for the no-work backoff
 */
inline void
doBackoff_for_TooLongToGetLock( int32 numTriesToGetLock, uint32 *seed1, 
                                uint32 *seed2 )
 { int32 i, waitIterations;
   volatile double fakeWorkVar; //busy-wait fake work

   waitIterations = 
    randomNumber(seed1, seed2) % 
    (numTriesToGetLock * GET_LOCK_BACKOFF_WEIGHT);   
   //addToHist( wait_iterations, coreLoopThdParams->wait_iterations_hist );
   for( i = 0; i < waitIterations; i++ )
    { fakeWorkVar += (fakeWorkVar + 32.0) / 2.0; //busy-wait
    }
 }


#ifdef DEBUG__TURN_ON_SEQUENTIAL_MODE

//===========================================================================
/*This sequential version does the same as threaded, except doesn't do the
 * pin-threads part, nor the wait until setup complete and acquire master
 * lock parts.
 */
void *
coreCtlr_Seq( void *paramsIn )
 { 
   int32           thisCoresIdx;
   int32           numRepetitionsWithNoWork;
   bool32          foundWork;
   AnimSlot       *animSlot;
   volatile int32 *addrOfMasterLock; //thing pointed to is volatile, not ptr
      //Variables used for pthread related things
   ThdParams      *thisCoresThdParams;
   cpu_set_t       coreMask;  //used during pinning pthread to CPU core
   int32           errorCode;
      //Variables used during measurements (inside macro!)
   TSCountLowHigh  endSusp;
      //Variables used in random-backoff, for master-lock and waiting for work
   uint32_t seed1 = rand()%1000; // init random number generator for backoffs
   uint32_t seed2 = rand()%1000;

   
   //===============  Initializations ===================
//   thisCoresThdParams = (ThdParams *)paramsIn;
//   thisCoresIdx = thisCoresThdParams->coreNum;
   thisCoresIdx = 0;

      //Assembly that saves addr of label of return instr -- addr used in assmbly
   recordCoreCtlrReturnLabelAddr((void**)&(_PRTopEnv->coreCtlrReturnPt));

   //TODO: DEBUG: check get correct pointer here
   animSlot = _PRTopEnv->allAnimSlots[ thisCoresIdx ][0];
   animSlot->slaveAssignedToSlot =  _PRTopEnv->idleSlv[thisCoresIdx];

   numRepetitionsWithNoWork = 0;
   addrOfMasterLock = &(_PRTopEnv->masterLock);
   
   //==================== pthread related stuff ======================
      //pin the pthread to the core -- takes away Linux control
      //Linux requires pinning to be done inside the thread-function
      //Designate a core by a 1 in bit-position corresponding to the core
/*
   CPU_ZERO(&coreMask); //initialize mask bits to zero
   CPU_SET(thisCoresThdParams->coreNum,&coreMask); //set bit repr the coreNum
   pthread_t selfThd = pthread_self();
   errorCode =
   pthread_setaffinity_np( selfThd, sizeof(coreMask), &coreMask);
   if(errorCode){ printf("\n pinning thd to core failed \n"); exit(0); }

      //make sure the controllers all start at same time, by making them wait
   pthread_mutex_lock(  &suspendLock );
   while( !(_PRTopEnv->firstProcessReady) )
    { pthread_cond_wait( &suspendCond, &suspendLock );
    }
   pthread_mutex_unlock( &suspendLock );
   
         HOLISTIC__CoreCtrl_Setup;
   
         DEBUG__printf1(TRUE, "started coreCtrlr", thisCoresIdx );
 */
   //====================== The Core Controller ======================
   while(1)  
    {        //Assembly code switches the core between animating a VP and
             // animating this core controller.  The switch is done by
             // changing the stack-pointer and frame-pointer and then doing
             // an assembly jmp.  When reading this code, the effect is 
             // that the "switchToSlv()" at the end of the loop is sort of a
             // "warp in time" -- the core disappears inside this, jmps to
             // animating a VP, and when that VP suspends, the suspend
             // jmps back. This has the effect of "returning" from the
             // switchToSlv() call. Then control loops back to here.
             //Alternatively, the VP suspend primitive could just not bother
             // returning from switchToSlv, and instead jmp directly to here.
      //core controller top of loop
      if(animSlot->slaveAssignedToSlot->typeOfVP == IdleVP)
       { //The Holistic stuff turns on idle slaves..  but can also be in mode
         // where have no idle slaves..  so, this IF statement can only be true
         // executed when HOLISTIC is turned on..
         numRepetitionsWithNoWork ++;
               HOLISTIC__Record_last_work;
       } 
      


            HOLISTIC__Record_AppResponderInvocation_start;
            MEAS__Capture_Pre_Master_Lock_Point;

      int numTriesToGetLock = 0; int gotLock = 0;
      while( gotLock == FALSE ) //keep going until get master lock
       { 
            //want to
            // reduce lock contention from cores with no work, so first
            // check if this is a core with no work, and busy wait if so.
            //Then, if it's been way too long without work, yield pthread
         if( numRepetitionsWithNoWork > NUM_REPS_W_NO_WORK_BEFORE_BACKOFF)
            doBackoff_for_TooLongWithNoWork( numRepetitionsWithNoWork, &seed1, &seed2 );
         if( numRepetitionsWithNoWork > NUM_REPS_W_NO_WORK_BEFORE_YIELD )
          {    //This is sequential mode.. just return
            return;
          }


            //Try to get the lock
         gotLock = __sync_bool_compare_and_swap( addrOfMasterLock,
                                                 UNLOCKED, LOCKED );
         if( gotLock )
          {    //At this point, have successfully gotten master lock.
               //So, break out of get-lock loop.
            break;
          }
            //Get here only when failed to get lock -- check in should do backoff

         numTriesToGetLock++;   //if too many, means too much contention
         if( numTriesToGetLock > NUM_TRIES_BEFORE_DO_BACKOFF ) 
            doBackoff_for_TooLongToGetLock( numTriesToGetLock, &seed1, &seed2 );
         if( numTriesToGetLock > MASTERLOCK_RETRIES_BEFORE_YIELD ) 
          { numTriesToGetLock = 0; pthread_yield(); }
       } //while( currVP == NULL )
            MEAS__Capture_Post_Master_Lock_Point;

         //have master lock, perform master function, which manages request
         // handling and assigning work to this core's slot
      foundWork =
         
         masterFunction( animSlot );
      
      PR_int__release_master_lock();
      
      if( foundWork )
         numRepetitionsWithNoWork = 0;
      else
         numRepetitionsWithNoWork += 1;

      //now that master is done, have work in the slot, so switch to it
            HOLISTIC__Record_Work_start;

      switchToSlv(animSlot->slaveAssignedToSlot); //Slave suspend makes core "return" from this call
      flushRegisters();    //prevent GCC optimization from doing bad things 

             MEAS__Capture_End_Susp_in_CoreCtlr_ForSys;
             HOLISTIC__Record_Work_end;
    }//while(1)
 }

#endif
