/*
 * Copyright 2010  OpenSourceResearchInstitute
 *
 * Licensed under BSD
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <inttypes.h>
#include <sys/time.h>

#include <PR__include/PR__WL.h>

#include "PR__structs__int.h"
#include "Services_Offered_by_PR/Services_Language/PRServ_int.h"


/* MEANING OF   WL  PI  SS  int
 * These indicate which places the function is safe to use.  They stand for:
 * WL: Wrapper Library
 * PI: Plugin 
 * SS: Startup and Shutdown
 * int: internal to the PR implementation
 */


void *
PR_WL__malloc( int32 sizeRequested )
 { void *ret;
 
   PR_int__get_malloc_lock();
   ret = PR_int__malloc( sizeRequested );
   PR_int__release_malloc_lock();
   return ret;
 }

void
PR_WL__free( void *ptrToFree )
 {
   PR_int__get_malloc_lock();
   PR_int__free( ptrToFree );
   PR_int__release_malloc_lock();
 }

/*there is a label inside this function -- save the addr of this label in
 * the callingSlv struc, as the pick-up point from which to start the next
 * work-unit for that slave.  If turns out have to save registers, then
 * save them in the slave struc too.  Then do assembly jump to the CoreCtlr's
 * "done with work-unit" label.  The slave struc is in the request in the
 * slave that animated the just-ended work-unit, so all the state is saved
 * there, and will get passed along, inside the request handler, to the
 * next work-unit for that slave.
 */
void
PR_WL__suspend_slaveVP_and_send_req( SlaveVP *animatingSlv )
 { 

      //This suspended Slv will get assigned by Master again at some
      // future point

      //return ownership of the Slv and anim slot to Master virt pr
   animatingSlv->animSlotAssignedTo->workIsDone = TRUE;

         HOLISTIC__Record_HwResponderInvocation_start;
         MEAS__Capture_Pre_Susp_Point;
      //This assembly function is a PR primitive that first saves the
      // stack and frame pointer, plus an addr inside this assembly code.
      //When core ctlr later gets this slave out of a sched slot, it
      // restores the stack and frame and then jumps to the addr.. that
      // jmp causes return from this function.
      //So, in effect, this function takes a variable amount of wall-clock
      // time to complete -- the amount of time is determined by the
      // Master, which makes sure the memory is in a consistent state first.
   switchToCoreCtlr(animatingSlv);
   flushRegisters();
         MEAS__Capture_Post_Susp_Point;
		 
   return;
 }

/*As of Feb 2013, doesn't use magic number -- only one ID shared across all
 * langlets
 */
inline
int32 *
PR__give_ID_from_slave( SlaveVP *animSlv, int32 magicNumber )
 {
   return animSlv->ID;
 }

inline
int32 *
PR__give_ID_from_lang_meta_task( void *_task )
 { PRMetaTask *task;
   task = PR_int__give_prolog_of_lang_meta_task(_task);
   return task->ID;
 }

SlaveVP *
PR__give_owner_of_ID( int32 *taskID, SlaveVP *animSlv )
 {
    printf("implement me %s", __FILE__);
   //metaTask = lookup( taskID );
   //return metaTask->slaveAssignedTo;
 }

/*For this implementation of PR, it may not make much sense to have the
 * system of requests for creating a new processor done this way.. but over
 * the scope of single-master, multi-master, mult-tasking, OS-implementing,
 * distributed-memory, and so on, this gives PR implementation a chance to
 * do stuff before suspend, in the SlaveVP, and in the Master before the plugin
 * is called, as well as in the lang-lib before this is called, and in the
 * plugin.  So, this gives both PR and language implementations a chance to
 * intercept at various points and do order-dependent stuff.
 *Having a standard PRNewPrReqData struc allows the language to create and
 * free the struc, while PR knows how to get the newSlv if it wants it, and
 * it lets the lang have lang-specific data related to creation transported
 * to the plugin.
 */
void
PR_WL__send_create_slaveVP_req( void *langReq, int32 *ID, CreateHandler handler, 
                                SlaveVP *reqstingSlv, int32 magicNum )
 { PRReqst req;

   req.reqType          = SlvCreate;
   req.ID               = ID;
   req.langMagicNumber  = magicNum;
   req.langReq          = langReq;
   req.createHdlr       = handler;
   reqstingSlv->request = &req;

   PR_WL__suspend_slaveVP_and_send_req( reqstingSlv );
 }


/*
 *This adds a request to dissipate, then suspends the processor so that the
 * request handler will receive the request.  The request handler is what
 * does the work of freeing memory and removing the processor from the
 * language environment's data structures.
 *The request handler also is what figures out when to shutdown the PR
 * system -- which causes all the core controller threads to die, and returns from
 * the call that started up PR to perform the work.
 *
 *This form is a bit misleading to understand if one is trying to figure out
 * how PR works -- it looks like a normal function call, but inside it
 * sends a request to the request handler and suspends the processor, which
 * jumps out of the PR_WL__dissipate_slaveVP function, and out of all nestings
 * above it, transferring the work of dissipating to the request handler,
 * which then does the actual work -- causing the processor that animated
 * the call of this function to disappear and the "hanging" state of this
 * function to just poof into thin air -- the virtual processor's trace
 * never returns from this call, but instead the virtual processor's trace
 * gets suspended in this call and all the virt processor's state disap-
 * pears -- making that suspend the last thing in the Slv's trace.
 */
void
PR_WL__send_end_slave_req( void *langReq, RequestHandler handler,
                           SlaveVP *slaveToDissipate, int32 magicNum )
 { PRReqst req;

   req.reqType                = SlvDissipate;
   req.langMagicNumber        = magicNum;
   req.handler                = handler;
//   req.nextReqst              = slaveToDissipate->request;
   slaveToDissipate->request = &req;

   PR_WL__suspend_slaveVP_and_send_req( slaveToDissipate );
 }
 
inline
void
PR_WL__send_create_task_req( BirthFn fn, void *initData, void *langReq, 
     int32 *taskID, CreateHandler handler, SlaveVP *animSlv, int32 magicNumber)
 { PRReqst req;
 
   req.reqType    = TaskCreate;
   req.birthFn    = fn;
   req.initData   = initData;
   req.ID         = taskID;
   req.langReq    = langReq;
   req.createHdlr = handler;
   req.langMagicNumber = magicNumber;
   animSlv->request = &req;
   
   PR_WL__suspend_slaveVP_and_send_req( animSlv );
 }

inline
void
PR_WL__send_end_task_request( void *langReq, RequestHandler handler, 
                                              SlaveVP *animSlv, int32 magicNum )
 { PRReqst req;
 
   req.reqType         = TaskEnd;
   req.langReq         = langReq;
   req.handler         = handler;
   req.langMagicNumber = magicNum;
   animSlv->request    = &req;
   
   PR_WL__suspend_slaveVP_and_send_req( animSlv );
 }


/*This call's name indicates that request is malloc'd -- so req handler
 * has to free any extra requests tacked on before a send, using this.
 *
 * This inserts the semantic-layer's request data into standard PR carrier
 * request data-struct that is mallocd.  The lang request doesn't need to
 * be malloc'd if this is called inside the same call chain before the
 * send of the last request is called.
 *
 *The request handler has to call PR_int__free_PRReq for any of these
 */
inline void
PR_WL__add_lang_request_in_mallocd_PRReqst( void *langReqData,
                                          SlaveVP *callingSlv )
 { PRReqst *req;
//WARNING: not updated.. may be buggy
   req = PR_int__malloc( sizeof(PRReqst) );
   req->reqType         = Language;
   req->langReq         = langReqData;
   req->nextReqst       = callingSlv->request;
   callingSlv->request  = req;
 }

inline 
int32 *
PR_WL__create_taskID_of_size( int32 numInts )
 { int32 *taskID;
   
   taskID    = PR_WL__malloc( sizeof(int32) + numInts * sizeof(int32) );
   taskID[0] = numInts;
   return taskID;
 }

/*This inserts the semantic-layer's request data into standard PR carrier.
 * PR Request data-struct is allocated on stack of this call & ptr to it sent
 * to plugin
 *Then it does suspend, to cause request to be sent.
 */
inline 
void
PR_WL__send_lang_request( void *langReqData, RequestHandler handler, 
                                           SlaveVP *callingSlv, int32 magicNum )
 { PRReqst req;

   req.reqType         = Language;
   req.langMagicNumber = magicNum;
   req.langReq         = langReqData;
   req.handler         = handler;
   req.nextReqst       = callingSlv->request;
   callingSlv->request = &req;
   
   PR_WL__suspend_slaveVP_and_send_req( callingSlv );
 }

inline 
void
PR_WL__send_lang_shutdown_request( SlaveVP *callingSlv, int32 magicNum )
 { PRReqst req;

   req.reqType         = LangShutdown;
   req.langMagicNumber = magicNum;
   req.nextReqst       = NULL;
   callingSlv->request = &req;
   
   PR_WL__suspend_slaveVP_and_send_req( callingSlv );
 }


/*This sends a PRLang request -- for probe, exception, and so on..
 * 
 */
inline void
PR_WL__send_service_request( void *langReqData, SlaveVP *callingSlv )
 { PRReqst req;

   req.reqType         = Service;
   req.langMagicNumber = PRServ_MAGIC_NUMBER;
   req.langReq         = langReqData;
   req.nextReqst       = callingSlv->request; //grab any other preceeding 
   callingSlv->request = &req;

   PR_WL__suspend_slaveVP_and_send_req( callingSlv );
 }

/*May 2012
 *To throw exception from wrapper lib or application, first turn
 * it into a request, then send the request
 */
void
PR_WL__throw_exception( char *msgStr, SlaveVP *reqstSlv,  PRExcp *excpData )
 { PRReqst req;
   PRServiceReq langReq;
   printf("exception");
   req.reqType         = Language;
   req.langReq          = &langReq;
   req.nextReqst       = reqstSlv->request; //gab any other preceeding 
   reqstSlv->request   = &req;

   langReq.msgStr        = msgStr;
   langReq.exceptionData = excpData;
   
   PR_WL__suspend_slaveVP_and_send_req( reqstSlv );
 }
