/*
 * Copyright 2010  OpenSourceResearchInstitute
 *
 * Licensed under BSD
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <inttypes.h>
#include <sys/time.h>
#include <pthread.h>

#include <PR__include/PR__structs__common.h>
#include <PR__include/PR__int.h>
#include <PR__include/PR__SS.h>
#include "Defines/PR_defs.h"
#include "PR__structs__int.h"
#include "Services_Offered_by_PR/Measurement_and_Stats/probes__int.h"

#define thdAttrs NULL


/* MEANING OF   WL  PI  SS  int
 * These indicate which places the function is safe to use.  They stand for:
 * WL: Wrapper Library
 * PI: Plugin 
 * SS: Startup and Shutdown
 * int: internal to the PR implementation
 */


//===========================================================================

/*
MallocProlog *
create_free_list();
*/

void
endOSThreadFn( void *initData, SlaveVP *animatingSlv );


//===========================================================================
void
PR_SS__create_topEnv()
 { 
   PRQueueStruc   **readyToAnimateQs;
   int32            coreIdx;
   SlaveVP        **masterVPs;
   AnimSlot       **allAnimSlots; //ptr to array of ptrs

      //Make the top env, which holds everything else
   _PRTopEnv = PR_int__malloc( sizeof(TopEnv) );
 
   
  #ifdef DEBUG__TURN_ON_SEQUENTIAL_MODE
   _PRTopEnv->num_cores = 1;
  #else
   _PRTopEnv->num_cores = giveNumLogicalCores();
  #endif

    
   //=================== Both single and multi lang vars ==================
   //
   //
   _PRTopEnv->metaInfo     = PR_int__malloc( sizeof(PRSysMetaInfo) );
   memset( _PRTopEnv->metaInfo, 0, sizeof(PRSysMetaInfo) );
  
   masterVPs               = PR_int__malloc( _PRTopEnv->num_cores * sizeof(SlaveVP *) );
   _PRTopEnv->masterVPs    = masterVPs;
   
   allAnimSlots            = PR_int__malloc( _PRTopEnv->num_cores * sizeof(AnimSlot *) );
   _PRTopEnv->allAnimSlots = allAnimSlots;

    
      //For each animation slot, there is an idle slave, and an initial
      // slave assigned as the current-task-slave.  Create them here.
   int32    coreNum;
   SlaveVP *idleSlv, *slotTaskSlv;
   _PRTopEnv->slotTaskSlvs = PR__malloc( _PRTopEnv->num_cores * sizeof(SlaveVP *));
   _PRTopEnv->idleSlv      = PR__malloc( _PRTopEnv->num_cores * sizeof(SlaveVP *));
      
   for( coreNum = 0;  coreNum < _PRTopEnv->num_cores; coreNum++ )
    { 
      allAnimSlots[ coreNum ] = PR_SS__create_anim_slot( coreNum );
      
      idleSlv = PR_int__create_slaveVP_helper( &idle_fn, NULL );
      idleSlv->coreAnimatedBy                = coreNum;
      idleSlv->typeOfVP                      = IdleVP;
      idleSlv->animSlotAssignedTo            = allAnimSlots[coreNum];
         //start with all slots filled by the idle slaves
      (allAnimSlots[coreNum])->slaveAssignedToSlot = idleSlv;
      _PRTopEnv->idleSlv[coreNum]  = idleSlv;
         
         
      slotTaskSlv = PR_int__create_slot_slave( );
      slotTaskSlv->coreAnimatedBy            = coreNum;
      slotTaskSlv->animSlotAssignedTo        = allAnimSlots[coreNum];
         
      slotTaskSlv->typeOfVP                     = SlotTaskSlv;
      _PRTopEnv->slotTaskSlvs[coreNum] = slotTaskSlv;
      
    }

   _PRTopEnv->slaveRecycleQ    = makePrivQ();
   
   _PRTopEnv->masterLock       = UNLOCKED;
   _PRTopEnv->seed1 = rand()%1000; // init random number generator
   _PRTopEnv->seed2 = rand()%1000; // init random number generator
   
   _PRTopEnv->measHistsInfo    = NULL; 
   
   _PRTopEnv->numSlavesCreated = 0;  //used by create slave to set slave ID
  
      //TODO: remove this, and the vars from TopEnv
   //=================== single lang only vars ==================
   //
   //
   _PRTopEnv->numSlavesCreated = 0;  //used to give unique ID to processor
   _PRTopEnv->numTasksCreated  = 0;   //to give unique ID to a task
   _PRTopEnv->numSlavesAlive   = 0;

   _PRTopEnv->shutdownInitiated = FALSE;
   _PRTopEnv->coreIsDone = PR_int__malloc( _PRTopEnv->num_cores * sizeof( bool32 ) );
   memset( _PRTopEnv->coreIsDone, 0, _PRTopEnv->num_cores * sizeof( bool32 ) );

   //=================== multi lang only vars ==================
   //
   //
   readyToAnimateQs = PR_int__malloc( _PRTopEnv->num_cores * sizeof(PRQueueStruc *) );
   
      //BUG: have a fixed max num processes, but never check whether exceeded
   _PRTopEnv->processes    = PR_int__malloc( NUM_IN_COLLECTION * sizeof(PRProcess *) );
   _PRTopEnv->numProcesses = 0;
   _PRTopEnv->currProcessIdx = 0;
   _PRTopEnv->firstProcessReady = FALSE;      //use while starting up coreCtlr   
      
      //initialize flags for waiting for activity within PR to complete
   pthread_mutex_init( &(_PRTopEnv->activityDoneLock), NULL );
   pthread_cond_init(  &(_PRTopEnv->activityDoneCond), NULL );
   _PRTopEnv->allActivityIsDone = FALSE;

   //============================= MEASUREMENT STUFF ========================
      
         MEAS__Make_Meas_Hists_for_Susp_Meas;
         MEAS__Make_Meas_Hists_for_Master_Meas;
         MEAS__Make_Meas_Hists_for_Master_Lock_Meas;
         MEAS__Make_Meas_Hists_for_Malloc_Meas;
         MEAS__Make_Meas_Hists_for_Plugin_Meas;
         MEAS__Make_Meas_Hists_for_Language;

         PROBES__Create_Probe_Bookkeeping_Vars;
         
         HOLISTIC__Setup_Perf_Counters;
         
   //========================================================================
 }

AnimSlot *
PR_SS__create_anim_slot( int32 coreSlotIsOn )
 { AnimSlot  *animSlot;
   int i;

   animSlot  = PR_int__malloc( sizeof(AnimSlot) );

      //Set state to mean "handling requests done, slot needs filling"
   animSlot->workIsDone         = FALSE;
   animSlot->needsWorkAssigned = TRUE;
   animSlot->coreSlotIsOn       = coreSlotIsOn;
   
   return animSlot;
 }


/*This is called during PR startup..  It simple creates the OS threads, with the
 * core controller code as the top level function.
 *However, there's nothing for these threads to do until a process is created,
 * so, the core controller has a cond-var and flag in the top env they all 
 * "wait" on.  The process creation call includes a singleton that releases
 * all the core controller threads when the first process is created.
 */
void
PR_SS__create_the_coreCtlr_OS_threads()
 {
   //========================================================================
   //                      Create the Threads
   int coreIdx, retCode;

      //Need the threads to be created suspended, and wait for a signal
      // before proceeding -- gives time after creating to initialize other
      // stuff before the coreCtlrs set off.
   _PRTopEnv->firstProcessReady = 0;
   
      //initialize the cond used to make the new threads wait and sync up
      //must do this before *creating* the threads..
   pthread_mutex_init( &suspendLock, NULL );
   pthread_cond_init( &suspendCond, NULL );

   coreCtlrThdHandles = PR_int__malloc( _PRTopEnv->num_cores * sizeof(pthread_t));
   coreCtlrThdParams = PR_int__malloc( _PRTopEnv->num_cores * sizeof(ThdParams *));
      //Make the threads that animate the core controllers
   for( coreIdx=0; coreIdx < _PRTopEnv->num_cores; coreIdx++ )
    { coreCtlrThdParams[coreIdx]          = PR_int__malloc( sizeof(ThdParams) );
      coreCtlrThdParams[coreIdx]->coreNum = coreIdx;

      retCode =
      pthread_create( &(coreCtlrThdHandles[coreIdx]),
                        thdAttrs,
                       &coreController,
               (void *)(coreCtlrThdParams[coreIdx]) );
      if(retCode){printf("ERROR creating thread: %d\n", retCode); exit(1);}
    }
 }

/*This is intended to be called from lang plugin dynamic lib, and call
 * into proto-runtime dynamic lib -- so, the call in the lang code
 * will get different answers depending upon which PR impl it connects
 * to.
 */
int
PR_SS__give_num_cores()
 { return _PRTopEnv->num_cores;
 }


//======================
//===
//=
//inline
void *
PR_SS__create_lang_env( int32 size, SlaveVP *slave, int32 magicNum )
 {
   return PR_int__create_lang_env_in_process( size, slave->processSlaveIsIn, magicNum );
 }


/*These store the pointer to handler into the language env -- language env
 * found by using magic num to look it up in the process that the seedVP
 * is inside of.
 */
void
PR_SS__register_assigner( SlaveAssigner assigner, SlaveVP *seedVP, int32 magicNum )
 { PRLangEnv *protoLangEnv;
 
   protoLangEnv = PR_SS__give_proto_lang_env_for_slave( seedVP, magicNum );
   protoLangEnv->workAssigner = assigner; 
 }
void
PR_SS__register_lang_shutdown_handler( LangShutdownHdlr shutdownHdlr, SlaveVP *seedVP,
                                  int32 magicNum )
 { PRLangEnv *protoLangEnv;
 
   protoLangEnv = PR_SS__give_proto_lang_env_for_slave( seedVP, magicNum );
   protoLangEnv->shutdownHdlr = shutdownHdlr; 
 }
void
PR_SS__register_lang_data_creator( LangDataCreator langDataCreator, 
                                              SlaveVP *seedVP, int32 magicNum )
 { PRLangEnv *protoLangEnv;
 
   protoLangEnv = PR_SS__give_proto_lang_env_for_slave( seedVP, magicNum );
   protoLangEnv->langDataCreator = langDataCreator; 
 }
void
PR_SS__register_lang_meta_task_creator( LangMetaTaskCreator langMetaTaskCreator, 
                                              SlaveVP *seedVP, int32 magicNum )
 { PRLangEnv *protoLangEnv;
 
   protoLangEnv = PR_SS__give_proto_lang_env_for_slave( seedVP, magicNum );
   protoLangEnv->langMetaTaskCreator = langMetaTaskCreator; 
 }
void
PR_SS__register_make_slave_ready_fn( MakeSlaveReadyFn fn, SlaveVP *seedVP,
                                                         int32 magicNum )
 { PRLangEnv *protoLangEnv;
 
   protoLangEnv = PR_SS__give_proto_lang_env_for_slave( seedVP, magicNum );
   protoLangEnv->makeSlaveReadyFn = fn; 
 }
void
PR_SS__register_make_task_ready_fn( MakeTaskReadyFn fn, SlaveVP *seedVP,
                                                         int32 magicNum )
 { PRLangEnv *protoLangEnv;
 
   protoLangEnv = PR_SS__give_proto_lang_env_for_slave( seedVP, magicNum );
   protoLangEnv->makeTaskReadyFn = fn; 
 }

//========


/*are in Master when call this..  due to detecting end-of-work, from inside 
 * end-task or dissipate..  or, call this when app calls "end process"
 */
void
PR_SS__end_process_normally( PRProcess *process )
 { int32 i, processIdx;
   PRProcess **processes;
   
      //remove process from PR's list of processes..
   processes = _PRTopEnv->processes;
   //find the process within the list
   for( i = 0; i < _PRTopEnv->numProcesses; i++ )
    { if( processes[i] == process )
       { processIdx = i;
         break;
       }
    }
   //move all the higher processes down, overwriting the target
   for( i = processIdx +1; i < _PRTopEnv->numProcesses; i++ )
    { processes[i-1] = processes[i];
    }
   _PRTopEnv->numProcesses -= 1;
   
      //remove process from the process-to-slot chooser
   _PRTopEnv->currProcessIdx = 0; //start choosing starting at process 0
      
      //call shutdown on each langlet started in process (which frees any
      // langlet-allocd data in langEnv);
      //Then free the lang env
   PRLangEnv *protoLangEnv;
   for( i = 0; i < process->numLangEnvs; i++ )
    { protoLangEnv = process->protoLangEnvsList[i];
         //The lang shutdowns should free any slaves or tasks in the langEnv
      (*protoLangEnv->shutdownHdlr)(&(protoLangEnv[1]));
      PR_int__free( protoLangEnv ); //don't need to remove from process!
    }
   PR_int__free( process->protoLangEnvsList ); //array of Envs empty so free it
   PR_int__free( &(((int32 *)process->langEnvs)[-1]) ); //the collection array
   
      //any slaves from this process still in slots will finish executing, but
      // have no lang env for the request handlers to use!
      //So, have to mark each slave, so the request handling isn't done
   AnimSlot *slot;
   int32 core;
   for( core = 0; core < _PRTopEnv->num_cores; core++ )
    { slot = _PRTopEnv->allAnimSlots[core];
      if( slot->slaveAssignedToSlot->processSlaveIsIn == process )
       {    //turn off req handling by disallowing slave from chging slot flag
         slot->slaveAssignedToSlot->animSlotAssignedTo = NULL;
         slot->workIsDone = FALSE; //just in case, make sure req hdling off
         slot->needsWorkAssigned = TRUE; //make new slave be assigned
       }
    }
     
      //cause resume of "PR__wait_for_process_to_end()" call
   if( process->hasWaitingToEnd )
    {
     #ifdef DEBUG__TURN_ON_SEQUENTIAL_MODE
      process->executionIsComplete = TRUE;
      PR_int__free(process);
         //The "wait for process to end" call started the PR activity that
         // happens inside that process.  That leads to here, running inside the
         // master.. so just return, and let the core ctlr detect no work and
         // return.
      return;
     #else
      //Here, must use OS thread constructs.. the main thread is waiting for
      // PR process to complete.  So, at this point, cause main to resume.
       
      pthread_mutex_lock(     &(process->doneLock) );
      process->executionIsComplete = TRUE;
      pthread_cond_broadcast( &(process->doneCond) );
      pthread_mutex_unlock(   &(process->doneLock) );
         //now wait for woken waiter to Ack, then free the process struct
      pthread_mutex_lock(     &(process->doneAckLock)  ); //BUG:? may be a race
      pthread_mutex_unlock(   &(process->doneAckLock)  );  
      PR_int__free(process);
     #endif
    }
      //if no more processes, cause resume of "PR__wait_for_all_activity_to_end"
   if( _PRTopEnv->numProcesses == 0)
    { 
      pthread_mutex_lock(     &(_PRTopEnv->activityDoneLock) );
      _PRTopEnv->allActivityIsDone = TRUE;
      pthread_cond_broadcast( &(_PRTopEnv->activityDoneCond) );
      pthread_mutex_unlock(   &(_PRTopEnv->activityDoneLock) );
    }
 }


/*Calling this causes the core controller OS threads to exit, 
 * 
 *The _PRTopEnv is used inside this shut down function, so it wait until
 * the OS thread shutdown is done before freeing it.
 *  
 *In here,create one core-loop shut-down slave for each core controller 
 * and put them all directly into the animation slots.
 * 
 *Note, this function should ONLY be called after all processes have ended,
 * because it doesn't pay attention to whether unfinished work exists.. 
 */
void
PR_SS__shutdown_OS_threads()
 { int32       coreIdx;
   SlaveVP    *shutDownSlv;
   AnimSlot   *animSlot;
      //create the shutdown processors, one for each core controller -- put them
      // directly into the slots
   PR_int__get_wrapper_lock();
   for( coreIdx = 0; coreIdx < _PRTopEnv->num_cores; coreIdx++ )
    {    //Note, this is running in the master
      shutDownSlv = PR_SS__create_shutdown_slave();
         //last slave has dissipated, so no more in slots, so write
         // shut down slave into first animulng slot.
      animSlot = _PRTopEnv->allAnimSlots[ coreIdx ];
      animSlot->slaveAssignedToSlot = shutDownSlv;
      animSlot->workIsDone          = TRUE;
      animSlot->needsWorkAssigned   = FALSE;
      shutDownSlv->coreAnimatedBy       = coreIdx;
      shutDownSlv->animSlotAssignedTo   = animSlot;
    }
   PR_int__release_wrapper_lock();
 }


SlaveVP* PR_SS__create_shutdown_slave(int32 coreNum)
 {
   SlaveVP* shutdownVP;
   
   shutdownVP = PR_int__create_slaveVP_helper( &idle_fn, NULL );
   shutdownVP->typeOfVP                      = ShutdownVP;
          
   return shutdownVP;
 }


/*This is run in the main thread, as part of the PR__shutdown() call
 * It frees any memory allocated from the OS
 */
void
PR_SS__cleanup_at_end_of_shutdown()
 { 
      //All the environment data has been allocated with PR__malloc, so just
      // free it
   PR_int__free( (void *)_PRTopEnv );
 }



void
PR_SS__print_out_measurements()
 {
   if( _PRTopEnv->measHistsInfo != NULL )
    { forAllInDynArrayDo( _PRTopEnv->measHistsInfo, (DynArrayFnPtr)&printHist );
      forAllInDynArrayDo( _PRTopEnv->measHistsInfo, (DynArrayFnPtr)&saveHistToFile);
      forAllInDynArrayDo( _PRTopEnv->measHistsInfo, (DynArrayFnPtr)&freeHist );
    }
   
   MEAS__Print_Hists_for_Susp_Meas;
   MEAS__Print_Hists_for_Master_Meas;
   MEAS__Print_Hists_for_Master_Lock_Meas;
   MEAS__Print_Hists_for_Malloc_Meas;
   MEAS__Print_Hists_for_Plugin_Meas;
 }

/*This waits for the OS threads in the PR system to end.  Causing them to end
 * has to be done separately (haven't worked out details as of this comment)
 */
void
PR_SS__wait_for_PR_to_shutdown()
 {
      //wait for all to complete
   int coreIdx;
   for( coreIdx=0; coreIdx < _PRTopEnv->num_cores; coreIdx++ )
    {
      pthread_join( coreCtlrThdHandles[coreIdx], NULL );
    }
 }


//============================

/*Am trying to be cute, avoiding IF statement in coreCtlr that checks for
 * a special shutdown slaveVP.  Ended up with extra-complex shutdown sequence.
 *This function has the sole purpose of setting the stack and framePtr
 * to the coreCtlr's stack and framePtr.. it does that then jumps to the
 * core ctlr's shutdown point -- might be able to just call Pthread_exit
 * from here, but am going back to the pthread's stack and setting everything
 * up just as if it never jumped out, before calling pthread_exit.
 *The end-point of core ctlr will free the stack and so forth of the
 * processor that animates this function, (this fn is transfering the
 * animator of the AppSlv that is in turn animating this function over
 * to core controller function -- note that this slices out a level of virtual
 * processors).
 */
void
endOSThreadFn( void *initData, SlaveVP *animatingSlv )
 { 
   #ifdef DEBUG__TURN_ON_SEQUENTIAL_MODE
    asmTerminateCoreCtlrSeq(animatingSlv);
   #else
    return;
    asmTerminateCoreCtlr(animatingSlv);
   #endif
 }

//================================


