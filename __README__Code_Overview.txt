
This file is intended to help those new to PR to find their way around the code.

Some observations:
-] PR.h is the top header file, and is the root of a tree of #includes that pulls in all the other headers

-] Defines directory contains all the header files that hold #define statements

-] PR has several kinds of function, grouped according to what kind of code should call them: PR_App_.. for applications to call, PR_WL_.. for wrapper-library code to call, PR_PI_.. for plugin code to call, and PR_int_.. for PR to use internally.  Sometimes PR_int_ functions are called from the wrapper library or plugin, but this should only be done by programmers who have gained an in-depth knowledge of PR's implementation and understand that PR_int_ functions are not protected for concurrent use..

-] PR has its own version of malloc, unfortunately, which is due to the system malloc breaking when the stack-pointer register is manipulated, which PR must do.  The PR form of malloc must be used in code that runs inside the PR system, especially all application code that uses a PR-based language.  However, a complication is that the malloc implementation is not protected with a lock.  However, mallocs performed in the main thread, outside the PR-language program, cannot use PR malloc..  this presents some issues crossing the boundary..

-] Things in the code are turned on and off by using #define in combination with #ifdef.  All defines for doing this are found in Defines/PR_defs__turn_on_and_off.h.  The rest of the files in Defines directory contain macro definitions, hardware constants, and any other #define statements.

-] PR has many macros used in the code..  such as for measurements and debug..  all measurement, debug, and statistics gathering statements can be turned on or off by commenting-out or uncommenting the appropriate #define.  

-] The best way to learn PR is to uncomment  DEBUG__TURN_ON_SEQUENTIAL_MODE, which allows using a normal debugger while sequentially executing through both application code and PR internals.  Setting breakpoints at various spots in the code is a good way to see the PR system in operation.

-] PR has several "PR primitives" implemented with assembly code.  The net effect of these assembly functions is to perform the switching between application code and the PR system.

-] The heart of this multi-core version of PR is the AnimationMaster and CoreController.  Those files have large comments explaining the nature of PR and this implementation.  Those comments are the best place to start reading, to get an understanding of the code before tracing through it.